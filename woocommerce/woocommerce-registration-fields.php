<?php
/**
 * Plugin Name: WooCommerce Registration Fields
 * Plugin URI: http://claudiosmweb.com/
 * Description: My Custom registration fields.
 * Version: 1.0
 * Author: Claudio Sanches
 * Author URI: http://claudiosmweb.com/
 * License: GPL2
 */

/**
 * Add new register fields for WooCommerce registration.
 *
 * @return string Register fields HTML.
 */
function vg_siva_wooc_extra_register_fields() {
    ?>

   <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
	<label for="billing_first_name"><?php _e( 'First name', 'vg-siva' ); ?></label>
    <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) echo esc_attr($_POST['billing_first_name']); ?>" />
    </p>

    <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
	<label for="billing_last_name"><?php _e( 'Last name', 'vg-siva' ); ?></label>
    <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) echo esc_attr($_POST['billing_last_name']); ?>" />
    </p>

    <div class="clear"></div>

    <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
	<label for="billing_phone"><?php _e( 'Phone', 'vg-siva' ); ?></label>
    <input type="text" class="input-text" name="billing_phone" id="reg_billing_phone" value="<?php if ( ! empty( $_POST['billing_phone'] ) ) echo esc_attr($_POST['billing_phone']); ?>" />
    </p>

    <?php
}

add_action( 'woocommerce_register_form_start', 'vg_siva_wooc_extra_register_fields' );

/**
 * Validate the extra register fields.
 *
 * @param  string $username          Current username.
 * @param  string $email             Current email.
 * @param  object $validation_errors WP_Error object.
 *
 * @return void
 */
function vg_siva_wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {
    if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
        $validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'vg-siva' ) );
    }

    if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
        $validation_errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'vg-siva' ) );
    }


    if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {
        $validation_errors->add( 'billing_phone_error', __( '<strong>Error</strong>: Phone is required!.', 'vg-siva' ) );
    }
}

add_action( 'woocommerce_register_post', 'vg_siva_wooc_validate_extra_register_fields', 10, 3 );

/**
 * Save the extra register fields.
 *
 * @param  int  $customer_id Current customer ID.
 *
 * @return void
 */
function vg_siva_wooc_save_extra_register_fields( $customer_id ) {
    if ( isset( $_POST['billing_first_name'] ) ) {
        // WordPress default first name field.
        update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );

        // WooCommerce billing first name.
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
    }

    if ( isset( $_POST['billing_last_name'] ) ) {
        // WordPress default last name field.
        update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );

        // WooCommerce billing last name.
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
    }

    if ( isset( $_POST['billing_phone'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
    }
}

add_action( 'woocommerce_created_customer', 'vg_siva_wooc_save_extra_register_fields' );