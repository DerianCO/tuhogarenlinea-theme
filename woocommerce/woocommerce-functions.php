<?php

remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
remove_action('woocommerce_cart_collaterals', 'woocommerce_cart_totals');

//Breadcrumb WooCommerce
function vg_siva_woocommerce_breadcrumbs() {
    return array(
		'delimiter'   => '<li class="separator"> / </li>',
		'wrap_before' => '<ul id="breadcrumbs" class="breadcrumbs">',
		'wrap_after'  => '</ul>',
		'before'      => '<li class="item">',
		'after'       => '</li>',
		'home'        => _x('Home', 'breadcrumb', 'vg-siva'),
	);
}
add_filter('woocommerce_breadcrumb_defaults', 'vg_siva_woocommerce_breadcrumbs', 20, 0);

// Override woocommerce widgets
if(! function_exists('vg_siva_override_woocommerce_widgets'))
{
	function vg_siva_override_woocommerce_widgets()
	{
		//Show mini cart on all pages
		if(class_exists('WC_Widget_Cart'))
		{
			unregister_widget('WC_Widget_Cart');
			get_template_part('woocommerce/class-wc-widget-cart');
			register_widget('Custom_WC_Widget_Cart');
		}
	}
}
add_action('widgets_init', 'vg_siva_override_woocommerce_widgets', 15);

//Change rating html
if(! function_exists('vg_siva_get_rating_html'))
{
	function vg_siva_get_rating_html($rating_html, $rating)
	{
		global $product;

		if($rating > 0)
		{
			$title = sprintf(esc_html__('Rated %s out of 5', 'vg-siva'), $rating);
		}
		else {
			$title = 'Not yet rated';
			$rating = 0;
		}

		$rating_html  = '<div class="product-rating">';
			$rating_html .= '<div class="star-rating" title="' . esc_attr($title) . '">';
				$rating_html .= '<span style="width:' . esc_attr(($rating / 5) * 100) . '%"><strong class="rating">' . $rating . '</strong> ' . esc_html__('out of 5', 'vg-siva') . '</span>';
			$rating_html .= '</div>';
			$rating_html .= $product->get_review_count(). esc_html__(" review(s)", "vg-siva");
		$rating_html .= '</div>';

		return $rating_html;
	}
}
add_filter('woocommerce_product_get_rating_html', 'vg_siva_get_rating_html', 10, 2);


//Change price html
add_filter('woocommerce_get_price_html', 'vg_siva_woo_price_html', 100, 2);
function vg_siva_woo_price_html($price, $product)
{
	if($product->is_type('variable')) {
    $variation_min_reg_price = $product->get_variation_regular_price('min', true);
    $variation_min_sale_price = $product->get_variation_sale_price('min', true);
    $symbol = get_woocommerce_currency_symbol();
    if(!empty($variation_min_reg_price)){
        $price = wc_price($variation_min_sale_price);
    }else{
        $price = wc_price($variation_min_reg_price);
    }

    $vg_siva_options 	= get_option("vg_siva_options");

    $from_price = isset($vg_siva_options['from_price_text']) ? $vg_siva_options['from_price_text'] : '';

    return '<div class="product-price price-variable"> <span class="from-price">'. $from_price .'</span> '. $price .'</div>';
	}
	else {
		return '<div class="product-price">'. $price .'</div>';
	}
}

//Change Sale/Hot Label
function vg_siva_woo_label_html(){

	$vg_siva_options 	= get_option("vg_siva_options");
	global $post, $product;
	$label ='';

	$featured = isset($vg_siva_options['hot_label']) ?  $vg_siva_options['hot_label'] : esc_html__('Hot', 'vg-siva');
	$sale = isset($vg_siva_options['sale_label']) ?  $vg_siva_options['sale_label'] : esc_html__('Sale', 'vg-siva');
	?>

	<?php if($product->is_featured() || $product->is_on_sale()) : ?>
	<div class="product-label">
		<?php if($product->is_featured()) : ?>
			<?php echo '<span class="featured">' . esc_html($featured) . '</span>'; ?>
		<?php endif; ?>

		<?php if($product->is_on_sale()) : ?>
			<?php
        if(! $product->is_type('variable')){
           // Get product prices
           $regular_price = (float) $product->get_regular_price(); // Regular price
           $sale_price = (float) $product->get_price(); // Active price (the "Sale price" when on-sale)

           // "Saving Percentage" calculation and formatting
           $precision = 0;
           $saving_percentage ='-'.round( 100 - ( $sale_price / $regular_price * 100 )) . '% OFF';

           $sale = $saving_percentage;
        }

        echo '<span class="sale">' . esc_html($sale) . '</span>';
      ?>
		<?php endif; ?>
	</div>
	<?php
	endif;
}
// Change products per page
function vg_siva_woo_change_per_page()
{
	$vg_siva_options 	= get_option("vg_siva_options");
	$products_per_page 	= isset($vg_siva_options['products_per_page']) ?  $vg_siva_options['products_per_page'] : 9;

	return $products_per_page;
}
add_filter('loop_shop_per_page', 'vg_siva_woo_change_per_page', 20);

// Change number or products per row to 4
function vg_siva_loop_columns()
{
	$vg_siva_options 		= get_option("vg_siva_options");
	$products_per_column = isset($vg_siva_options['products_per_column']) ? $vg_siva_options['products_per_column'] : 3;

	if(isset($_GET['column']) && $_GET['column'] != ''){
		$products_per_column = $_GET['column'];
	}
	elseif(isset($_GET['sidebar']) && $_GET['sidebar'] == 'both'){
		$products_per_column = 2;
	}
	else{
		$products_per_column;
	}

	return $products_per_column;
}
add_filter('loop_shop_columns', 'vg_siva_loop_columns', 999);

function vg_siva_woocommerce_category_image() {
	global $wp_query, $vg_siva_options;
	if ( is_product_category() || is_shop() || is_product_tag()){
		$image = '';
		if(is_product_category() || is_product_tag()) {
			$cat = $wp_query->get_queried_object();
			$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
			$image = wp_get_attachment_url( $thumbnail_id );
		}
	    if ( ($image && !empty($image))) {?>
		   <div class="category-image-desc"><img data-src="<?php echo esc_url($image); ?>" src="#" alt="" class="lazy"/></div>
		<?php
		}
	}
}
add_action( 'woocommerce_archive_description', 'vg_siva_woocommerce_category_image', 5 );
// For Single Product Page
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 15);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 15);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 25);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 20);

// Move message to top
remove_action('woocommerce_before_shop_loop', 'wc_print_notices', 10);
add_action('woocommerce_show_message', 'wc_print_notices', 10);


//Display stock status on product page
function vg_siva_product_stock_status(){
	global $product;
	?>
	<div class="in-stock">
		<?php esc_html_e('Availability:', 'vg-siva');?>
		<?php if($product->is_in_stock()){ ?>
			<span><?php echo($product->get_stock_quantity())." "; ?><?php esc_html_e('In stock', 'vg-siva');?></span>
		<?php } else { ?>
			<span class="out-stock"><?php esc_html_e('Out of stock', 'vg-siva');?></span>
		<?php } ?>
	</div>
	<?php
}
add_action('woocommerce_single_product_summary', 'vg_siva_product_stock_status', 15);

//Column of Thumbnail Images*
function vg_siva_get_column_thumbnail_images(){
	global $vg_siva_options;
	$columnRelated = isset($vg_siva_options['column_thumbnail_images']) ? $vg_siva_options['column_thumbnail_images'] : '4';
	$slideSpeed = isset($vg_siva_options['slide_speed']) ? $vg_siva_options['slide_speed'] : '200';
	$paginationSpeed = isset($vg_siva_options['pagination_speed']) ? $vg_siva_options['pagination_speed'] : '800';
	$rewindSpeed = isset($vg_siva_options['rewind_speed']) ? $vg_siva_options['rewind_speed'] : '1000';
	$autoPlay = isset($vg_siva_options['auto_play']) ? $vg_siva_options['auto_play'] : 'false';
	$stopHover = isset($vg_siva_options['stop_hover']) ? $vg_siva_options['stop_hover'] : 'false';
	$mouseDrag = isset($vg_siva_options['mouse_drag']) ? $vg_siva_options['mouse_drag'] : 'false';
	$touchDrag = isset($vg_siva_options['touch_drag']) ? $vg_siva_options['touch_drag'] : 'false';

	$inlineJS = "
		jQuery(document).ready(function($) {
			$('.single-product-image .flex-control-thumbs').owlCarousel({
				items: 				".($columnRelated).",
				itemsDesktop: 		[1170,".($columnRelated - 1)."],
				itemsDesktopSmall: 	[980,".($columnRelated - 1)."],
				itemsTablet: 		[800,3],
				itemsTabletSmall: 	[650,3],
				itemsMobile: 		[599,2],
				slideSpeed: 		".($slideSpeed).",
				paginationSpeed: 	".($paginationSpeed).",
				rewindSpeed: 		".($rewindSpeed).",
				autoPlay: 			".(($autoPlay) ? 'true' : 'false').",
				stopOnHover: 		".(($stopHover) ? 'true' : 'false').",
				navigation: 		true,
				scrollPerPage: 		false,
				pagination: 		false,
				paginationNumbers: 	false,
				mouseDrag: 			".(($mouseDrag) ? 'true' : 'false').",
				touchDrag: 			".(($touchDrag) ? 'true' : 'false').",
				itemsCustom : 		false,
				navigationText: 	['".esc_html__("Prev","vg-siva")."', '".esc_html__("Next","vg-siva")."'],
				leftOffSet: 		-15,
			});
		});
	";
	wp_add_inline_script('vg-siva-js', $inlineJS);
}
add_action('wp_head', 'vg_siva_get_column_thumbnail_images');

// Change Product Buttons
function vg_siva_product_buttons_wishlist(){
	global $product;
	?>
	<?php if(class_exists('YITH_WCWL')) : ?>
		<div class="vgw-wishlist style-2">
			<?php echo preg_replace("/<img[^>]+\>/i", " ", do_shortcode('[yith_wcwl_add_to_wishlist]')); ?>
		</div>
	<?php endif; ?>
	<?php
}
add_action('woocommerce_before_single_product_summary', 'vg_siva_product_buttons_wishlist', 25);

function vg_siva_product_buttons(){
	global $product;
	?>
	<?php if(class_exists('YITH_Woocompare')){ ?>
	<div class="action-buttons">
		<div class="vgw-compare">
			<?php echo do_shortcode('[yith_compare_button]') ?>
		</div>
	</div>
	<?php } ?>
	<?php
}
add_action('woocommerce_after_add_to_cart_button', 'vg_siva_product_buttons', 10);
add_action('woocommerce_single_variation', 'vg_siva_product_buttons', 40);


//Display Product Single Tabs

//Display Up Sell
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
add_action('woocommerce_upsell_display_box', 'woocommerce_upsell_display', 15);

//Display Related Product
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
function vg_siva_single_related(){
	global $vg_siva_options;

	if(isset($vg_siva_options['related_products']) && $vg_siva_options['related_products']) {
		add_action('woocommerce_related_display_box', 'woocommerce_output_related_products', 20);
	}
}
add_action('init', 'vg_siva_single_related');

//Column Related Products
function vg_siva_get_column_related(){
	global $vg_siva_options;
	$slideSpeed = isset($vg_siva_options['slide_speed']) ? $vg_siva_options['slide_speed'] : '200';
	$paginationSpeed = isset($vg_siva_options['pagination_speed']) ? $vg_siva_options['pagination_speed'] : '800';
	$rewindSpeed = isset($vg_siva_options['rewind_speed']) ? $vg_siva_options['rewind_speed'] : '1000';
	$autoPlay = isset($vg_siva_options['auto_play']) ? $vg_siva_options['auto_play'] : 'false';
	$stopHover = isset($vg_siva_options['stop_hover']) ? $vg_siva_options['stop_hover'] : 'false';
	$mouseDrag = isset($vg_siva_options['mouse_drag']) ? $vg_siva_options['mouse_drag'] : 'false';
	$touchDrag = isset($vg_siva_options['touch_drag']) ? $vg_siva_options['touch_drag'] : 'false';

	$inlineJS = "
		jQuery(document).ready(function($) {
			$('.related .shop-products, .upsells .shop-products, .cross-sells .shop-products').owlCarousel({
				items: 				2,
				itemsDesktop: 		[1170,2],
				itemsDesktopSmall: 	[980,3],
				itemsTablet: 		[800,2],
				itemsTabletSmall: 	[650,2],
				itemsMobile: 		[479,1],
				slideSpeed: 		".($slideSpeed).",
				paginationSpeed: 	".($paginationSpeed).",
				rewindSpeed: 		".($rewindSpeed).",
				autoPlay: 			".(($autoPlay) ? 'true' : 'false').",
				stopOnHover: 		".(($stopHover) ? 'true' : 'false').",
				navigation: 		true,
				scrollPerPage: 		false,
				pagination: 		false,
				paginationNumbers: 	false,
				mouseDrag: 			".(($mouseDrag) ? 'true' : 'false').",
				touchDrag: 			".(($touchDrag) ? 'true' : 'false').",
				itemsCustom : 		false,
				navigationText: 	['".esc_html__("Prev","vg-siva")."', '".esc_html__("Next","vg-siva")."'],
				leftOffSet: 		-15,
			});
		});
	";
	wp_add_inline_script('vg-siva-js', $inlineJS);
}
add_action('wp_head', 'vg_siva_get_column_related');


//Change number of related products on product page. Set your own value for 'posts_per_page'
function vg_siva_woo_related_products_limit($args) {
	global $product, $vg_siva_options;

	$relatedAmount = isset($vg_siva_options['total_related_products']) ? $vg_siva_options['total_related_products'] : 6;
	$args['posts_per_page'] = $relatedAmount;

	return $args;
}
add_filter('woocommerce_output_related_products_args', 'vg_siva_woo_related_products_limit');

add_filter( 'max_srcset_image_width', create_function( '', 'return 1;' ) );
