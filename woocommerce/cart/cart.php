<?php
/**
 * Cart Page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if(! defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$vg_siva_options = get_option("vg_siva_options");
wc_print_notices();

do_action('woocommerce_before_cart'); ?>

<?php
$sidebar = 'none';
$colClass = 6;
$colClass2 = 3;
if(isset($vg_siva_options['default_blog_sidebar']) && $vg_siva_options['default_blog_sidebar']!=''){
	$sidebar = $vg_siva_options['default_blog_sidebar'];
	switch($sidebar) {
		case 'left':
			$colClass = 9;
			$colClass2 = 12;
			break;
		case 'right':
			$colClass = 9;
			$colClass2 = 12;
			break;
		default:
			break;
	}
}
$colContent = (is_active_sidebar('sidebar-1')) ? esc_attr($colClass) : 9;
$colContent2 = (is_active_sidebar('sidebar-1')) ? esc_attr($colClass2) : 12;
?>

<form class="cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
<?php do_action('woocommerce_before_cart_table'); ?>
<div class="container">
	<table class="shop_table cart" cellspacing="0">
		<thead>
			<tr>
				<th class="product-thumbnail"><?php esc_html_e('Products', 'vg-siva'); ?></th>
				<th class="product-name"><?php esc_html_e('Name of products', 'vg-siva'); ?></th>
				<th class="product-price"><?php esc_html_e('Price', 'vg-siva'); ?></th>
				<th class="product-quantity"><?php esc_html_e('Quantity', 'vg-siva'); ?></th>
				<th class="product-subtotal"><?php esc_html_e('Total', 'vg-siva'); ?></th>
				<th class="product-remove">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php do_action('woocommerce_before_cart_contents'); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

						<td class="product-thumbnail">
							<?php
								$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

								if ( ! $product_permalink ) {
									echo esc_url($thumbnail);
								} else {
									printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
								}
							?>
						</td>

						<td data-th="<?php esc_html_e('Product', 'vg-siva'); ?>" class="product-name">
							<?php
								if ( ! $product_permalink ) {
									echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
								} else {
									echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );
								}
								?>
								<div class="description">
									<?php
									$trimexcerpt = apply_filters('the_content', get_post_field('post_content', $product_id));
									 $words_short_des = 15;
									 $shortexcerpt = wp_trim_words( $trimexcerpt, $num_words = $words_short_des, $more = '...' );
									 echo ($shortexcerpt);
									?>
								</div>
								<?php
								// Meta data
								echo WC_GET_FORMATTED_CART_ITEM_DATA( $cart_item );

								// Backorder notification
								if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
									echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'vg-siva' ) . '</p>';
								}
							?>
						</td>

						<td data-th="<?php esc_html_e('Price', 'vg-siva'); ?>" class="product-price">
							<?php
								echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
							?>
						</td>

						<td data-th="<?php esc_html_e('Quantity', 'vg-siva'); ?>" class="product-quantity">
							<?php
								if ( $_product->is_sold_individually() ) {
									$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
								} else {
									$product_quantity = woocommerce_quantity_input( array(
										'input_name'  => "cart[{$cart_item_key}][qty]",
										'input_value' => $cart_item['quantity'],
										'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
										'min_value'   => '0',
									), $_product, false );
								}

								echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
							?>
						</td>

						<td data-th="<?php esc_html_e('Total', 'vg-siva'); ?>" class="product-subtotal">
							<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
							?>
						</td>

						<td class="product-remove">
							<?php
								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
									'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><i class="icon-trash icons" aria-hidden="true"></i></a>',
									esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
									__( 'Remove this item', 'vg-siva' ),
									esc_attr( $product_id ),
									esc_attr( $_product->get_sku() )
								), $cart_item_key );
							?>
						</td>
					</tr>
					<?php
				}
			}

			do_action('woocommerce_cart_contents');
			?>
			<?php do_action('woocommerce_after_cart_contents'); ?>
		</tbody>
	</table>
</div>
<?php if (isset($vg_siva_options['promotional_banner_cart']['url']) && $vg_siva_options['promotional_banner_cart']['url'] != "") { ?>
<div class="banner_cart">
	<img src="<?php echo esc_url($vg_siva_options['promotional_banner_cart']['url']) ?>" alt="promotional_banner_cart">
</div>
<?php } ?>

<div class="container">
	<div class="actions">
		<div class="row">

			<div class="col-xs-12 col-lg-3 buttons-cart">
				<input type="submit" class="button" name="update_cart" value="<?php esc_html_e('Update Cart', 'vg-siva'); ?>" />
				<a class="button continue" href="<?php echo get_permalink(wc_get_page_id('shop'));?>"><?php esc_html_e('Continue Shopping', 'vg-siva');?></a>

				<div class="clear clearfix"></div>

			</div>

			<div class="col-xs-12 col-md-6 col-lg-<?php echo esc_attr($colContent); ?>">
				<?php if(WC()->cart->coupons_enabled()) { ?>
					<div class="coupon">
						<h3><?php esc_html_e('Estimate your shipping cost', 'vg-siva');?></h3>
						<div class="form-coupon">
							<label for="coupon_code"><?php esc_html_e('Coupon code', 'vg-siva'); ?></label>
							<div class="clear clearfix"></div>
							<div class="wrap">
								<input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" />
								<input type="submit" class="button" name="apply_coupon" value="<?php esc_html_e('Apply Coupon', 'vg-siva'); ?>" />
							</div>
						</div>
						<?php do_action('woocommerce_cart_coupon'); ?>

					</div>
				<?php } ?>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-<?php echo esc_attr($colContent2); ?>">
				<div class="cart-total-wrapper">
					<div class="total-cost">
						<?php woocommerce_cart_totals(); ?>
					</div>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="cart-collaterals">
					<?php do_action('woocommerce_cart_collaterals'); ?>
				</div>
			</div>
		</div>

		<?php do_action('woocommerce_cart_actions'); ?>

		<?php wp_nonce_field('woocommerce-cart'); ?>
	</div>
</div>
<?php do_action('woocommerce_after_cart_table'); ?>
</form>


<?php do_action('woocommerce_after_cart'); ?>
