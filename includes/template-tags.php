<?php
/**
 * Custom VG Siva template tags
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package VG Siva
 */


if(! function_exists('vg_siva_entry_meta')) :
/**
 * Prints HTML with meta information for the categories, tags.
 *
 * Create your own vg_siva_entry_meta() function to override in a child theme.
 *
 */

function vg_siva_entry_meta() {
	
	if(in_array(get_post_type(), array('post', 'attachment'))) {
		vg_siva_entry_date();
	}
	if (has_post_thumbnail() && get_the_post_thumbnail() != NULL) {
		if('post' === get_post_type()) {
			vg_siva_entry_author();
		}
	}
	
	if (has_post_thumbnail() && get_the_post_thumbnail() != NULL) {
		vg_siva_entry_category(); 
	}
	
}
endif;

if(! function_exists('vg_siva_entry_meta_small')) :
/**
 * Prints HTML with meta information for the categories, tags.
 *
 * Create your own vg_siva_entry_meta_small() function to override in a child theme.
 *
 */

function vg_siva_entry_meta_small() {
	
	vg_siva_entry_author();
	
	vg_siva_entry_category(); 
	
}
endif;


if(! function_exists('vg_siva_entry_author')) :
/**
 * Prints HTML with date information for current author.
 *
 * Create your own vg_siva_entry_author() function to override in a child theme.
 *
 */
function vg_siva_entry_author() {
	printf('<span class="meta byline"><span class="author vcard"><span class="screen-reader-text">%1$s </span><span class="title">%1$s </span> <b><a class="url fn n" href="%2$s">%3$s</a></b></span></span>',
		_x('By', 'Used before post author name.', 'vg-siva'),
		get_author_posts_url(get_the_author_meta('ID')),
		get_the_author()
	);
}
endif;

if(! function_exists('vg_siva_entry_date')) :
/**
 * Prints HTML with date information for current post.
 *
 * Create your own vg_siva_entry_date() function to override in a child theme.
 *
 */
function vg_siva_entry_date() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

	if(get_the_time('U') !== get_the_modified_time('U')) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf($time_string,
		esc_attr(get_the_date('c')),
		get_the_date(),
		esc_attr(get_the_modified_date('c')),
		get_the_modified_date()
	);
	printf('<span class="meta posted-on"><span class="screen-reader-text">%1$s </span> <span class="title">%1$s</span> <b><a href="%2$s" rel="bookmark">%3$s</a></b></span>',
		_x('Posted', 'Used before publish date.', 'vg-siva'),
		esc_url(get_permalink()),
		$time_string
	);
	
}
endif;

if(! function_exists('vg_siva_entry_category')) :
/**
 * Prints HTML with post format for current post.
 *
 * Create your own vg_siva_entry_format() function to override in a child theme.
 *
 */
function vg_siva_entry_format() {
	$format = get_post_format();
	$icon='';
	//var_dump($vgpc_format);
	switch($format) {
		case 'aside':
			$icon = 'icon-aside';
			break;
		case 'image':
			$icon = 'icon-image';
			break;
		case 'video':
			$icon = 'icon-video';
			break;
		case 'audio':
			$icon = 'icon-audio';
			break;
		case 'quote':
			$icon = 'icon-quote';
			break;
		case 'link':
			$icon = 'icon-link';
			break;
		default : 
			break;
	}
	printf('<span class="meta entry-format '.esc_attr($icon).'"><span class="screen-reader-text">%1$s </span>%2$s</span>',
		_x('Categories', 'Used before post format names.', 'vg-siva'),
		$format
	);
}
endif;

if(! function_exists('vg_siva_entry_category')) :
/**
 * Prints HTML with category for current post.
 *
 * Create your own vg_siva_entry_category() function to override in a child theme.
 *
 */
function vg_siva_entry_category() {
	$categories_list = get_the_category_list(_x(', ', 'Used between list items, there is a space after the comma.', 'vg-siva'));
	if($categories_list && vg_siva_categorized_blog()) {
		printf('<span class="meta cat-links"> <span class="title"> %1$s </span> <span class="screen-reader-text">%1$s </span>%2$s</span>',
			_x('Categories', 'Used before category names.', 'vg-siva'),
			$categories_list
		);
	}
}
endif;

if(! function_exists('vg_siva_entry_tags')) :
/**
 * Prints HTML with tags for current post.
 *
 * Create your own vg_siva_entry_tags() function to override in a child theme.
 *
 */
function vg_siva_entry_tags() {
	$tags_list = get_the_tag_list('', _x(', ', 'Used between list items, there is a space after the comma.', 'vg-siva'));
	if($tags_list) {
		printf('<span class="tags-links"><span class="title"> %1$s</span> <span class="screen-reader-text">%1$s </span>%2$s</span>',
			_x('Tags', 'Used before tag names.', 'vg-siva'),
			$tags_list
		);
	}
}
endif;

if(! function_exists('vg_siva_entry_comments')) :
/**
 * Prints HTML with comment count for current post.
 *
 * Create your own vg_siva_entry_comments() function to override in a child theme.
 *
 */
function vg_siva_entry_comments() {
	if(! is_singular() && ! post_password_required() &&(comments_open() || get_comments_number())) {
		echo '<span class="meta comments-link"><i class="zmdi zmdi-comments"></i>';
		comments_popup_link(sprintf(__('Leave a comment<span class="screen-reader-text"> on %s</span>', 'vg-siva'), get_the_title()));
		echo '</span>';
	}
}
endif;

if(! function_exists('vg_siva_post_thumbnail')) :
/**
 * Displays an optional post thumbnail.
 *
 * Wraps the post thumbnail in an anchor element on index views, or a div
 * element when on single views.
 *
 * Create your own vg_siva_post_thumbnail() function to override in a child theme.
 *
 */
function vg_siva_post_thumbnail() {
	if(post_password_required() || is_attachment() || ! has_post_thumbnail()) {
		return;
	}
	$setMargin = '';
	if(get_the_post_thumbnail() == NULL){
		$setMargin = 'set-margin';
	}
	if(is_singular()) :
	?>
	<div class="post-thumbnail <?php echo esc_attr($setMargin); ?>">
		<?php the_post_thumbnail(); ?>
	</div><!-- .post-thumbnail -->

	<?php else : ?>
	<div class="post-thumbnail <?php echo esc_attr($setMargin); ?>">
		<a class="link-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
			<?php the_post_thumbnail('vg_siva_post_full', array('alt' => the_title_attribute('echo=0'))); ?>
		</a>
	</div><!-- .post-thumbnail -->

	<?php endif; // End is_singular()
}
endif;

if(! function_exists('vg_siva_excerpt')) :
	/**
	 * Displays the optional excerpt.
	 *
	 * Wraps the excerpt in a div element.
	 *
	 * Create your own vg_siva_excerpt() function to override in a child theme.
	 *
	 *
	 * @param string $class Optional. Class string of the div element. Defaults to 'entry-summary'.
	 */
	function vg_siva_excerpt($class = 'entry-summary') {
		$class = esc_attr($class);

		if(has_excerpt() || is_search()) : ?>
			<div class="<?php echo esc_attr($class); ?>">
				<?php the_excerpt(); ?>
			</div>
		<?php endif;
	}
endif;

if(! function_exists('vg_siva_excerpt_more') && ! is_admin()) :
/**
 * Replaces "[...]"(appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * Create your own vg_siva_excerpt_more() function to override in a child theme.
 *
 *
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function vg_siva_excerpt_more() {
	$link = sprintf('<a href="%1$s" class="more-link">%2$s</a>',
		get_permalink(get_the_ID()),
		/* translators: %s: Name of current post */
		sprintf(__('Read more <span class="screen-reader-text"> "%s"</span>', 'vg-siva'), get_the_title(get_the_ID()))
	);
	return ' &hellip; ' . $link;
}
add_filter('excerpt_more', 'vg_siva_excerpt_more');
endif;

if(! function_exists('vg_siva_categorized_blog')) :
/**
 * Determines whether blog/site has more than one category.
 *
 * Create your own vg_siva_categorized_blog() function to override in a child theme.
 *
 *
 * @return bool True if there is more than one category, false otherwise.
 */
function vg_siva_categorized_blog() {
	if(false === ($all_the_cool_cats = get_transient('vg_siva_categories'))) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories(array(
			'fields'     => 'ids',
			// We only need to know if there is more than one category.
			'number'     => 2,
		));

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count($all_the_cool_cats);

		set_transient('vg_siva_categories', $all_the_cool_cats);
	}

	if($all_the_cool_cats > 1) {
		// This blog has more than 1 category so vg_siva_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so vg_siva_categorized_blog should return false.
		return false;
	}
}
endif;

/**
 * Flushes out the transients used in vg_siva_categorized_blog().
 *
 */
function vg_siva_category_transient_flusher() {
	if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient('vg_siva_categories');
}
add_action('edit_category', 'vg_siva_category_transient_flusher');
add_action('save_post',     'vg_siva_category_transient_flusher');

if(! function_exists('vg_siva_the_custom_logo')) :
/**
 * Displays the optional custom logo.
 *
 * Does nothing if the custom logo is not available.
 *
 */
function vg_siva_the_custom_logo() {
	if(function_exists('the_custom_logo')) {
		the_custom_logo();
	}
}
endif;


//Change search form
function vg_siva_search_form($form) {
	if(get_search_query()!=''){
		$search_str = get_search_query();
	} else {
		$search_str = esc_html__('Search...', 'vg-siva');
	}
	
	$form = '<form role="search" method="get" id="blogsearchform" class="searchform" action="' . esc_url(home_url('/')). '" >
	<div class="form-input">
		<input class="input_text" type="text" value="'.esc_attr($search_str).'" name="s" id="search_input" />
		<button class="button" type="submit" id="blogsearchsubmit"><i class="fa fa-search"></i></button>
		<input type="hidden" name="post_type" value="post" />
		</div>
	</form>';
	
	$inlineJS = '
		jQuery(document).ready(function(){
			jQuery("#search_input").focus(function(){
				if(jQuery(this).val()=="'. esc_html__('Search...', 'vg-siva').'"){
					jQuery(this).val("");
				}
			});
			jQuery("#search_input").focusout(function(){
				if(jQuery(this).val()==""){
					jQuery(this).val("'. esc_html__('Search...', 'vg-siva').'");
				}
			});
			jQuery("#blogsearchsubmit").on("click", function(){
				if(jQuery("#search_input").val()=="'. esc_html__('Search...', 'vg-siva').'" || jQuery("#search_input").val()==""){
					jQuery("#search_input").focus();
					return false;
				}
			});
		});
	';
	wp_add_inline_script('vg-siva-js', $inlineJS);
	return $form;
} 
//add_action('wp_enqueue_scripts', 'vg_siva_search_form');
add_filter('get_search_form', 'vg_siva_search_form');


//Comment
if(! function_exists('vg_siva_comment')) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own vg_siva_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Warethemes 1.0
 */
function vg_siva_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php esc_html_e( 'Pingback:', 'vg-siva' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( '(Edit)', 'vg-siva' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<div class="comment-avatar">
				<?php echo get_avatar( $comment, 50 ); ?>
			</div>
			<div class="comment-info">
				<header class="comment-meta comment-author vcard">
					<?php
						
						printf( '<cite><b class="fn">%1$s</b> %2$s</cite>',
							get_comment_author_link(),
							// If current post author is also comment author, make it known visually.
							( $comment->user_id === $post->post_author ) ? '<span>' . '</span>' : ''
						);
					?>
					<div class="time-reply">
					<?php
						printf( '<time datetime="%1$s">%2$s</time>',
							get_comment_time( 'c' ),
							/* translators: 1: date, 2: time */
							sprintf( esc_html__( '%1$s at %2$s', 'vg-siva' ), get_comment_date(), get_comment_time() )
						);
						
					?>
					</div>
				</header><!-- .comment-meta -->
				<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'vg-siva' ); ?></p>
				<?php endif; ?>

				<section class="comment-content comment">
					<?php comment_text(); ?>
					<div class="reply-edit">
						<?php edit_comment_link( esc_html__( 'Edit', 'vg-siva' ), '', '' ); ?>
						
						<?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'vg-siva' ), 'after' => '', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					
					</div><!-- .reply -->
					
					
				</section><!-- .comment-content -->
			</div>
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // end comment_type check
}
endif;

if(! function_exists('before_comment_fields') &&  ! function_exists('after_comment_fields')) :

	//Change comment form
	function vg_siva_before_comment_fields() {
		echo '<div class="comment-input">';
	}
	add_action('comment_form_before_fields', 'vg_siva_before_comment_fields');

	function vg_siva_after_comment_fields() {
		echo '</div>';
	}
	add_action('comment_form_after_fields', 'vg_siva_after_comment_fields');

endif; 

function vg_siva_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}
add_filter( 'comment_form_fields', 'vg_siva_comment_field_to_bottom' );
 
function vg_siva_customize_register($wp_customize) {
	$wp_customize->get_setting('blogname')->transport         = 'postMessage';
	$wp_customize->get_setting('blogdescription')->transport  = 'postMessage';
	$wp_customize->get_setting('header_textcolor')->transport = 'postMessage';
}
add_action('customize_register', 'vg_siva_customize_register');

/**
 * Enqueue Javascript postMessage handlers for the Customizer.
 *
 * Binds JS handlers to make the Customizer preview reload changes asynchronously.
 *
 * @since Warethemes 1.0
 */
 
add_action('wp_enqueue_scripts', 'vg_siva_wcqi_enqueue_polyfill');
function vg_siva_wcqi_enqueue_polyfill() {
    wp_enqueue_script('wcqi-number-polyfill');
}


if ( ! function_exists( 'vg_siva_pagination' ) ) :
/* Pagination */
function vg_siva_pagination() {
	global $wp_query;

	$big = 999999999; // need an unlikely integer
 
	echo paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'prev_text'    => wp_kses(__('<i class="fa fa-chevron-left"></i>', 'vg-siva'), array('i' => array('class' => array()))),
		'next_text'    => wp_kses(__('<i class="fa fa-chevron-right"></i>', 'vg-siva'), array('i' => array('class' => array()))),
	) );
}
endif;
