<?php
if(! function_exists('vg_siva_custom_styles'))
{
	function vg_siva_custom_styles()
	{
		$vg_siva_options 	= get_option("vg_siva_options");

		ob_start();
		?>
		<?php if(isset($vg_siva_options['typography_customize']) && $vg_siva_options['typography_customize']): ?>
			body{
				font-family:
				<?php if($vg_siva_options['font_source'] == "3") echo '\'' . $vg_siva_options['main_typekit_font_face'] . '\','; ?>
				<?php if($vg_siva_options['font_source'] == "2") echo '\'' . $vg_siva_options['main_google_font_face'] . '\','; ?>
				<?php if($vg_siva_options['font_source'] == "1") echo '\'' . $vg_siva_options['main_font']['font-family'] . '\','; ?>
				<?php if($vg_siva_options['font_source'] == "3") echo '\'' . $vg_siva_options['secondary_typekit_font_face'] . '\','; ?>
				<?php if($vg_siva_options['font_source'] == "2") echo '\'' . $vg_siva_options['secondary_google_font_face'] . '\','; ?>
				<?php if($vg_siva_options['font_source'] == "1") echo '\'' . $vg_siva_options['secondary_font']['font-family'] . '\','; ?>
				sans-serif;
			}
		<?php endif; ?>

		<?php if(isset($vg_siva_options['styling_customize']) && $vg_siva_options['styling_customize']): ?>
		/* Common Css */
		body{
			color:<?php echo ($vg_siva_options['body_color']); ?>;
			background-color: <?php echo ($vg_siva_options['main_background']['background-color']); ?>;
			background-image: url('<?php echo ($vg_siva_options['main_background']['background-image']); ?>');
			background-repeat: <?php echo ($vg_siva_options['main_background']['background-repeat']); ?>;
			background-size: <?php echo ($vg_siva_options['main_background']['background-size']); ?>;
			background-attachment: <?php echo ($vg_siva_options['main_background']['background-attachment']); ?>;
			background-position: <?php echo ($vg_siva_options['main_background']['background-position']); ?>;
		}
		.vg-website-wrapper a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .main-navigation ul ul {
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .main-navigation ul ul ul li a:hover:before {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .main-navigation .mega-menu > ul > li > a:before {
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper #loader {
		  border-top-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper #loader:before {
		  border-top-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper #loader:after {
		  border-top-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .owl-theme .owl-controls .owl-page.active span,
		.vg-website-wrapper .owl-theme .owl-controls .owl-page:hover span {
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .owl-theme .owl-controls .owl-page.active span:after,
		.vg-website-wrapper .owl-theme .owl-controls .owl-page:hover span:after {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .owl-theme .owl-controls .owl-buttons div:hover {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .visible-buttons .owl-controls .owl-buttons div:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .position-buttons .owl-controls .owl-buttons div:hover {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?> !important;
		}
		.vg-website-wrapper .atc-notice-wrapper .close:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .atc-notice-wrapper .product-info .price .special-price {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .atc-notice-wrapper .buttons .button {
		  background: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .quantity .minus,
		.vg-website-wrapper .quantity .plus {
		  background: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .quantity .minus,
		.vg-website-wrapper .quantity .plus {
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .product-rating {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .product-rating .star-rating:before {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .product-rating .star-rating span {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .widget-title h3:before {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .widget ul li a:hover,
		.vg-website-wrapper .widget ol li a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .widget ul li.chosen a:hover,
		.vg-website-wrapper .widget ol li.chosen a:hover {
		  background: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .tagcloud a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .widget_rss ul li .rsswidget {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .widget_product_categories ul li a:hover,
		.vg-website-wrapper .widget_categories ul li a:hover,
		.vg-website-wrapper .widget_nav_menu ul li a:hover,
		.vg-website-wrapper .widget_archive ul li a:hover,
		.vg-website-wrapper .widget_meta ul li a:hover,
		.vg-website-wrapper .widget_recent_comments ul li a:hover,
		.vg-website-wrapper .widget_recent_entries ul li a:hover,
		.vg-website-wrapper .widget_pages ul li a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .widget_product_categories ul li.current-menu-item,
		.vg-website-wrapper .widget_categories ul li.current-menu-item,
		.vg-website-wrapper .widget_nav_menu ul li.current-menu-item,
		.vg-website-wrapper .widget_archive ul li.current-menu-item,
		.vg-website-wrapper .widget_meta ul li.current-menu-item,
		.vg-website-wrapper .widget_recent_comments ul li.current-menu-item,
		.vg-website-wrapper .widget_recent_entries ul li.current-menu-item,
		.vg-website-wrapper .widget_pages ul li.current-menu-item,
		.vg-website-wrapper .widget_product_categories ul li.current-cat,
		.vg-website-wrapper .widget_categories ul li.current-cat,
		.vg-website-wrapper .widget_nav_menu ul li.current-cat,
		.vg-website-wrapper .widget_archive ul li.current-cat,
		.vg-website-wrapper .widget_meta ul li.current-cat,
		.vg-website-wrapper .widget_recent_comments ul li.current-cat,
		.vg-website-wrapper .widget_recent_entries ul li.current-cat,
		.vg-website-wrapper .widget_pages ul li.current-cat,
		.vg-website-wrapper .widget_product_categories ul li.cat-parent,
		.vg-website-wrapper .widget_categories ul li.cat-parent,
		.vg-website-wrapper .widget_nav_menu ul li.cat-parent,
		.vg-website-wrapper .widget_archive ul li.cat-parent,
		.vg-website-wrapper .widget_meta ul li.cat-parent,
		.vg-website-wrapper .widget_recent_comments ul li.cat-parent,
		.vg-website-wrapper .widget_recent_entries ul li.cat-parent,
		.vg-website-wrapper .widget_pages ul li.cat-parent,
		.vg-website-wrapper .widget_product_categories ul li:hover,
		.vg-website-wrapper .widget_categories ul li:hover,
		.vg-website-wrapper .widget_nav_menu ul li:hover,
		.vg-website-wrapper .widget_archive ul li:hover,
		.vg-website-wrapper .widget_meta ul li:hover,
		.vg-website-wrapper .widget_recent_comments ul li:hover,
		.vg-website-wrapper .widget_recent_entries ul li:hover,
		.vg-website-wrapper .widget_pages ul li:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .widget_product_categories ul li.current-menu-item > a,
		.vg-website-wrapper .widget_categories ul li.current-menu-item > a,
		.vg-website-wrapper .widget_nav_menu ul li.current-menu-item > a,
		.vg-website-wrapper .widget_archive ul li.current-menu-item > a,
		.vg-website-wrapper .widget_meta ul li.current-menu-item > a,
		.vg-website-wrapper .widget_recent_comments ul li.current-menu-item > a,
		.vg-website-wrapper .widget_recent_entries ul li.current-menu-item > a,
		.vg-website-wrapper .widget_pages ul li.current-menu-item > a,
		.vg-website-wrapper .widget_product_categories ul li.current-cat > a,
		.vg-website-wrapper .widget_categories ul li.current-cat > a,
		.vg-website-wrapper .widget_nav_menu ul li.current-cat > a,
		.vg-website-wrapper .widget_archive ul li.current-cat > a,
		.vg-website-wrapper .widget_meta ul li.current-cat > a,
		.vg-website-wrapper .widget_recent_comments ul li.current-cat > a,
		.vg-website-wrapper .widget_recent_entries ul li.current-cat > a,
		.vg-website-wrapper .widget_pages ul li.current-cat > a,
		.vg-website-wrapper .widget_product_categories ul li.cat-parent > a,
		.vg-website-wrapper .widget_categories ul li.cat-parent > a,
		.vg-website-wrapper .widget_nav_menu ul li.cat-parent > a,
		.vg-website-wrapper .widget_archive ul li.cat-parent > a,
		.vg-website-wrapper .widget_meta ul li.cat-parent > a,
		.vg-website-wrapper .widget_recent_comments ul li.cat-parent > a,
		.vg-website-wrapper .widget_recent_entries ul li.cat-parent > a,
		.vg-website-wrapper .widget_pages ul li.cat-parent > a,
		.vg-website-wrapper .widget_product_categories ul li:hover > a,
		.vg-website-wrapper .widget_categories ul li:hover > a,
		.vg-website-wrapper .widget_nav_menu ul li:hover > a,
		.vg-website-wrapper .widget_archive ul li:hover > a,
		.vg-website-wrapper .widget_meta ul li:hover > a,
		.vg-website-wrapper .widget_recent_comments ul li:hover > a,
		.vg-website-wrapper .widget_recent_entries ul li:hover > a,
		.vg-website-wrapper .widget_pages ul li:hover > a {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .widget_recent_comments ul li a {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .woocommerce.widget_price_filter .price_slider_amount .button {
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .woocommerce.widget_price_filter .price_slider_amount .button {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .yith-woocompare-widget a.compare {
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .yith-woocompare-widget a.compare {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .widget_product_categories ul li > a:before {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .sidebar-category .widget_layered_nav ul li > a:hover,
		.vg-website-wrapper .sidebar-category .widget_categories ul li > a:hover,
		.vg-website-wrapper .sidebar-category .widget_nav_menu ul li > a:hover,
		.vg-website-wrapper .sidebar-category .widget_layered_nav ol li > a:hover,
		.vg-website-wrapper .sidebar-category .widget_categories ol li > a:hover,
		.vg-website-wrapper .sidebar-category .widget_nav_menu ol li > a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .tools_button:hover {
		  background: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .sl-text b {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .tparrows:hover:before {
		  color: <?php echo ($vg_siva_options['main_color']); ?> !important;
		}
		.vg-website-wrapper div.vg-siva-category-treeview .treeview li a:before {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .wpb_heading.style-3:before {
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .woocommerce ul.cart_list li .product-image .quantity,
		.vg-website-wrapper .woocommerce ul.product_list_widget li .product-image .quantity {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .woocommerce ul.cart_list li .amount:hover,
		.vg-website-wrapper .woocommerce ul.product_list_widget li .amount:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .main-navigation ul li.current-menu-parent a,
		.vg-website-wrapper .main-navigation ul li.current-menu-item a {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .main-navigation ul ul li.current-menu-item a {
		  color: <?php echo ($vg_siva_options['main_color']); ?> !important;
		}
		.vg-website-wrapper .main-navigation ul ul li a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?> !important;
		}
		.vg-website-wrapper .vgw-testimonial.owl-theme.position-buttons .owl-controls .owl-buttons div:hover,
		.vg-website-wrapper .client-info .rating,
		.vg-website-wrapper .vc_welcome .item-rating,
		.vg-website-wrapper .vgw-item-i .product-title a:hover,
		.vg-website-wrapper .vgw-item-i .button-group a:hover,
		.vg-website-wrapper .vgw-item-i .button-group .add-to-cart a.added_to_cart,
		.vg-website-wrapper .vgw-item-i .button-group .add-to-cart a.wc-forward {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .vgw-item-i .button-group .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .vgw-item-i .button-group .yith-wcwl-wishlistaddedbrowse a,
		.vg-website-wrapper .vgw-item-i .button-group .yith-wcwl-wishlistaddedbrowse.show a,
		.vg-website-wrapper .vgw-item-i .button-group .vgw-compare a.added {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .wpb_row .vc_tta.vc_general .vc_tta-tab.vc_active > a,
		.vg-website-wrapper .wpb_row .vc_tta.vc_general .vc_tta-tab:hover > a {
		  color: <?php echo ($vg_siva_options['main_color']); ?> !important;
		}
		.vg-website-wrapper .item-i .post-title a:hover,
		.vg-website-wrapper .vgp-item-i .post-title a:hover,
		.vg-website-wrapper .vgp-item-i.style-3 .post-title a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper #crumbs a:hover,
		.vg-website-wrapper #breadcrumbs a:hover,
		.vg-website-wrapper #crumbs span:hover,
		.vg-website-wrapper #breadcrumbs span:hover,
		.vg-website-wrapper #crumbs li:last-child,
		.vg-website-wrapper #breadcrumbs li:last-child,
		.vg-website-wrapper #crumbs .current,
		.vg-website-wrapper #breadcrumbs .current {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .shop-products.list-view .vgw-item-i .button-group a:hover {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .shop-products.list-view .vgw-item-i .button-group .add-to-cart a:hover,
		.vg-website-wrapper .shop-products.list-view .vgw-item-i .button-group .add-to-cart a.added_to_cart,
		.vg-website-wrapper .shop-products.list-view .vgw-item-i .button-group .add-to-cart a.wc-forward {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .shop-products.list-view .vgw-item-i .button-group .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .shop-products.list-view .vgw-item-i .button-group .yith-wcwl-wishlistaddedbrowse a {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .shop-products.list-view .vgw-item-i .button-group .yith-wcwl-wishlistaddedbrowse.show a {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .shop-products.list-view .vgw-item-i .button-group .vgw-compare a.added {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .all-subcategories li h3:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .toolbar .woocommerce-pagination ul.page-numbers li a.current,
		.vg-website-wrapper .toolbar .woocommerce-pagination ul.page-numbers li span.current,
		.vg-website-wrapper .toolbar .woocommerce-pagination ul.page-numbers li a:hover,
		.vg-website-wrapper .toolbar .woocommerce-pagination ul.page-numbers li span:hover {
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  background: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .single-product-image .vgw-wishlist.style-2 a:hover {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .single-product-image .vgw-wishlist.style-2 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .single-product-image .vgw-wishlist.style-2 .yith-wcwl-wishlistaddedbrowse a {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .single-product-image .vgw-wishlist.style-2 .yith-wcwl-wishlistaddedbrowse.show a {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .single-product-info .woocommerce-product-rating,
		.vg-website-wrapper .single-product-info .woocommerce-product-rating .star-rating:before,
		.vg-website-wrapper .single-product-info .woocommerce-product-rating .star-rating span {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .single-product-info .cart .single_add_to_cart_button.alt:hover {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .single-product-info .cart .single_add_to_cart_button.disabled:hover {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?> !important;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .single-product-info .action-buttons a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .single-product-info .action-buttons .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .single-product-info .action-buttons .vgw-compare a.added {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .single-product-info .product_meta a:hover,
		.vg-website-wrapper .single-product-info .product_meta span.line:hover,
		.vg-website-wrapper .single-product-info .product_meta .sku:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .woocommerce div.product .woocommerce-tabs ul.tabs li.active a,
		.vg-website-wrapper .woocommerce div.product .woocommerce-tabs ul.tabs li:hover a {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .post .entry-meta a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .st-feature-post.visible-buttons .owl-controls .owl-buttons div:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .st-testimonial .inside-section {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .all-posts .page-title {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .wpcf7-form .wpcf7-submit {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .faqs-page .vc_tta.vc_general.vc_tta-accordion .vc_tta-panel.vc_active .vc_tta-panel-heading .vc_tta-panel-title > a {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .question-form .wpcf7-form .wpcf7-submit:hover {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .page-404 .button:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .socails-icon li a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .filter-options .btn.active,
		.vg-website-wrapper .filter-options .btn:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .filter-options .btn.active:before,
		.vg-website-wrapper .filter-options .btn:hover:before,
		.vg-website-wrapper .filter-options .btn.active:after,
		.vg-website-wrapper .filter-options .btn:hover:after {
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .filter-options .btn.active span:before,
		.vg-website-wrapper .filter-options .btn:hover span:before,
		.vg-website-wrapper .filter-options .btn.active span:after,
		.vg-website-wrapper .filter-options .btn:hover span:after {
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .woocommerce table.shop_table .product-name a {
		  color: <?php echo ($vg_siva_options['main_color']); ?> !important;
		}
		.vg-website-wrapper .woocommerce .quality_text {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .woocommerce .actions .coupon .button:hover {
		  background: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .woocommerce .actions .cart-total-wrapper .total-cost .cart_totals .wc-proceed-to-checkout a.checkout-button:hover {
		  background: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .woocommerce .cart-collaterals .cart_totals h2 a:hover,
		.vg-website-wrapper .woocommerce .cart-collaterals .shipping_calculator h2 a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper #customer_login form.register input.button {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper #customer_login .lost_password a {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper h2.recent-orders-title {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .info-box h2,
		.vg-website-wrapper .info-box h2.title {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .woocommerce table.wishlist_table .product-add-to-cart a:hover {
		  background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		/* Update New */
		.vg-website-wrapper .main-navigation ul li a:hover {
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .main-navigation ul li.current-menu-parent a,
		.vg-website-wrapper .main-navigation ul li.current-menu-item a {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		  border-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .main-navigation ul ul li.current-menu-item a {
		  color: <?php echo ($vg_siva_options['main_color']); ?> !important;
		}
		.vg-website-wrapper .main-navigation ul ul li a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?> !important;
		}
		.vg-website-wrapper #vg-footer-wrapper .widget ul li a:hover,
		.vg-website-wrapper #vg-footer-wrapper .widget ol li a:hover {
		  color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .search-icon:hover {
			color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .ft-newsletters .widget_wysija_cont .wysija-submit{
			background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .ft-newsletters .widget_wysija_cont .wysija-submit:hover{
			background-color: #fff;
		}
		.vg-website-wrapper .darken .widget_product_categories ul li.current-menu-item, .vg-website-wrapper .darken .widget_categories ul li.current-menu-item, .vg-website-wrapper .darken .widget_nav_menu ul li.current-menu-item, .vg-website-wrapper .darken .widget_archive ul li.current-menu-item, .vg-website-wrapper .darken .widget_meta ul li.current-menu-item, .vg-website-wrapper .darken .widget_recent_comments ul li.current-menu-item, .vg-website-wrapper .darken .widget_recent_entries ul li.current-menu-item, .vg-website-wrapper .darken .widget_pages ul li.current-menu-item, .vg-website-wrapper .darken .widget_product_categories ul li.current-cat, .vg-website-wrapper .darken .widget_categories ul li.current-cat, .vg-website-wrapper .darken .widget_nav_menu ul li.current-cat, .vg-website-wrapper .darken .widget_archive ul li.current-cat, .vg-website-wrapper .darken .widget_meta ul li.current-cat, .vg-website-wrapper .darken .widget_recent_comments ul li.current-cat, .vg-website-wrapper .darken .widget_recent_entries ul li.current-cat, .vg-website-wrapper .darken .widget_pages ul li.current-cat, .vg-website-wrapper .darken .widget_product_categories ul li.cat-parent, .vg-website-wrapper .darken .widget_categories ul li.cat-parent, .vg-website-wrapper .darken .widget_nav_menu ul li.cat-parent, .vg-website-wrapper .darken .widget_archive ul li.cat-parent, .vg-website-wrapper .darken .widget_meta ul li.cat-parent, .vg-website-wrapper .darken .widget_recent_comments ul li.cat-parent, .vg-website-wrapper .darken .widget_recent_entries ul li.cat-parent, .vg-website-wrapper .darken .widget_pages ul li.cat-parent, .vg-website-wrapper .darken .widget_product_categories ul li:hover, .vg-website-wrapper .darken .widget_categories ul li:hover, .vg-website-wrapper .darken .widget_nav_menu ul li:hover, .vg-website-wrapper .darken .widget_archive ul li:hover, .vg-website-wrapper .darken .widget_meta ul li:hover, .vg-website-wrapper .darken .widget_recent_comments ul li:hover, .vg-website-wrapper .darken .widget_recent_entries ul li:hover, .vg-website-wrapper .darken .widget_pages ul li:hover{
			 color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .darken .widget ul li a:hover, .vg-website-wrapper .darken .widget ol li a:hover,
		.vg-website-wrapper .darken .widget_product_categories ul li.current-menu-item > a, .vg-website-wrapper .darken .widget_categories ul li.current-menu-item > a, .vg-website-wrapper .darken .widget_nav_menu ul li.current-menu-item > a, .vg-website-wrapper .darken .widget_archive ul li.current-menu-item > a, .vg-website-wrapper .darken .widget_meta ul li.current-menu-item > a, .vg-website-wrapper .darken .widget_recent_comments ul li.current-menu-item > a, .vg-website-wrapper .darken .widget_recent_entries ul li.current-menu-item > a, .vg-website-wrapper .darken .widget_pages ul li.current-menu-item > a, .vg-website-wrapper .darken .widget_product_categories ul li.current-cat > a, .vg-website-wrapper .darken .widget_categories ul li.current-cat > a, .vg-website-wrapper .darken .widget_nav_menu ul li.current-cat > a, .vg-website-wrapper .darken .widget_archive ul li.current-cat > a, .vg-website-wrapper .darken .widget_meta ul li.current-cat > a, .vg-website-wrapper .darken .widget_recent_comments ul li.current-cat > a, .vg-website-wrapper .darken .widget_recent_entries ul li.current-cat > a, .vg-website-wrapper .darken .widget_pages ul li.current-cat > a, .vg-website-wrapper .darken .widget_product_categories ul li.cat-parent > a, .vg-website-wrapper .darken .widget_categories ul li.cat-parent > a, .vg-website-wrapper .darken .widget_nav_menu ul li.cat-parent > a, .vg-website-wrapper .darken .widget_archive ul li.cat-parent > a, .vg-website-wrapper .darken .widget_meta ul li.cat-parent > a, .vg-website-wrapper .darken .widget_recent_comments ul li.cat-parent > a, .vg-website-wrapper .darken .widget_recent_entries ul li.cat-parent > a, .vg-website-wrapper .darken .widget_pages ul li.cat-parent > a, .vg-website-wrapper .darken .widget_product_categories ul li:hover > a, .vg-website-wrapper .darken .widget_categories ul li:hover > a, .vg-website-wrapper .darken .widget_nav_menu ul li:hover > a, .vg-website-wrapper .darken .widget_archive ul li:hover > a, .vg-website-wrapper .darken .widget_meta ul li:hover > a, .vg-website-wrapper .darken .widget_recent_comments ul li:hover > a, .vg-website-wrapper .darken .widget_recent_entries ul li:hover > a, .vg-website-wrapper .darken .widget_pages ul li:hover > a{
			 color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .header-style-3 .top-account .sign-in{
			background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .header-style-3 .top-account .sign-in:hover{
			background-color: #f0412b;
		}
		.vg-website-wrapper .darken .main-navigation ul li.current-menu-parent a, .vg-website-wrapper .darken .main-navigation ul li.current-menu-item a{
			color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .darken .main-navigation ul li a:hover{
			color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .darken .main-navigation.style-2 ul li a:hover{
			background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .darken .main-navigation.style-2 ul li.current-menu-parent a, .vg-website-wrapper .darken .main-navigation.style-2 ul li.current-menu-item a{
			background-color: <?php echo ($vg_siva_options['main_color']); ?>;
		}
		.vg-website-wrapper .darken .main-navigation.style-2 ul li a:hover,
		.vg-website-wrapper .darken .main-navigation.style-2 ul li.current-menu-parent a, .vg-website-wrapper .darken .main-navigation.style-2 ul li.current-menu-item a{
			color:#fff;
		}
		<?php endif; ?>

		<?php if(isset($vg_siva_options['header_bar_customize']) && $vg_siva_options['header_bar_customize']): ?>
		.vg-website-wrapper #vg-header-wrapper{
			color:<?php echo ($vg_siva_options['header_bar_text']); ?>;
			background-color: <?php echo ($vg_siva_options['header_bar_background']['background-color']); ?> !important;
			background-image: url('<?php echo ($vg_siva_options['header_bar_background']['background-image']); ?>') !important;
			background-repeat: <?php echo ($vg_siva_options['header_bar_background']['background-repeat']); ?> !important;
			background-size: <?php echo ($vg_siva_options['header_bar_background']['background-size']); ?> !important;
			background-attachment: <?php echo ($vg_siva_options['header_bar_background']['background-attachment']); ?> !important;
			background-position: <?php echo ($vg_siva_options['header_bar_background']['background-position']); ?> !important;
		}
		.top-bar, .site-header, .site-navigation , .site-header.darken{
			background-color:transparent;
		}
		.header-style-5 .site-navigation{
			background-color:transparent;
		}
		#logo-wrapper img{
			height:<?php echo ($vg_siva_options['logo_height']); ?>px !important;
		}
		.logo-background{
			width:<?php echo ($vg_siva_options['logo_width']); ?>px;
			height:<?php echo ($vg_siva_options['logo_height']); ?>px;
		}
		/* Update New */
		.vg-website-wrapper #vg-header-wrapper a:hover {
		  color: <?php echo ($vg_siva_options['header_bar_link_hover_color']); ?>;
		}
		.main-site-page.contact-page .site-content{
		    background: url("<?php echo ($vg_siva_options['bg_img_contact_pg']); ?>;") center bottom no-repeat;
		}		
		.vg-website-wrapper #vg-header-wrapper .top-bar{
			background-color:transparent;
		}
		.vg-website-wrapper .main-navigation ul li a{
			color: <?php echo ($vg_siva_options['header_bar_link_color']); ?>
		}
		.vg-website-wrapper .main-navigation ul li.home a:before{
			color: <?php echo ($vg_siva_options['header_bar_link_color']); ?>
		}
		.vg-website-wrapper .main-navigation ul li.current-menu-parent a, .vg-website-wrapper .main-navigation ul li.current-menu-item a{
			color: <?php echo ($vg_siva_options['header_bar_link_hover_color']); ?>;
			border-color: <?php echo ($vg_siva_options['header_bar_link_hover_color']); ?>;
		}
		.vg-website-wrapper .mini_cart_inner .top-cart-title a{
			border-color: <?php echo ($vg_siva_options['header_bar_text']); ?>;
			color: <?php echo ($vg_siva_options['header_bar_text']); ?>;
		}
		.vg-website-wrapper .mini_cart_inner .top-cart-title a:hover{
			color: <?php echo ($vg_siva_options['header_bar_link_hover_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .widget,
		.vg-website-wrapper #vg-header-wrapper .darken .widget{
			color: <?php echo ($vg_siva_options['header_bar_text']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .widget ul li a,
		.vg-website-wrapper #vg-header-wrapper .widget ol li a{
			color: <?php echo ($vg_siva_options['header_bar_link_color']); ?>
		}

		.text-banner_color{
			color: <?php echo ($vg_siva_options['banner_text']); ?>
		}

		.vg-website-wrapper #vg-header-wrapper .widget_product_categories ul li a,
		.vg-website-wrapper #vg-header-wrapper .widget_categories ul li a,
		.vg-website-wrapper #vg-header-wrapper .widget_nav_menu ul li a,
		.vg-website-wrapper #vg-header-wrapper .widget_archive ul li a,
		.vg-website-wrapper #vg-header-wrapper .widget_meta ul li a,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_comments ul li a,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_entries ul li a,
		.vg-website-wrapper #vg-header-wrapper .widget_pages ul li a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_product_categories ul li,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_categories ul li,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_nav_menu ul li,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_archive ul li,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_meta ul li,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_recent_comments ul li,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_recent_entries ul li,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_pages ul li{
			color: <?php echo ($vg_siva_options['header_bar_link_color']); ?>
		}
		.vg-website-wrapper #vg-header-wrapper .widget_product_categories ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_categories ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_nav_menu ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_archive ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_meta ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_comments ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_entries ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_pages ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .darken .widget ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .darken .widget ol li a:hover,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_product_categories ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_categories ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_nav_menu ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_archive ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_meta ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_recent_comments ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_recent_entries ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_pages ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_product_categories ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_categories ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_nav_menu ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_archive ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_meta ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_recent_comments ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_recent_entries ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_pages ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_product_categories ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_categories ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_nav_menu ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_archive ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_meta ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_recent_comments ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_recent_entries ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_pages ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_product_categories ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_categories ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_nav_menu ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_archive ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_meta ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_recent_comments ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_recent_entries ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .darken .widget_pages ul li:hover > a{
			color: <?php echo ($vg_siva_options['header_bar_link_hover_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .widget_product_categories ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .widget_categories ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .widget_nav_menu ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .widget_archive ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .widget_meta ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_comments ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_entries ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .widget_pages ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .widget_product_categories ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .widget_categories ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .widget_nav_menu ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .widget_archive ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .widget_meta ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_comments ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_entries ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .widget_pages ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .widget_product_categories ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .widget_categories ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .widget_nav_menu ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .widget_archive ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .widget_meta ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_comments ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_entries ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .widget_pages ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .widget_product_categories ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_categories ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_nav_menu ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_archive ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_meta ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_comments ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_recent_entries ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .widget_pages ul li:hover{
			color: <?php echo ($vg_siva_options['header_bar_link_hover_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .darken .main-navigation.style-2 ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .darken .main-navigation.style-2 ul li.current-menu-parent a,
		.vg-website-wrapper #vg-header-wrapper .darken .main-navigation.style-2 ul li.current-menu-item a{
			color:#fff;
		}
		<?php endif; ?>

		<?php if(isset($vg_siva_options['top_bar_customize']) && $vg_siva_options['top_bar_customize']): ?>
		.vg-website-wrapper #vg-header-wrapper .top-bar{
			color:<?php echo ($vg_siva_options['top_bar_text']); ?>;
			background-color: <?php echo ($vg_siva_options['top_bar_background']['background-color']); ?> !important;
			background-image: url('<?php echo ($vg_siva_options['top_bar_background']['background-image']); ?>') !important;
			background-repeat: <?php echo ($vg_siva_options['top_bar_background']['background-repeat']); ?> !important;
			background-size: <?php echo ($vg_siva_options['top_bar_background']['background-size']); ?> !important;
			background-attachment: <?php echo ($vg_siva_options['top_bar_background']['background-attachment']); ?> !important;
			background-position: <?php echo ($vg_siva_options['top_bar_background']['background-position']); ?> !important;
		}
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget{
			color: <?php echo ($vg_siva_options['top_bar_text']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .widget ul li a,
		.vg-website-wrapper #vg-header-wrapper .widget ol li a{
			color: <?php echo ($vg_siva_options['top_bar_link_color']); ?>
		}

		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_product_categories ul li a,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_categories ul li a,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_nav_menu ul li a,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_archive ul li a,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_meta ul li a,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_comments ul li a,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_entries ul li a,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_pages ul li a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_product_categories ul li,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_categories ul li,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_nav_menu ul li,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_archive ul li,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_meta ul li,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_recent_comments ul li,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_recent_entries ul li,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_pages ul li{
			color: <?php echo ($vg_siva_options['top_bar_link_color']); ?>
		}
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_product_categories ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_categories ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_nav_menu ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_archive ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_meta ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_comments ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_entries ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_pages ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget ol li a:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_product_categories ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_categories ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_nav_menu ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_archive ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_meta ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_recent_comments ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_recent_entries ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_pages ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_product_categories ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_categories ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_nav_menu ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_archive ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_meta ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_recent_comments ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_recent_entries ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_pages ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_product_categories ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_categories ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_nav_menu ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_archive ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_meta ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_recent_comments ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_recent_entries ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_pages ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_product_categories ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_categories ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_nav_menu ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_archive ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_meta ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_recent_comments ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_recent_entries ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_pages ul li:hover > a{
			color: <?php echo ($vg_siva_options['top_bar_link_hover_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_product_categories ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_categories ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_nav_menu ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_archive ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_meta ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_comments ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_entries ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_pages ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_product_categories ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_categories ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_nav_menu ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_archive ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_meta ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_comments ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_entries ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_pages ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_product_categories ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_categories ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_nav_menu ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_archive ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_meta ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_comments ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_entries ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_pages ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_product_categories ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_categories ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_nav_menu ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_archive ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_meta ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_comments ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_entries ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_pages ul li:hover{
			color: <?php echo ($vg_siva_options['top_bar_link_hover_color']); ?>;
		}
		<?php endif; ?>

		<?php if(isset($vg_siva_options['menu_bar_customize']) && $vg_siva_options['menu_bar_customize']): ?>
		.vg-website-wrapper #vg-header-wrapper .site-navigation{
			color:<?php echo ($vg_siva_options['menu_bar_text_color']); ?>;
			background-color: <?php echo ($vg_siva_options['menu_bar_background']['background-color']); ?> !important;
			background-image: url('<?php echo ($vg_siva_options['menu_bar_background']['background-image']); ?>') !important;
			background-repeat: <?php echo ($vg_siva_options['menu_bar_background']['background-repeat']); ?> !important;
			background-size: <?php echo ($vg_siva_options['menu_bar_background']['background-size']); ?> !important;
			background-attachment: <?php echo ($vg_siva_options['menu_bar_background']['background-attachment']); ?> !important;
			background-position: <?php echo ($vg_siva_options['menu_bar_background']['background-position']); ?> !important;
		}
		.vg-website-wrapper #vg-header-wrapper .site-navigation .main-navigation ul li a{
			color: <?php echo ($vg_siva_options['menu_bar_link_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .site-navigation .main-navigation ul li.home a:before{
			color: <?php echo ($vg_siva_options['menu_bar_text_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .site-navigation .main-navigation ul li a:hover{
			color: <?php echo ($vg_siva_options['menu_bar_link_hover_color']); ?>;
			border-color: <?php echo ($vg_siva_options['menu_bar_link_hover_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .site-navigation .main-navigation ul li.current-menu-parent a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .main-navigation ul li.current-menu-item a{
			color: <?php echo ($vg_siva_options['menu_bar_link_hover_color']); ?>;
			border-color: <?php echo ($vg_siva_options['menu_bar_link_hover_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget{
			color: <?php echo ($vg_siva_options['menu_bar_text_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget ul li a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget ol li a{
			color: <?php echo ($vg_siva_options['menu_bar_link_color']); ?>
		}

		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_product_categories ul li a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_categories ul li a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_nav_menu ul li a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_archive ul li a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_meta ul li a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_recent_comments ul li a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_recent_entries ul li a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_pages ul li a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_product_categories ul li,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_categories ul li,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_nav_menu ul li,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_archive ul li,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_meta ul li,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_recent_comments ul li,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_recent_entries ul li,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_pages ul li{
			color: <?php echo ($vg_siva_options['menu_bar_link_color']); ?>
		}
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_product_categories ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_categories ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_nav_menu ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_archive ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_meta ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_recent_comments ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_recent_entries ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_pages ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget ul li a:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget ol li a:hover,
		.vg-website-wrapper #vg-header-wrapper .top-bar.darken .widget_product_categories ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_categories ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_nav_menu ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_archive ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_meta ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_recent_comments ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_recent_entries ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_pages ul li.current-menu-item > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_product_categories ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_categories ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_nav_menu ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_archive ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_meta ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_recent_comments ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_recent_entries ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_pages ul li.current-cat > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_product_categories ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_categories ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_nav_menu ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_archive ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_meta ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_recent_comments ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_recent_entries ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_pages ul li.cat-parent > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_product_categories ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_categories ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_nav_menu ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_archive ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_meta ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_recent_comments ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_recent_entries ul li:hover > a,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .darken .widget_pages ul li:hover > a{
			color: <?php echo ($vg_siva_options['menu_bar_link_hover_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_product_categories ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_categories ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_nav_menu ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_archive ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_meta ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_recent_comments ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_recent_entries ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_pages ul li.current-menu-item,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_product_categories ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_categories ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_nav_menu ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_archive ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_meta ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_recent_comments ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .top-bar .widget_recent_entries ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_pages ul li.current-cat,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_product_categories ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_categories ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_nav_menu ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_archive ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_meta ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_recent_comments ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_recent_entries ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_pages ul li.cat-parent,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_product_categories ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_categories ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_nav_menu ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_archive ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_meta ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_recent_comments ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_recent_entries ul li:hover,
		.vg-website-wrapper #vg-header-wrapper .site-navigation .widget_pages ul li:hover{
			color: <?php echo ($vg_siva_options['menu_bar_link_hover_color']); ?>;
		}
		<?php endif; ?>

		<?php if(isset($vg_siva_options['sticky_menu_customize']) && $vg_siva_options['sticky_menu_customize']): ?>
		.vg-website-wrapper #vg-header-wrapper.fixed{
			color:<?php echo ($vg_siva_options['sticky_menu_text_color']); ?> ;
			background-color: <?php echo ($vg_siva_options['sticky_menu_background']['background-color']); ?> !important;
			background-image: url('<?php echo ($vg_siva_options['sticky_menu_background']['background-image']); ?>') !important;
			background-repeat: <?php echo ($vg_siva_options['sticky_menu_background']['background-repeat']); ?> !important;
			background-size: <?php echo ($vg_siva_options['sticky_menu_background']['background-size']); ?> !important;
			background-attachment: <?php echo ($vg_siva_options['sticky_menu_background']['background-attachment']); ?> !important;
			background-position: <?php echo ($vg_siva_options['sticky_menu_background']['background-position']); ?> !important;
		}
		.vg-website-wrapper #vg-header-wrapper.fixed .main-navigation ul li a{
			color: <?php echo ($vg_siva_options['sticky_menu_link_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper.fixed .main-navigation ul li.home a:before{
			color: <?php echo ($vg_siva_options['sticky_menu_text_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper.fixed .main-navigation ul li a:hover{
			color: <?php echo ($vg_siva_options['sticky_menu_link_hover_color']); ?>;
			border-color: <?php echo ($vg_siva_options['sticky_menu_link_hover_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper.fixed .main-navigation ul li.current-menu-parent a,
		.vg-website-wrapper #vg-header-wrapper.fixed .main-navigation ul li.current-menu-item a{
			color: <?php echo ($vg_siva_options['sticky_menu_link_hover_color']); ?>;
			border-color: <?php echo ($vg_siva_options['sticky_menu_link_hover_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper.fixed .search-icon{
			color:<?php echo ($vg_siva_options['sticky_menu_text_color']); ?>;
			border-color:<?php echo ($vg_siva_options['sticky_menu_text_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper.fixed .search-icon:hover{
			color:<?php echo ($vg_siva_options['sticky_menu_link_hover_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper.fixed .mini_cart_inner .top-cart-title a{
			color:<?php echo ($vg_siva_options['sticky_menu_text_color']); ?>;
			border-color:<?php echo ($vg_siva_options['sticky_menu_text_color']); ?>;
		}
		.vg-website-wrapper #vg-header-wrapper.fixed .mini_cart_inner .top-cart-title a:hover{
			color:<?php echo ($vg_siva_options['sticky_menu_link_hover_color']); ?>;
		}
		<?php endif; ?>

		<?php if(isset($vg_siva_options['footer_customize']) && $vg_siva_options['footer_customize']): ?>
		.vg-website-wrapper #vg-footer-wrapper{
			color:<?php echo ($vg_siva_options['footer_text_color']); ?>;
			background-color: <?php echo ($vg_siva_options['footer_background']['background-color']); ?>;
			background-image: url('<?php echo ($vg_siva_options['footer_background']['background-image']); ?>');
			background-repeat: <?php echo ($vg_siva_options['footer_background']['background-repeat']); ?>;
			background-size: <?php echo ($vg_siva_options['footer_background']['background-size']); ?>;
			background-attachment: <?php echo ($vg_siva_options['footer_background']['background-attachment']); ?>;
			background-position: <?php echo ($vg_siva_options['footer_background']['background-position']); ?>;
		}
		.vg-website-wrapper #vg-footer-wrapper .widget{
			color: <?php echo ($vg_siva_options['footer_text_color']); ?>;
		}
		.vg-website-wrapper #vg-footer-wrapper  .widget-title h3{
			color: <?php echo ($vg_siva_options['footer_text_color']); ?>;
		}
		.vg-website-wrapper #vg-footer-wrapper .widget ul li a,
		.vg-website-wrapper #vg-footer-wrapper .widget ol li a{
			color: <?php echo ($vg_siva_options['footer_link_color']); ?>
		}
		.vg-website-wrapper #vg-footer-wrapper .widget ul li a:hover,
		.vg-website-wrapper #vg-footer-wrapper .widget ol li a:hover{
			color: <?php echo ($vg_siva_options['footer_link_hover_color']); ?>
		}
		.vg-website-wrapper #vg-footer-wrapper .working-hours,
		.vg-website-wrapper #vg-footer-wrapper .working-hours b{
			color: <?php echo ($vg_siva_options['footer_text_color']); ?>;
		}
		.vg-website-wrapper #vg-footer-wrapper .copyright-text a{
			color: <?php echo ($vg_siva_options['footer_link_color']); ?>
		}
		.vg-website-wrapper #vg-footer-wrapper .ft-slogan{
			color:#000;
		}
		<?php endif; ?>

		<?php
		$content = ob_get_clean();
		$content = str_replace(array("\r\n", "\r"), "\n", $content);

		$lines 		= explode("\n", $content);
		$new_lines 	= array();

		foreach($lines as $i => $line) {
			if(!empty($line)) $new_lines[] = trim($line);
		}
		$custom_css = implode($new_lines);

		wp_enqueue_style('vg-siva-style', get_stylesheet_uri(), array(), '1.0', 'all');
		wp_add_inline_style('vg-siva-style', $custom_css);
	}
}

// Only load when Demo Mode = Disabled
$vg_siva_options = get_option("vg_siva_options");
if(isset($vg_siva_options['demo_mode']) && empty($vg_siva_options['demo_mode'])) {
add_action('wp_enqueue_scripts', 'vg_siva_custom_styles', 999);
}
// End


if(! function_exists('vg_siva_custom_code'))
{
	function vg_siva_custom_code()
	{
		$vg_siva_options 	= get_option("vg_siva_options");

		/********************************************************************/
		/* Custom CSS *******************************************************/
		/********************************************************************/

		if((isset($vg_siva_options['custom_css'])) &&(trim($vg_siva_options['custom_css']) != "")) {
			$custom_css = $vg_siva_options['custom_css'];
			wp_enqueue_style('vg-siva-style', get_stylesheet_uri(), array(), '1.0', 'all');
			wp_add_inline_style('vg-siva-style', $custom_css);
		}

		/********************************************************************/
		/* Custom JavaScript Code *******************************************/
		/********************************************************************/
		if((isset($vg_siva_options['custom_js'])) &&(trim($vg_siva_options['custom_js']) != "")) {
			$custom_js = $vg_siva_options['custom_js'];
			wp_add_inline_script('vg-siva-js', $custom_js, 'before');
		}
	}
}
add_action('wp_enqueue_scripts', 'vg_siva_custom_code', 1000);
