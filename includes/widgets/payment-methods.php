<?php
/*
 * This is Brand Logo Carousel widget
 */

// don't load directly
if(!defined('ABSPATH')) die('-1');

if(! function_exists('vg_siva_payment_methods_widget'))
{
	function vg_siva_payment_methods_widget() {
		register_widget('Vina_PaymentMethods_Widget');
	}
}
add_action('widgets_init', 'vg_siva_payment_methods_widget');

if(! class_exists('Vina_PaymentMethods_Widget'))
{
	class Vina_PaymentMethods_Widget extends WP_Widget
	{

		public function __construct()
		{
			parent::__construct(
				'vgw_payment_methods', // Base ID
				esc_html__('VGW Payment Methods', 'vg-siva'), // Name
				array('description' => esc_html__('A widget that displays Payment Methods Profiles', 'vg-siva'),) // Args
			);
		}

		public function widget($args, $instance)
		{
			$title = apply_filters('widget_title', $instance['title']);

			echo ($args['before_widget']);

			if(! empty($title))
				echo ($args['before_title']) . esc_html($title) . $args['after_title'];

			$vg_siva_options = get_option("vg_siva_options");

			$payment_gateway = $pm_baloto = $pm_card_exito = $pm_pse = $pm_card_alkosto = $pm_paypal = $pm_carulla = $pm_bancolombia = $pm_bancooccidente = $pm_bancoath = "";

			$payment_gateway_link = $vg_siva_options['payment_gateway_link'];

			if (isset($vg_siva_options['payment_gateway']['url'])) $payment_gateway	=	esc_url($vg_siva_options['payment_gateway']['url']);
			if (isset($vg_siva_options['pm_baloto']['url'])) $pm_baloto	=	esc_url($vg_siva_options['pm_baloto']['url']);
			if (isset($vg_siva_options['pm_card_exito']['url'])) $pm_card_exito	=	esc_url($vg_siva_options['pm_card_exito']['url']);
			if (isset($vg_siva_options['pm_pse']['url'])) $pm_pse	=	esc_url($vg_siva_options['pm_pse']['url']);
			if (isset($vg_siva_options['pm_card_alkosto']['url'])) $pm_card_alkosto	=	esc_url($vg_siva_options['pm_card_alkosto']['url']);
			if (isset($vg_siva_options['pm_paypal']['url'])) $pm_paypal	=	esc_url($vg_siva_options['pm_paypal']['url']);
			if (isset($vg_siva_options['pm_carulla']['url'])) $pm_carulla	=	esc_url($vg_siva_options['pm_carulla']['url']);
			if (isset($vg_siva_options['pm_bancolombia']['url'])) $pm_bancolombia	=	esc_url($vg_siva_options['pm_bancolombia']['url']);
			if (isset($vg_siva_options['pm_bancooccidente']['url'])) $pm_bancooccidente	=	esc_url($vg_siva_options['pm_bancooccidente']['url']);
			if (isset($vg_siva_options['pm_bancoath']['url'])) $pm_bancoath	=	esc_url($vg_siva_options['pm_bancoath']['url']);

			if (!empty($payment_gateway))	echo('<div class="box_payment_gateway"><a href="'. esc_url($payment_gateway_link) .'" ><img src="' . esc_url($payment_gateway) . '"class="payment_method_logo" id="payment_gateway"  alt="'. $vg_siva_options['payment_gateway']['title'] .'"> </a></div>');

			echo ('<div class="box_payment_methods"><div class="item_payment_methods">');
			if (!empty($pm_baloto))	echo('<img src="' . esc_url($pm_baloto) . '"class="payment_method_logo" alt="'. $vg_siva_options['pm_baloto']['title'] .'">');
			if (!empty($pm_card_exito))	echo('<img src="' . esc_url($pm_card_exito) . '"class="payment_method_logo" alt="'. $vg_siva_options['pm_card_exito']['title'] .'">');
			if (!empty($pm_pse))	echo('<img src="' . esc_url($pm_pse) . '"class="payment_method_logo" alt="'. $vg_siva_options['pm_pse']['title'] .'">');
			if (!empty($pm_card_alkosto))	echo('<img src="' . esc_url($pm_card_alkosto) . '"class="payment_method_logo" alt="'. $vg_siva_options['pm_card_alkosto']['title'] .'">');
			if (!empty($pm_paypal))	echo('<img src="' . esc_url($pm_paypal) . '"class="payment_method_logo" alt="'. $vg_siva_options['pm_paypal']['title'] .'">');
			if (!empty($pm_carulla))	echo('<img src="' . esc_url($pm_carulla) . '"class="payment_method_logo" alt="'. $vg_siva_options['pm_carulla']['title'] .'">');
			if (!empty($pm_bancolombia))	echo('<img src="' . esc_url($pm_bancolombia) . '"class="payment_method_logo" alt="'. $vg_siva_options['pm_bancolombia']['title'] .'">');
			if (!empty($pm_bancooccidente))	echo('<img src="' . esc_url($pm_bancooccidente) . '"class="payment_method_logo" alt="'. $vg_siva_options['pm_bancooccidente']['title'] .'">');
			if (!empty($pm_bancoath))	echo('<img src="' . esc_url($pm_bancoath) . '"class="payment_method_logo" alt="'. $vg_siva_options['pm_bancoath']['title'] .'">');
			echo ('</div></div>');
			echo ($args['after_widget']);
		}

		public function form($instance)
		{
			$title = !empty($instance['title']) ? $instance['title'] : esc_html__('Get Connected', 'vg-siva');
			?>

			<p><em><?php esc_html_e('You can manager Payment methods in admin >> Payment Methods.', 'vg-siva'); ?></em></p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'vg-siva'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
			</p>

			<?php
		}

		public function update($new_instance, $old_instance)
		{
			$instance = array();

			$instance['title'] = (! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';

			return $instance;
		}
	}
}
