<?php

    /**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if(! class_exists('Redux')) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "vg_siva_options";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => $opt_name,
        'display_name' => $theme->get('Name'),
        'display_version' => 'v.' . $theme->get('Version'),
        'page_slug' => 'vg-siva',
        'page_title' => $theme->get('Name'),
        'update_notice' => FALSE,
        'intro_text' => '',
        'footer_text' => esc_html__('Copyright &copy; 2016 ', 'vg-siva') . $theme->get('Name') . esc_html__('. All Rights Reserved.', 'vg-siva'),
        'admin_bar' => TRUE,
        'menu_type' => 'menu',
        'menu_title' => $theme->get('Name'),
        'allow_sub_menu' => TRUE,
        'page_parent_post_type' => 'your_post_type',
        'page_priority' => '3',
        'customizer' => FALSE,
        'default_mark' => '*',
		'global_variable' => 'vg_siva_options',
        'hints' => array(
            'icon' => 'el el-adjust-alt',
            'icon_position' => 'right',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
			),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
			),
            'tip_effect' => array(
                'show' => array(
                    'duration' => '500',
                    'event' => 'mouseover',
				),
                'hide' => array(
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
				),
			),
		),
        'output' => FALSE,
        'output_tag' => FALSE,
        'page_permissions' => 'manage_options',
        'save_defaults' => TRUE,
        'show_import_export' => TRUE,
        'database' => '',
        'transient_time' => '3600',
        'network_sites' => TRUE,
		'dev_mode' => false,
		'forced_dev_mode_off' => TRUE,
		'disable_tracking' => TRUE,
	);

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/vinawebsolutions/',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
	);
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/vnwebsolutions',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
	);

	if(! isset($args['global_variable']) || $args['global_variable'] !== false) {
        if(! empty($args['global_variable'])) {
            $v = $args['global_variable'];
        }
		else {
            $v = str_replace('-', '_', $args['opt_name']);
        }
    }

    Redux::setArgs($opt_name, $args);

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __('Theme Information 1', 'vg-siva'),
            'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'vg-siva')
		),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __('Theme Information 2', 'vg-siva'),
            'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'vg-siva')
		)
	);
    Redux::setHelpTab($opt_name, $tabs);

    // Set the help sidebar
    $content = __('<p>This is the sidebar content, HTML is allowed.</p>', 'vg-siva');
    Redux::setHelpSidebar($opt_name, $content);


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

	Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-tachometer',
        'title'  => __('General', 'vg-siva'),
        'fields' => array(

			array(
                'title' => __('Demo Mode', 'vg-siva'),
                'subtitle' => __('<em>Enabled / Disabled Demo Mode.<br>(for Developer only).</em>', 'vg-siva'),
				'desc' =>__('<em>When enabled, some config from Theme Options will not implement.</em>', 'vg-siva'),
                'id' => 'demo_mode',
                'on' => __('Enabled', 'vg-siva'),
                'off' => __('Disabled', 'vg-siva'),
                'type' => 'switch',
                'default' => 0,
			),

			array(
				'id'=>'demo_setting',
				'type' => 'textarea',
				'title' => __('Demo Setting', 'vg-siva'),
				'subtitle' => __('This field only use for Developer.', 'vg-siva'),
				'validate' => 'html_custom',
				'default' => 'niche-01:layout-1,preset-1,full-width',
				'allowed_html' => array('br' => array()),
				'required' => array('demo_mode', '=', array('1')),
			),

            array(
                'id'       => 'default_layout',
                'type'     => 'image_select',
                'compiler' => true,
                'title'    => __('Default Layout', 'vg-siva'),
                'subtitle' => __('<em>Select the default layout for your website.</em>', 'vg-siva'),
                'options'  => array(
                    'layout-1' => array(
                        'alt' => __('Layout 1', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/header_1.png'
					),
                    'layout-2' => array(
                        'alt' => __('Layout 2', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/header_2.png'
					),
                    'layout-3' => array(
                        'alt' => __('Layout 3', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/header_3.png'
					),
					'layout-4' => array(
                        'alt' => __('Layout 4', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/header_4.png'
					),
					'layout-5' => array(
                        'alt' => __('Layout 4', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/header_5.png'
					),

				),
                'default'  => 'layout-1',
				'required' => array('demo_mode', '=', array('0')),
			),

			array(
                'id'       => 'default_preset',
                'type'     => 'image_select',
                'compiler' => true,
                'title'    => __('Default Preset Color', 'vg-siva'),
                'subtitle' => __('<em>Select the default preset color for your website.</em>', 'vg-siva'),
                'options'  => array(
                    'preset-1' => array(
                        'alt' => __('Preset Color 1', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/preset-1.png'
					),
                    'preset-2' => array(
                        'alt' => __('Preset Color 2', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/preset-2.png'
					),
                    'preset-3' => array(
                        'alt' => __('Preset Color 3', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/preset-3.png'
					),
					'preset-4' => array(
                        'alt' => __('Preset Color 4', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/preset-4.png'
					),
				),
                'default'  => 'preset-1',
				'required' => array('demo_mode', '=', array('0')),
			),

			array(
                'id'       => 'website_width',
                'type'     => 'button_set',
                'title'    => __('Website Width', 'vg-siva'),
                'subtitle' => __('<em>Set up the width of the Website.</em>', 'vg-siva'),
                'options'  => array(
                    'full-width' => 'Full',
                    'box-width'  => 'Box'
				),
                'default'  => 'full-width',
				'required' => array('demo_mode', '=', array('0')),
			),

            array(
                'id'            => 'website_background',
                'type'          => 'background',
                'title'         => "Website Background Color",
                'subtitle'      => "<em>The website background.</em>",
                'default'  => array(
                    'background-color' => '#333333',
				),
                'transparent'   => false,
				'required' 		=> array(
					array('website_width', 'equals', array('box')),
					array('demo_mode', '=', array('0'))
				),
			),
            array(
				'id'        => 'theme_loading',
				'type'      => 'switch',
				'title'     => esc_html__('Show Loading Page', 'vg-siva'),
				'default'   => false,
			),
      ),
  ));


    Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-arrow-circle-up',
        'title'  => __('Header', 'vg-siva'),
        'fields' => array(
			array(
				'id'    => 'header_bar_warning',
				'type'  => 'info',
				'title' => __('Demo Mode is Enabled!', 'vg-siva'),
				'style' => 'warning',
				'desc'  => __('Demo Mode is Enabled, please disable it to customize Header section.', 'vg-siva'),
				'required' => array('demo_mode','=','1'),
			),
			array(
                'title' => __('Your Logo', 'vg-siva'),
                'subtitle' => __('<em>Upload your logo image.</em>', 'vg-siva'),
                'id' => 'site_logo',
                'type' => 'media',
                'default' => array(
                    'url' => '',
				),
				'required' => array('demo_mode','=','0'),
			),
			array(
                'title' => __('Customize CSS', 'vg-siva'),
                'subtitle' => __('<em>Allow you change background, font-size, font-color of Header site. If Disabled, the theme will use default values from preset color.</em>', 'vg-siva'),
                'id' => 'header_bar_customize',
                'on' => __('Enabled', 'vg-siva'),
                'off' => __('Disabled', 'vg-siva'),
                'type' => 'switch',
                'default' => 0,
				'required' => array('demo_mode','=','0'),
			),
			array(
                'title' => __('Logo Container Min Width', 'vg-siva'),
                'subtitle' => __('<em>Drag the slider to set the logo container min width.</em>', 'vg-siva'),
                'id' => 'logo_width',
                'type' => 'slider',
                "default" => 106,
                "min" => 0,
                "step" => 1,
                "max" => 600,
                'display_value' => 'text',
				'required' => array('header_bar_customize','=','1')
			),

            array(
                'title' => __('Logo Height', 'vg-siva'),
                'subtitle' => __('<em>Drag the slider to set the logo height <br/>(ignored if there\'s no uploaded logo).</em>', 'vg-siva'),
                'id' => 'logo_height',
                'type' => 'slider',
                "default" => 30,
                "min" => 0,
                "step" => 1,
                "max" => 300,
                'display_value' => 'text',
				'required' => array('header_bar_customize','=','1')
			),

			array(
                'title' => __('Header Bar Text Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Header Bar Link Color.</em>', 'vg-siva'),
                'id' => 'header_bar_text',
                'type' => 'color',
                'default' => '#5db582',
                'transparent' => false,
                'required' => array(
					array('header_bar_customize','=','1'),
				)
			),
      array(
                'title' => __('Banner Text Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Banner Text Color.</em>', 'vg-siva'),
                'id' => 'banner_text',
                'type' => 'color',
                'default' => '#5db5822',
                'transparent' => false,
                'required' => array(
          array('header_bar_customize','=','1'),
        )
      ),
			array(
                'title' => __('Header Bar Link Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Header Bar Link Color.</em>', 'vg-siva'),
                'id' => 'header_bar_link_color',
                'type' => 'color',
                'default' => '#5db582',
                'transparent' => false,
                'required' => array(
					array('header_bar_customize','=','1'),
				)
			),
			array(
                'title' => __('Header Bar Link Hover Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Header Bar Link Hover Color.</em>', 'vg-siva'),
                'id' => 'header_bar_link_hover_color',
                'type' => 'color',
                'default' => '#a34253',
                'transparent' => false,
                'required' => array(
					array('header_bar_customize','=','1'),
				)
			),
            array(
                'id'            => 'header_bar_background',
                'type'          => 'background',
                'title'         => __('Header Bar Background', 'vg-siva'),
                'subtitle'      => __('<em>The Header Bar background with image, color, etc.</em>', 'vg-siva'),
                'default'  => array(
                    'background-color' => '#f5f5f5',
				),
                'transparent'   => false,
				'required' 		=> array('header_bar_customize','=','1'),
			),

		),
	));
	Redux::setSection($opt_name, array(
        'icon'       => 'fa fa-angle-right',
        'title'      => __('Top Bar', 'vg-siva'),
        'subsection' => true,
        'fields'     => array(

			array(
				'id'    => 'top_bar_warning',
				'type'  => 'info',
				'title' => __('Demo Mode is Enabled!', 'vg-siva'),
				'style' => 'warning',
				'desc'  => __('Demo Mode is Enabled, please disable it to customize Top Bar.', 'vg-siva'),
				'required' => array('demo_mode','=','1'),
			),

			array(
                'title' => __('Customize CSS', 'vg-siva'),
                'subtitle' => __('<em>You can change background, font-size, font-color of Top Bar if enabled this option.</em>', 'vg-siva'),
                'id' => 'top_bar_customize',
                'on' => __('Enabled', 'vg-siva'),
                'off' => __('Disabled', 'vg-siva'),
                'type' => 'switch',
                'default' => 0,
				'required' => array('demo_mode','=','0')
			),

			array(
                'id'            => 'top_bar_background',
                'type'          => 'background',
                'title'         => __('Top Bar Background', 'vg-siva'),
                'subtitle'      => __('<em>Top Bar Background with image, color, etc.</em>', 'vg-siva'),
                'default'  => array(
                    'background-color' => '#1f1f1f',
				),
                'transparent'   => false,
				'required' 		=> array('top_bar_customize','=','1'),
			),
			array(
                'title' => __('Top Bar Text Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Top Bar Text Color.</em>', 'vg-siva'),
                'id' => 'top_bar_text',
                'type' => 'color',
                'default' => '#ffffff',
                'transparent' => false,
                'required' => array('top_bar_customize','=','1')
			),

			array(
                'title' => __('Top Bar Link Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Top Bar Link Color.</em>', 'vg-siva'),
                'id' => 'top_bar_link_color',
                'type' => 'color',
                'default' => '#ff0000',
                'transparent' => false,
                'required' => array('top_bar_customize','=','1')
			),

			array(
                'title' => __('Top Bar Link Hover Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Top Bar Link Hover Color.</em>', 'vg-siva'),
                'id' => 'top_bar_link_hover_color',
                'type' => 'color',
                'default' => '#ffff00',
                'transparent' => false,
                'required' => array('top_bar_customize','=','1')
			),
		)
	));
	Redux::setSection($opt_name, array(
        'icon'       => 'fa fa-angle-right',
        'title'      => __('Menu Bar', 'vg-siva'),
        'subsection' => true,
        'fields'     => array(

			array(
				'id'    => 'menu_bar_warning',
				'type'  => 'info',
				'title' => __('Demo Mode is Enabled!', 'vg-siva'),
				'style' => 'warning',
				'desc'  => __('Demo Mode is Enabled, please disable it to customize Menu Bar.', 'vg-siva'),
				'required' => array('demo_mode','=','1'),
			),

			array(
                'title' => __('Customize CSS', 'vg-siva'),
                'subtitle' => __('<em>Allow you change background, font-size, font-color of Menu Bar. If Disabled, the theme will use default values from preset color.</em>', 'vg-siva'),
                'id' => 'menu_bar_customize',
                'on' => __('Enabled', 'vg-siva'),
                'off' => __('Disabled', 'vg-siva'),
                'type' => 'switch',
                'default' => 0,
				'required' => array('demo_mode','=','0'),
			),

            array(
                'id'            => 'menu_bar_background',
                'type'          => 'background',
                'title'         => __('Menu Bar Background', 'vg-siva'),
                'subtitle'      => __('<em>The Menu Bar background with image, color, etc.</em>', 'vg-siva'),
                'default'  => array(
                    'background-color' => '#1f1f1f',
				),
                'transparent'   => false,
				'required' 		=> array('menu_bar_customize','=','1'),
			),

			array(
                'title' => __('Menu Bar Text Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Menu Bar Text Color.</em>', 'vg-siva'),
                'id' => 'menu_bar_text_color',
                'type' => 'color',
                'default' => '#ffffff',
                'transparent' => false,
                'required' => array(
					array('menu_bar_customize','=','1'),
				)
			),
			array(
                'title' => __('Menu Bar Link Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Menu Bar Link Color.</em>', 'vg-siva'),
                'id' => 'menu_bar_link_color',
                'type' => 'color',
                'default' => '#ff0000',
                'transparent' => false,
                'required' => array(
					array('menu_bar_customize','=','1'),
				)
			),
			array(
                'title' => __('Menu Bar Link Hover Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Menu Bar Link Hover Color.</em>', 'vg-siva'),
                'id' => 'menu_bar_link_hover_color',
                'type' => 'color',
                'default' => '#ffff00',
                'transparent' => false,
                'required' => array(
					array('menu_bar_customize','=','1'),
				)
			),
		)

	));
    Redux::setSection($opt_name, array(
        'icon'    => 'fa fa-arrow-circle-down',
        'title'   => __('Footer', 'vg-siva'),
        'fields'  => array(

			array(
				'id'    => 'footer_warning',
				'type'  => 'info',
				'title' => __('Demo Mode is Enabled!', 'vg-siva'),
				'style' => 'warning',
				'desc'  => __('Demo Mode is Enabled, please disable it to customize Footer.', 'vg-siva'),
				'required' => array('demo_mode','=','1'),
			),
			array(
                'title' => __('Customize CSS', 'vg-siva'),
                'subtitle' => __('<em>Allow you change background, font-size, font-color of Footer. If Disabled, the theme will use default values from preset color.</em>', 'vg-siva'),
                'id' => 'footer_customize',
                'on' => __('Enabled', 'vg-siva'),
                'off' => __('Disabled', 'vg-siva'),
                'type' => 'switch',
                'default' => 0,
				'required' => array('demo_mode','=','0'),
			),

            array(
                'id'            => 'footer_background',
                'type'          => 'background',
                'title'         => "Footer Background",
                'subtitle'      => "<em>Footer background with image, color, etc.</em>",
                'default'  => array(
                    'background-color' => '#1f1f1f',
				),
                'transparent'   => false,
				'required' 		=> array('footer_customize','=','1'),
			),
			array(
                'title' => __('Footer Text Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Footer Text Color.</em>', 'vg-siva'),
                'id' => 'footer_text_color',
                'type' => 'color',
                'transparent' => false,
                'default' => '#fff',
				'required' => array('footer_customize','=','1'),
			),
            array(
                'title' => __('Footer Link Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Footer Link Color.</em>', 'vg-siva'),
                'id' => 'footer_link_color',
                'type' => 'color',
                'transparent' => false,
                'default' => '#fff',
				'required' => array('footer_customize','=','1'),
			),

            array(
                'title' => __('Footer Link Hover Color', 'vg-siva'),
                'subtitle' => __('<em>Specify the Footer Link Hover Color.</em>', 'vg-siva'),
                'id' => 'footer_link_hover_color',
                'type' => 'color',
                'transparent' => false,
                'default' => '#1197d6',
				'required' => array('footer_customize','=','1'),
			),
		)

	));

    Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-list-alt',
        'title'  => __('Blog', 'vg-siva'),
        'fields' => array(
			array(
                'id'       => 'default_blog_sidebar',
                'type'     => 'image_select',
                'compiler' => true,
                'title'    => __('Default Sidebar', 'vg-siva'),
                'subtitle' => __('<em>Select the default blog sidebar for your website.</em>', 'vg-siva'),
                'options'  => array(
                    'left' => array(
                        'alt' => __('Lef Sidebar', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/sidebar_1.png'
					),
                    'right' => array(
                        'alt' => __('Right Sidebar', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/sidebar_2.png'
					),
                    'none' => array(
                        'alt' => __('Without Sidebar', 'vg-siva'),
                        'img' => get_template_directory_uri() . '/assets/images/theme_options/sidebar_3.png'
					),
				),
                'default'  => 'none',
			),
		)

	));

    Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-shopping-cart',
        'title'  => __('Shop', 'vg-siva'),
        'fields' => array(

			array(
				'id'       => 'default_woo_hover_effect',
				'subtitle' => esc_html__('Select default product hover effect.', 'vg-siva'),
				'type'     => 'select',
				'multi'    => false,
				'title'    => esc_html__('Hover Effect', 'vg-siva'),
				'options'  => array(
					'default' => 'Default',
					'effect-1' => 'Effect 01',
					'effect-2' => 'Effect 02',
					'effect-3' => 'Effect 03',
				),
				'default'  => 'effect-1'
			),

            array(
                'title' => __('Quick View', 'vg-siva'),
                'subtitle' => __('<em>Enable / Disable the quick view modal.</em>', 'vg-siva'),
                'desc' => __('<em>When enabled, the feature Turns On the quick view functionality.</em>', 'vg-siva'),
                'id' => 'quick_view',
                'on' => __('Enabled', 'vg-siva'),
                'off' => __('Disabled', 'vg-siva'),
                'type' => 'switch',
				'default' => 1,
			),

            array(
                'title' => __('Second Image on Catalog Page(Hover)', 'vg-siva'),
                'subtitle' => __('<em>Change to display second image when hover on product image.</em>', 'vg-siva'),
                'id' => 'second_image_product_listing',
                'on' => __('Enabled', 'vg-siva'),
                'off' => __('Disabled', 'vg-siva'),
                'type' => 'switch',
                'default' => 1,
			),

            array(
                'title' => __('Number of Products per Column', 'vg-siva'),
                'subtitle' => __('<em>Drag the slider to set the number of products per column <br />to be listed on the shop page and catalog pages.</em>', 'vg-siva'),
                'id' => 'products_per_column',
                'min' => '2',
                'step' => '1',
                'max' => '4',
                'type' => 'slider',
                'default' => '4',
			),

            array(
                'title' => __('Number of Products per Page', 'vg-siva'),
                'subtitle' => __('<em>Drag the slider to set the number of products per page <br />to be listed on the shop page and catalog pages.</em>', 'vg-siva'),
                'id' => 'products_per_page',
                'min' => '1',
                'step' => '1',
                'max' => '48',
                'type' => 'slider',
                'edit' => '1',
                'default' => '12',
			),

            array(
                'title' => __('Featured Label', 'vg-siva'),
                'subtitle' => __('<em>Out of Stock label text.</em>', 'vg-siva'),
                'id' => 'hot_label',
                'type' => 'text',
                'default' => 'Hot',
			),

            array(
                'title' => __('Sale Label', 'vg-siva'),
                'subtitle' => __('<em>Sale label text.</em>', 'vg-siva'),
                'id' => 'sale_label',
                'type' => 'text',
                'default' => 'Sale',
			),

      array(
        'title' => __('From text', 'vg-siva'),
        'subtitle' => __('<em>Prefix for price of products of type variable.</em>', 'vg-siva'),
        'id' => 'from_price_text',
        'type' => 'text',
        'default' => 'Desde',
      ),

		)

	));

    Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-archive',
        'title'  => __('Product Page', 'vg-siva'),
        'fields' => array(

			array(
                'title' => __('Column of Thumbnail Images', 'vg-siva'),
                'subtitle' => __('<em>Drag the slider to set the number of Thumbnail Images will visible in carousel.</em>', 'vg-siva'),
                'id' => 'column_thumbnail_images',
                'type' => 'slider',
                "default" => 4,
                "min" => 2,
                "step" => 1,
                "max" => 4,
                'display_value' => 'text',
			),

            array(
                'title' => __('Related Products', 'vg-siva'),
                'subtitle' => __('<em>Enable / Disable Related Products.<em>', 'vg-siva'),
                'id' => 'related_products',
                'on' => __('Enabled', 'vg-siva'),
                'off' => __('Disabled', 'vg-siva'),
                'type' => 'switch',
                'default' => 1,
			),

			array(
                'title' => __('Total Related Products', 'vg-siva'),
                'subtitle' => __('<em>Drag the slider to set the Total Related Products will display in carousel.</em>', 'vg-siva'),
                'id' => 'total_related_products',
                'type' => 'slider',
                "default" => 6,
                "min" => 1,
                "step" => 1,
                "max" => 12,
                'display_value' => 'text',
				'required' => array('related_products','=','1')
			),

            array(
                'title' => __('Sharing Options', 'vg-siva'),
                'subtitle' => __('<em>Enable / Disable Sharing Options.<em>', 'vg-siva'),
                'id' => 'sharing_options',
                'on' => __('Enabled', 'vg-siva'),
                'off' => __('Disabled', 'vg-siva'),
                'type' => 'switch',
                'default' => 1,
			),

		)

	));

  Redux::setSection($opt_name, array(
      'icon'   => 'fa fa-archive',
      'title'  => __('Contact Page', 'vg-siva'),
      'fields' => array(
        array(
          'title' => __('Background Image URL', 'vg-siva'),
          'subtitle' => __('<em>URL of image for background in contact page</em>', 'vg-siva'),
          'id' => 'bg_img_contact_pg',
          'type' => 'text',
          'default' => '',
        ),
      )
  ));

	Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-photo',
        'title'  => __('Project Page', 'vg-siva'),
        'fields' => array(
            array(
				'id'        	=> 'portfolio_columns',
				'type'      	=> 'slider',
				'title'     	=> esc_html__('Portfolio Columns', 'vg-siva'),
				"default"   	=> 4,
				"min"       	=> 2,
				"step"      	=> 1,
				"max"       	=> 4,
				'display_value' => 'text'
			),
			array(
				'id'        	=> 'portfolio_per_page',
				'type'      	=> 'slider',
				'title'     	=> esc_html__('Projects per page', 'vg-siva'),
				'desc'      	=> esc_html__('Amount of projects per page on portfolio page', 'vg-siva'),
				"default"   	=> 12,
				"min"       	=> 4,
				"step"      	=> 1,
				"max"       	=> 48,
				'display_value' => 'text'
			),
		),
	));
	Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-image',
        'title'  => __('Brand Logos', 'vg-siva'),
        'fields' => array(

			array(
				'id'          => 'brand_logos',
				'type'        => 'slides',
				'title'       => __('Brand Logos Manager', 'vg-siva'),
				'placeholder' => array(
					'title'           => __('This is a title', 'vg-siva'),
					'description'     => __('Description Here', 'vg-siva'),
					'url'             => __('Give us a link!', 'vg-siva'),
				),
			),

		)
	));

    Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-paint-brush',
        'title'  => __('Styling', 'vg-siva'),
        'fields' => array(
            array(
				'id'       => 'effect_banner',
				'title'     => esc_html__('Images Banner', 'vg-siva'),
				'subtitle' => esc_html__('Hover Effect Images Banner', 'vg-siva'),
				'type'     => 'select',
				'options'  => array(
					'style-0' 	=> 'No Effect',
					'style-1' 	=> 'Effect Style 1',
					'style-2' 	=> 'Effect Style 2',
					'style-3' 	=> 'Effect Style 3',
					'style-4' 	=> 'Effect Style 4',
				),
				'default'  => 'style-1'
			),
			array(
				'id'    => 'styling_warning',
				'type'  => 'info',
				'title' => __('Demo Mode is Enabled!', 'vg-siva'),
				'style' => 'warning',
				'desc'  => __('Demo Mode is Enabled, please disable it to customize the Styling of the theme.', 'vg-siva'),
				'required' => array('demo_mode','=','1'),
			),

			array(
                'title' => __('Customize Styling', 'vg-siva'),
                'subtitle' => __('<em>Allow you customize the Styling of the theme. If Disabled, the theme will use default values from preset color.</em>', 'vg-siva'),
                'id' => 'styling_customize',
                'on' => __('Enabled', 'vg-siva'),
                'off' => __('Disabled', 'vg-siva'),
                'type' => 'switch',
                'default' => 0,
				'required' => array('demo_mode','=','0'),
			),

            array(
                'title' => __('Body Texts Color', 'vg-siva'),
                'subtitle' => __('<em>Body Texts Color of the site.</em>', 'vg-siva'),
                'id' => 'body_color',
                'type' => 'color',
                'transparent' => false,
                'default' => '#545454',
				'required' => array('styling_customize','=','1'),
			),

            array(
                'title' => __('Main Theme Color', 'vg-siva'),
                'subtitle' => __('<em>The main color of the site.</em>', 'vg-siva'),
                'id' => 'main_color',
                'type' => 'color',
                'transparent' => false,
                'default' => '#bb06d3',
				'required' => array('styling_customize','=','1'),
			),

            array(
                'id'            => 'main_background',
                'type'          => 'background',
                'title'         => "Body Background",
                'subtitle'      => "<em>Body background with image, color, etc.</em>",
                'default'  => array(
                    'background-color' => '#fff',
				),
                'transparent'   => false,
				'required' 		=> array('styling_customize','=','1'),
			),

		)

	));

    Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-font',
        'title'  => __('Typography', 'vg-siva'),
        'fields' => array(

            array(
				'id'    => 'typography_warning',
				'type'  => 'info',
				'title' => __('Demo Mode is Enabled!', 'vg-siva'),
				'style' => 'warning',
				'desc'  => __('Demo Mode is Enabled, please disable it to customize Typography of the theme.', 'vg-siva'),
				'required' => array('demo_mode','=','1'),
			),

			array(
                'title' => __('Customize Typography', 'vg-siva'),
                'subtitle' => __('<em>Allow you customize Typography of the theme. If Disabled, the theme will use default values from preset color.</em>', 'vg-siva'),
                'id' => 'typography_customize',
                'on' => __('Enabled', 'vg-siva'),
                'off' => __('Disabled', 'vg-siva'),
                'type' => 'switch',
                'default' => 0,
				'required' => array('demo_mode','=','0'),
			),

            array(
                'id' => 'source_fonts_info',
                'icon' => true,
                'type' => 'info',
                'raw' => __('<h3 style="margin: 0;"><i class="fa fa-font"></i> Font Sources</h3>', 'vg-siva'),
				'required' => array('typography_customize','=','1'),
			),

            array(
                'title'    => __('Font Source', 'vg-siva'),
                'subtitle' => __('<em>Choose the Font Source</em>', 'vg-siva'),
                'id'       => 'font_source',
                'type'     => 'radio',
                'options'  => array(
                    '1' => 'Standard + Google Webfonts',
                    '2' => 'Google Custom',
                    '3' => 'Adobe Typekit'
				),
                'default' => '2',
				'required' => array('typography_customize','=','1'),
			),

            // Google Code
            array(
                'id'=>'font_google_code',
                'type' => 'text',
                'title' => __('Google Code', 'vg-siva'),
                'subtitle' => __('<em>Paste the provided Google Code</em>', 'vg-siva'),
                'default' => 'https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i',
                'required' => array(
					array('font_source','=','2'),
					array('typography_customize','=','1')
				)
			),

            // Typekit ID
            array(
                'id'=>'font_typekit_kit_id',
                'type' => 'text',
                'title' => __('Typekit Kit ID', 'vg-siva'),
                'subtitle' => __('<em>Paste the provided Typekit Kit ID.</em>', 'vg-siva'),
                'default' => '',
                'required' => array(
					array('font_source','=','3'),
					array('typography_customize','=','1')
				)
			),

            array(
                'id' => 'main_font_info',
                'icon' => true,
                'type' => 'info',
                'raw' => __('<h3 style="margin: 0;"><i class="fa fa-font"></i> Main Font</h3>', 'vg-siva'),
				'required' => array('typography_customize','=','1'),
			),

            // Standard + Google Webfonts
            array(
                'title' => __('Font Face', 'vg-siva'),
                'subtitle' => __('<em>Pick the Main Font for your site.</em>', 'vg-siva'),
                'id' => 'main_font',
                'type' => 'typography',
                'line-height' => false,
                'text-align' => false,
                'font-style' => false,
                'font-weight' => false,
                'all_styles'=> true,
                'font-size' => false,
                'color' => false,
                'default' => array(
                    'font-family' => 'Montserrat',
                    'subsets' => '',
				),
                'required' => array(
					array('font_source','=','1'),
					array('typography_customize','=','1')
				)
			),

            // Google Custom
            array(
                'title' => __('Google Font Face', 'vg-siva'),
                'subtitle' => __('<em>Enter your Google Font Name for the theme\'s Main Typography</em>', 'vg-siva'),
                'desc' => __('e.g.: PT Sans', 'vg-siva'),
                'id' => 'main_google_font_face',
                'type' => 'text',
                'default' => 'PT Sans',
                'required' => array(
					array('font_source','=','2'),
					array('typography_customize','=','1')
				)
			),

            // Adobe Typekit
            array(
                'title' => __('Typekit Font Face', 'vg-siva'),
                'subtitle' => __('<em>Enter your Typekit Font Name for the theme\'s Main Typography</em>', 'vg-siva'),
                'desc' => __('e.g.: futura-pt', 'vg-siva'),
                'id' => 'main_typekit_font_face',
                'type' => 'text',
                'default' => '',
                'required' => array(
					array('font_source','=','3'),
					array('typography_customize','=','1')
				)
			),


            array(
                'id' => 'secondary_font_info',
                'icon' => true,
                'type' => 'info',
                'raw' => __('<h3 style="margin: 0;"><i class="fa fa-font"></i> Secondary Font</h3>', 'vg-siva'),
				'required' => array('typography_customize','=','1'),
			),

            // Standard + Google Webfonts
            array(
                'title' => __('Font Face', 'vg-siva'),
                'subtitle' => __('<em>Pick the Secondary Font for your site.</em>', 'vg-siva'),
                'id' => 'secondary_font',
                'type' => 'typography',
                'line-height' => false,
                'text-align' => false,
                'font-style' => false,
                'font-weight' => false,
                'all_styles'=> true,
                'font-size' => false,
                'color' => false,
                'default' => array(
                    'font-family' => 'Pontano Sans',
                    'subsets' => '',
				),
                'required' => array(
					array('font_source','=','1'),
					array('typography_customize','=','1')
				)

			),

            // Google Custom
            array(
                'title' => __('Google Font Face', 'vg-siva'),
                'subtitle' => __('<em>Enter your Google Font Name for the theme\'s Secondary Typography</em>', 'vg-siva'),
                'desc' => __('e.g.: PT Sans', 'vg-siva'),
                'id' => 'secondary_google_font_face',
                'type' => 'text',
                'default' => 'PT Sans',
                'required' => array(
					array('font_source','=','2'),
					array('typography_customize','=','1')
				)
			),

            // Adobe Typekit
            array(
                'title' => __('Typekit Font Face', 'vg-siva'),
                'subtitle' => __('<em>Enter your Typekit Font Name for the theme\'s Secondary Typography</em>', 'vg-siva'),
                'desc' => __('e.g.: futura-pt', 'vg-siva'),
                'id' => 'secondary_typekit_font_face',
                'type' => 'text',
                'default' => '',
                'required' => array(
					array('font_source','=','3'),
					array('typography_customize','=','1')
				)
			),

		)

	));

    Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-share-alt-square',
        'title'  => __('Social Media', 'vg-siva'),
        'fields' => array(

            array(
                'title' => __('<i class="fa fa-facebook"></i> Facebook', 'vg-siva'),
                'subtitle' => __('<em>Type your Facebook profile URL here.</em>', 'vg-siva'),
                'id' => 'facebook_link',
                'type' => 'text',
                'default' => 'https://www.facebook.com/VinaWebSolutions',
			),

            array(
                'title' => __('<i class="fa fa-twitter"></i> Twitter', 'vg-siva'),
                'subtitle' => __('<em>Type your Twitter profile URL here.</em>', 'vg-siva'),
                'id' => 'twitter_link',
                'type' => 'text',
                'default' => 'http://twitter.com/vnwebsolutions',
			),

            array(
                'title' => __('<i class="fa fa-pinterest"></i> Pinterest', 'vg-siva'),
                'subtitle' => __('<em>Type your Pinterest profile URL here.</em>', 'vg-siva'),
                'id' => 'pinterest_link',
                'type' => 'text',
                'default' => 'http://www.pinterest.com/',
			),

            array(
                'title' => __('<i class="fa fa-linkedin"></i> LinkedIn', 'vg-siva'),
                'subtitle' => __('<em>Type your LinkedIn profile URL here.</em>', 'vg-siva'),
                'id' => 'linkedin_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-google-plus"></i> Google+', 'vg-siva'),
                'subtitle' => __('<em>Type your Google+ profile URL here.</em>', 'vg-siva'),
                'id' => 'googleplus_link',
                'type' => 'text',
				'default' => '#',
			),

            array(
                'title' => __('<i class="fa fa-rss"></i> RSS', 'vg-siva'),
                'subtitle' => __('<em>Type your RSS Feed URL here.</em>', 'vg-siva'),
                'id' => 'rss_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-tumblr"></i> Tumblr', 'vg-siva'),
                'subtitle' => __('<em>Type your Tumblr URL here.</em>', 'vg-siva'),
                'id' => 'tumblr_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-instagram"></i> Instagram', 'vg-siva'),
                'subtitle' => __('<em>Type your Instagram profile URL here.</em>', 'vg-siva'),
                'id' => 'instagram_link',
                'type' => 'text',
                'default' => '',
			),

            array(
                'title' => __('<i class="fa fa-youtube-play"></i> Youtube', 'vg-siva'),
                'subtitle' => __('<em>Type your Youtube profile URL here.</em>', 'vg-siva'),
                'id' => 'youtube_link',
                'type' => 'text',
                'default' => '',
			),

            array(
                'title' => __('<i class="fa fa-vimeo-square"></i> Vimeo', 'vg-siva'),
                'subtitle' => __('<em>Type your Vimeo profile URL here.</em>', 'vg-siva'),
                'id' => 'vimeo_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-behance"></i> Behance', 'vg-siva'),
                'subtitle' => __('<em>Type your Behance profile URL here.</em>', 'vg-siva'),
                'id' => 'behance_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-dribbble"></i> Dribble', 'vg-siva'),
                'subtitle' => __('<em>Type your Dribble profile URL here.</em>', 'vg-siva'),
                'id' => 'dribble_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-flickr"></i> Flickr', 'vg-siva'),
                'subtitle' => __('<em>Type your Flickr profile URL here.</em>', 'vg-siva'),
                'id' => 'flickr_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-git"></i> Git', 'vg-siva'),
                'subtitle' => __('<em>Type your Git profile URL here.</em>', 'vg-siva'),
                'id' => 'git_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-skype"></i> Skype', 'vg-siva'),
                'subtitle' => __('<em>Type your Skype profile URL here.</em>', 'vg-siva'),
                'id' => 'skype_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-weibo"></i> Weibo', 'vg-siva'),
                'subtitle' => __('<em>Type your Weibo profile URL here.</em>', 'vg-siva'),
                'id' => 'weibo_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-foursquare"></i> Foursquare', 'vg-siva'),
                'subtitle' => __('<em>Type your Foursquare profile URL here.</em>', 'vg-siva'),
                'id' => 'foursquare_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-soundcloud"></i> Soundcloud', 'vg-siva'),
                'subtitle' => __('<em>Type your Soundcloud profile URL here.</em>', 'vg-siva'),
                'id' => 'soundcloud_link',
                'type' => 'text',
			),

            array(
                'title' => __('<i class="fa fa-vk"></i> VK', 'vg-siva'),
                'subtitle' => __('<em>Type your VK profile URL here.</em>', 'vg-siva'),
                'id' => 'vk_link',
                'type' => 'text',
			),

		)

	));

  Redux::setSection($opt_name, array(
      'icon'   => 'fa fa-share-alt-square',
      'title'  => __('Payment Methods', 'vg-siva'),
      'fields' => array(
        array(
          'title' => __('payment gateway link', 'vg-siva'),
          'subtitle' => __('link to payment gateway platform', 'vg-siva'),
          'id' => 'payment_gateway_link',
          'type' => 'text',
        ),
        array(
          'title' => __('payment gateway', 'vg-siva'),
          'subtitle' => __('<em>Define you payment gateway.</em>', 'vg-siva'),
          'id' => 'payment_gateway',
          'type' => 'media',
          'default' => array(
            'url' => '',
          ),
          'required' => false,
        ),
        array(
          'title' => __('baloto', 'vg-siva'),
          'subtitle' => __('<em>Define image for baloto.</em>', 'vg-siva'),
          'id' => 'pm_baloto',
          'type' => 'media',
          'default' => array(
            'url' => '',
          ),
          'required' => false,
        ),
        array(
          'title' => __('card exito', 'vg-siva'),
          'subtitle' => __('<em>Define image for card exito.</em>', 'vg-siva'),
          'id' => 'pm_card_exito',
          'type' => 'media',
          'default' => array(
            'url' => '',
          ),
          'required' => false,
        ),
        array(
          'title' => __('pse', 'vg-siva'),
          'subtitle' => __('<em>Define image for pse.</em>', 'vg-siva'),
          'id' => 'pm_pse',
          'type' => 'media',
          'default' => array(
            'url' => '',
          ),
          'required' => false,
        ),
        array(
          'title' => __('card alkosto', 'vg-siva'),
          'subtitle' => __('<em>Define image for card alkosto.</em>', 'vg-siva'),
          'id' => 'pm_card_alkosto',
          'type' => 'media',
          'default' => array(
            'url' => '',
          ),
          'required' => false,
        ),
        array(
          'title' => __('paypal', 'vg-siva'),
          'subtitle' => __('<em>Define image for paypal.</em>', 'vg-siva'),
          'id' => 'pm_paypal',
          'type' => 'media',
          'default' => array(
            'url' => '',
          ),
          'required' => false,
        ),
        array(
          'title' => __('carulla', 'vg-siva'),
          'subtitle' => __('<em>Define image for carulla.</em>', 'vg-siva'),
          'id' => 'pm_carulla',
          'type' => 'media',
          'default' => array(
            'url' => '',
          ),
          'required' => false,
        ),
        array(
          'title' => __('bancolombia', 'vg-siva'),
          'subtitle' => __('<em>Define image for bancolombia.</em>', 'vg-siva'),
          'id' => 'pm_bancolombia',
          'type' => 'media',
          'default' => array(
            'url' => '',
          ),
          'required' => false,
        ),
        array(
          'title' => __('bancooccidente', 'vg-siva'),
          'subtitle' => __('<em>Define image for banco occidente.</em>', 'vg-siva'),
          'id' => 'pm_bancooccidente',
          'type' => 'media',
          'default' => array(
            'url' => '',
          ),
          'required' => false,
        ),
        array(
          'title' => __('bancoath', 'vg-siva'),
          'subtitle' => __('<em>Define image for banco ATH.</em>', 'vg-siva'),
          'id' => 'pm_bancoath',
          'type' => 'media',
          'default' => array(
            'url' => '',
          ),
          'required' => false,
        ),
      )
  ));

	Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-exchange',
        'title'  => __('Owl Carousel', 'vg-siva'),
        'fields' => array(
            array(
				'id' 		=> 'slide_speed',
                'title' 	=> __('Slide Speed(ms)', 'vg-siva'),
                'desc' 		=> __('<em>Slide speed in milliseconds. Only use numeric. Default: 200.</em>', 'vg-siva'),
                'type' 		=> 'text',
                'default' 	=> '200',
			),
			array(
				'id' 		=> 'pagination_speed',
                'title' 	=> __('Pagination Speed(ms)', 'vg-siva'),
                'desc' 		=> __('<em>Pagination speed in milliseconds. Only use numeric. Default: 800.</em>', 'vg-siva'),
                'type' 		=> 'text',
                'default' 	=> '800',
			),
			array(
                'title' 	=> __('Rewind Speed(ms)', 'vg-siva'),
                'desc' 		=> __('<em>Rewind speed in milliseconds. Only use numeric. Default: 1000.</em>', 'vg-siva'),
                'id' 		=> 'rewind_speed',
                'type' 		=> 'text',
                'default' 	=> '1000',
			),
			array(
                'title' 	=> __('Autoplay', 'vg-siva'),
				'desc' 		=>__('<em>Enable/disable autoplay for all carousel.</em>', 'vg-siva'),
                'id' 		=> 'auto_play',
                'on' 		=> __('Enabled', 'vg-siva'),
                'off' 		=> __('Disabled', 'vg-siva'),
                'type' 		=> 'switch',
                'default' 	=> 0,
			),
			array(
                'title' 	=> __('Autoplay Speed(ms)', 'vg-siva'),
                'desc' 		=> __('<em>Autoplay speed in milliseconds. Only use numeric. Default: 5000.</em>', 'vg-siva'),
                'id' 		=> 'autoplay_speed',
                'type' 		=> 'text',
                'default' 	=> '5000',
				'required'	=> array('auto_play', '=', array('1')),
			),
			array(
                'title' 	=> __('Stop on Hover', 'vg-siva'),
				'desc' 		=>__('<em>Enable/disable stop carousel when mouse hover function.</em>', 'vg-siva'),
                'id' 		=> 'stop_hover',
                'on' 		=> __('Enabled', 'vg-siva'),
                'off' 		=> __('Disabled', 'vg-siva'),
                'type' 		=> 'switch',
                'default' 	=> 0,
			),
			array(
                'title' 	=> __('Rewind Nav', 'vg-siva'),
				'desc' 		=>__('<em>Slide to first item. Use rewindSpeed to change animation speed.</em>', 'vg-siva'),
                'id' 		=> 'rewind_nav',
                'on' 		=> __('Enabled', 'vg-siva'),
                'off' 		=> __('Disabled', 'vg-siva'),
                'type' 		=> 'switch',
                'default' 	=> 1,
			),
			array(
                'title' 	=> __('Scroll per Page', 'vg-siva'),
				'desc' 		=>__('<em>Scroll per page not per item. This affect next/prev buttons and mouse/touch dragging.</em>', 'vg-siva'),
                'id' 		=> 'scroll_page',
                'on' 		=> __('Enabled', 'vg-siva'),
                'off' 		=> __('Disabled', 'vg-siva'),
                'type' 		=> 'switch',
                'default' 	=> 0,
			),
			array(
                'title' 	=> __('Mouse Drag', 'vg-siva'),
				'desc' 		=>__('<em>Enable/disable mouse drag function.</em>', 'vg-siva'),
                'id' 		=> 'mouse_drag',
                'on' 		=> __('Enabled', 'vg-siva'),
                'off' 		=> __('Disabled', 'vg-siva'),
                'type' 		=> 'switch',
                'default' 	=> 0,
			),
			array(
                'title' 	=> __('Touch Drag', 'vg-siva'),
				'desc' 		=>__('<em>Enable/disable touch drag function.</em>', 'vg-siva'),
                'id' 		=> 'touch_drag',
                'on' 		=> __('Enabled', 'vg-siva'),
                'off' 		=> __('Disabled', 'vg-siva'),
                'type' 		=> 'switch',
                'default' 	=> 1,
			),
		)

	));


  Redux::setSection($opt_name, array(
      'icon'   => 'fa fa-arrow-circle-up',
      'title'  => __('Banners', 'vg-siva'),
      'fields' => array(
        array(
          'title' => __('Show Top Banner', 'vg-siva'),
          'subtitle' => __('<em>If disable, the top banner is hidden.</em>', 'vg-siva'),
          'id' => 'active_header_banner_top',
          'on' => __('Enabled', 'vg-siva'),
          'off' => __('Disabled', 'vg-siva'),
          'type' => 'switch',
          'default' => 1,
        ),
        array(
          'title' => __('Top Banner', 'vg-siva'),
          'subtitle' => __('<em>Upload your image for top banner.</em>', 'vg-siva'),
          'id' => 'header_banner_top',
          'type' => 'media',
          'default' => array(
              'url' => '',
          ),
          'required' => array('active_header_banner_top','=','1'),
        ),
        array(
          'title' => __('Cart Banner', 'vg-siva'),
          'subtitle' => __('<em>Upload your promotional banner for cart.</em>', 'vg-siva'),
          'id' => 'promotional_banner_cart',
          'type' => 'media',
          'default' => array(
              'url' => '',
          ),
        ),
      ),
  ));


    Redux::setSection($opt_name, array(
        'icon'   => 'fa fa-code',
        'title'  => __('Custom Code', 'vg-siva'),
        'fields' => array(

            array(
                'title' => __('Custom CSS', 'vg-siva'),
                'subtitle' => __('<em>Paste your custom CSS code here.</em>', 'vg-siva'),
                'id' => 'custom_css',
                'type' => 'ace_editor',
                'mode' => 'css',
            ),

            array(
                'title' => __('Custom JavaScript Code', 'vg-siva'),
                'subtitle' => __('<em>Paste your custom JS code here. The code will be added to your site.</em>', 'vg-siva'),
                'id' => 'custom_js',
                'type' => 'ace_editor',
                'mode' => 'javascript',
            ),

        )

    ));

    /*
     * <--- END SECTIONS
     */
