=== VG Siva ===

Contributors: automattic
Tags: responsive, clothing, electronic, fashion, fashion shop, woocommerce, bootstrap 3, redux framework, multipurpose, creative, ecommerce, minimalist

Requires at least: 4.0
Tested up to: 4.2.2
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A starter theme called VG Siva, or underscores.

== Description ==

VG Siva is a creative, minimalist responsive WooCommerce WordPress theme specifically designed to adapt its display according to the device in use. It's packed with great features you can customize according to your brand.

It is an up-to-date theme with the latest WordPress eCommerce trends and can handle all the technical stuff for you. You will find 5 elegant homepage layouts along with lots of premium elements in VG Siva: Revolution Slideshow, Visual Composer, WooCommerce Product Carousel, Brand Logo Carousel, Blog Carousel, unique ShortCodes, widgets... The theme comes with an extensive admin panel, packed with various options so you can easily modify styles, colors, fonts and other aspects of the theme. 

Stand out from your competitors with this sleek, trendy theme! 

== Main Features ==
	- Fully Responsive: This theme is responsive to give a perfect user experience on all devices.
	- 5 elegant homepage layouts with lots of premium elements. It's powerful design tool!
	- Boxed or Fullwidth layout: This can be set globally or even per page!
	- Built on Twitter Bootstrap: VG iFoody uses Twitter Bootstrap. This means that a range of shortcodes are automatically supported. For ease of use you can use the Visual Composer, Easy Bootstrap Shortcode or any other plugin to easily add visuals to your website.
	- Slider Revolution plugin included: This theme includes the Slider Revolution plugin, saving you $19
	- Visual Composer plugin included: This theme includes the Visual Composer plugin, saving you $30
	- Contact Form 7 plugin support: This theme includes styling for the Contact Form 7 plugin!
	- Translation Ready: With added support for the WPML plugin
	- WooCommerce 2.6+ Ready: Start selling online
	- Demo content included! (quickstart package)
	- Unlimited Color Options
	- Moveable & Unlimited Sidebars: Move the sidebar to the left, the right, or hide it entirely for a fullwidth page or post!
	- Designed with HTML5 and CSS3
	- Customizable Design & Code
	- Product Quick View, Product Compare and Ajax Wishlist ready!
	- Online Documentation – http://wordpress.vinagecko.com/docs/?theme=siva
	- Supports Chrome, Safari, Firefox, IE
	- Child Theme included

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Changelog ==

= 1.0 - Mar 12 2017 =
* Initial release
