<?php
// DO NOT MODIFY
// 1. Replace 'vg-siva' by 'vg-' + theme name.
// 2. Replace 'vg_siva' by 'vg_' + theme name.
// 3. Replace 'VG Siva' by 'VG ' + theme Name.
// 4. Replace 'Siva' by theme name
// 5. Replace 'siva' by theme Name

// Includes Plugin
include_once(ABSPATH . 'wp-admin/includes/plugin.php');
if(! function_exists('vg_siva_setup'))
{
	function vg_siva_setup()
	{
		// Add Redux Framework
		require get_template_directory() . '/admin/admin-init.php';

		// Make theme available for translation.
		load_theme_textdomain('vg-siva', get_template_directory() . '/languages');

		// This theme styles the visual editor with editor-style.css to match the theme style.
		add_editor_style();

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		// Let WordPress manage the document title.
		add_theme_support('title-tag');

		// Enable support for Post Thumbnails on posts and pages.
		add_theme_support('post-thumbnails');

		// Enable support for WooCommerce
		add_theme_support('woocommerce');
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(array(
			'primary' 				=> esc_html__('Primary Menu', 'vg-siva'),
			'category-product' 		=>  esc_html__('Category Product Menu', 'vg-siva'),
			'mobilemenucategory' 	=>  esc_html__('Mobile Menu Category Product', 'vg-siva'),
		));

		// Switch default core markup for search form, comment form, and comments to output valid HTML5.
		add_theme_support('html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		));

		// Enable support for Post Formats.
		add_theme_support('post-formats', array(
			'aside',
			'image',
			'video',
			'audio',
			'quote',
			'link',
		));

		// Set up the WordPress core custom background feature.
		add_theme_support('custom-background', apply_filters('vg_siva_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		)));

		add_theme_support( "custom-header", array(
			'default-color' => '',
		));

		// Post Thumbnails Size
		set_post_thumbnail_size(1170, 9999); // Unlimited height, soft crop
		add_image_size('vg_siva_post_full', 1170, 780, true); //(cropped)
		add_image_size('vg_siva_post_carousel', 570, 475, true); //(cropped)
		add_image_size('vg_siva_post_carousel_2', 285, 220, true); //(cropped)
		add_image_size('vg_siva_post_carousel_3', 1620, 690, true); //(cropped)
		add_image_size('vg_siva_post_carousel_4', 460, 460, true); //(cropped)
		add_image_size('vg_siva_testimonial', 165, 400, true); //(cropped)
	}
}
add_action('after_setup_theme', 'vg_siva_setup');

/******************************************************************************/
/************************ One Click Sample data *******************************/
/******************************************************************************/

if(is_plugin_active('one-click-demo-import/one-click-demo-import.php')){
	function vg_siva_plugin_intro_text($default_text)
	{
		$default_text .= '<div class="ocdi__intro-text"><h2>VG siva - Import Demo Data</h2></div>';

		return $default_text;
	}
	add_filter('pt-ocdi/plugin_intro_text', 'vg_siva_plugin_intro_text');

	add_filter('pt-ocdi/disable_pt_branding', '__return_true');

	function vg_siva_import_files()
	{
		return array(
			array(
				'import_file_name'             => __('VG Siva - Import Demo Data', 'vg-siva'),
				'categories'                   => array('Category 1', 'Category 2'),
				'local_import_file'            => trailingslashit(get_template_directory()) . 'sample-data/siva_contents.xml',
				'local_import_widget_file'     => trailingslashit(get_template_directory()) . 'sample-data/siva_widgets.wie',
				'local_import_customizer_file' => trailingslashit(get_template_directory()) . 'sample-data/siva_customizer.dat',
				'local_import_redux'           => array(
					array(
						'file_path'   => trailingslashit(get_template_directory()) . 'sample-data/siva_options.json',
						'option_name' => 'vg_siva_options',
					),
				),

				'import_preview_image_url'     =>  home_url('/') . 'wp-content/themes/vg-siva/screenshot.png',
				'import_notice'                => __('After you import this demo data, you will have to setup the slider separately. Please read the document of the theme to know how to import slider.', 'vg-siva'),
				'preview_url'                  => 'http://wordpress.vinagecko.net/t/siva/',
			),
		);
	}
	add_filter('pt-ocdi/import_files', 'vg_siva_import_files');

	function vg_siva_after_import_setup()
	{
		// Assign menus to their locations.
		$main_menu = get_term_by('name', 'Main Menu', 'nav_menu');

		set_theme_mod( 'nav_menu_locations', array(
				'primary' => $main_menu->term_id,
			)
		);

		// Assign front page and posts page (blog page).
		$front_page_id = get_page_by_title('Home Default');

		update_option('show_on_front', 'page');
		update_option('page_on_front', $front_page_id->ID);
	}
	add_action('pt-ocdi/after_import', 'vg_siva_after_import_setup');
}

/******************************************************************************/
/******************** Set the content width in pixels *************************/
/******************************************************************************/

if(! function_exists('vg_siva_content_width'))
{
	function vg_siva_content_width() {
		$GLOBALS['content_width'] = apply_filters('vg_siva_content_width', 640);
	}
}
add_action('after_setup_theme', 'vg_siva_content_width', 0);

/******************************************************************************/
/******************** Register Google fonts *************************/
/******************************************************************************/

if ( ! function_exists( 'vg_siva_fonts_url' ) ) :
function vg_siva_fonts_url() {
    $fonts_url = '';
    $fonts     = array();
    $subsets   = '';

    /* translators: If there are characters in your language that are not supported by this font, translate this to 'off'. Do not translate into your own language. */
    if ( 'off' !== esc_html_x( 'on', 'Poppins font: on or off', 'vg-siva' ) ) {
        $fonts[] = 'Poppins:300,400,500,600,700';
    }
	if ( 'off' !== esc_html_x( 'on', 'Playfair Display font: on or off', 'vg-siva' ) ) {
        $fonts[] = 'Playfair Display:400,400i,700,700i,900,900i';
    }
	if ( 'off' !== esc_html_x( 'on', 'Droid Serif font: on or off', 'vg-siva' ) ) {
        $fonts[] = 'Droid Serif:400,400i,700,700i';
    }
	if ( 'off' !== esc_html_x( 'on', 'Montserrat font: on or off', 'vg-siva' ) ) {
        $fonts[] = 'Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i';
    }
    if ( $fonts ) {

		$protocol = is_ssl() ? 'https' : 'http';
		$query_args = array(
			'family' => urlencode( implode( '|', $fonts )),
			'subset' => urlencode( $subsets ),
		);
		$fonts_url = add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" );
    }

    return $fonts_url;
}
endif;


/**
 * Enqueue scripts and styles.
 */
function vg_siva_fonts_scripts() {

    // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style( 'vg-siva-fonts', vg_siva_fonts_url(), array(), null );
}
add_action( 'wp_enqueue_scripts', 'vg_siva_fonts_scripts' );

/******************************************************************************/
/************************* Register widget area *******************************/
/******************************************************************************/

if(! function_exists('vg_siva_widgets_init'))
{
	function vg_siva_widgets_init()
	{
		register_sidebar(array(
			'name'          => esc_html__('Sidebar', 'vg-siva'),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Widget on Blog', 'vg-siva'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));
		register_sidebar(array(
			'name' 			=> esc_html__('Top Bar 01', 'vg-siva'),
			'id' 			=> 'top-bar-01',
			'class' 		=> 'top-bar-01',
			'description' 	=> esc_html__('Widget on Column 01', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Top Bar 02', 'vg-siva'),
			'id' 			=> 'top-bar-02',
			'class' 		=> 'top-bar-02',
			'description' 	=> esc_html__('Widget on Column 02', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Top Search', 'vg-siva'),
			'id' 			=> 'top-search',
			'class' 		=> 'top-search',
			'description' 	=> esc_html__('Widget for Search', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Top Cart', 'vg-siva'),
			'id' 			=> 'top-cart',
			'class' 		=> 'top-cart',
			'description' 	=> esc_html__('Widget for Mini Cart of WooCommerce', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Top Static', 'vg-siva'),
			'id' 			=> 'top-static',
			'class' 		=> 'top-static',
			'description' 	=> esc_html__('Widget for slogan offers!', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Top Settings', 'vg-siva'),
			'id' 			=> 'top-settings',
			'class' 		=> 'top-settings',
			'description' 	=> esc_html__('Widget for all setting : currency, languages', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Top Menu', 'vg-siva'),
			'id' 			=> 'top-menu',
			'class' 		=> 'top-menu',
			'description' 	=> esc_html__('Widget only work with Layout 3', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Top Account', 'vg-siva'),
			'id' 			=> 'top-account',
			'class' 		=> 'top-account',
			'description' 	=> esc_html__('Widget only work with Layout 3', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Social Icons', 'vg-siva'),
			'id' 			=> 'social-icons',
			'class' 		=> 'social-icons',
			'description' 	=> esc_html__('Widget for Social', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Top Banner', 'vg-siva'),
			'id' 			=> 'top-banner',
			'class' 		=> 'top-banner',
			'description' 	=> esc_html__('Widget only work width Layout 2 on Top Header', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));


		register_sidebar(array(
			'name' 			=> esc_html__('Newsletters', 'vg-siva'),
			'id' 			=> 'ft-newsletters',
			'class' 		=> 'ft-newsletters',
			'description' 	=> esc_html__('Widget sign up for latest Newsletters', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Footer 01', 'vg-siva'),
			'id' 			=> 'footer-01',
			'class' 		=> 'footer-01',
			'description' 	=> esc_html__('Widget on Footer area 1', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="col-xs-6 widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title footer-widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Footer 02', 'vg-siva'),
			'id' 			=> 'footer-02',
			'class' 		=> 'footer-02',
			'description' 	=> esc_html__('Widget on Footer area 2', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="col-xs-6 widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title footer-widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));

		register_sidebar(array(
			'name' 			=> esc_html__('Footer Payments', 'vg-siva'),
			'id' 			=> 'footer-payments',
			'class' 		=> 'footer-payments',
			'description' 	=> esc_html__('Widget on Footer Payments', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="col-xs-12  widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title footer-widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));
		register_sidebar(array(
			'name' 			=> esc_html__('Filter in Shop', 'vg-siva'),
			'id' 			=> 'filter-shop',
			'class' 		=> 'filter-shop',
			'description' 	=> esc_html__('Widget on toolbar category product', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));
		register_sidebar(array(
			'name'          => esc_html__('Featrued Post', 'vg-siva'),
			'id'            => 'feature-post',
			'description'   => esc_html__('Widget on Category Post', 'vg-siva'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));
		register_sidebar(array(
			'name'          => esc_html__('Sticky Post', 'vg-siva'),
			'id'            => 'sticky-post',
			'description'   => esc_html__('Widget on Category Post', 'vg-siva'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));
		register_sidebar(array(
			'name'          => esc_html__('Widget Testimonial', 'vg-siva'),
			'id'            => 'st-testimonial',
			'description'   => esc_html__('Widget on Category Post', 'vg-siva'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title' 	=> '<div class="widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));
		register_sidebar(array(
			'name' 			=> esc_html__('Bottom Footer', 'vg-siva'),
			'id' 			=> 'footer-copyright',
			'class' 		=> 'footer-copyright',
			'description' 	=> esc_html__('Widget on Bottom Footer Column 01', 'vg-siva'),
			'before_widget' => '<div id="%1$s" class="widget widget-footer widget-bottomft %2$s">',
			'after_widget' 	=> '</div>',
			'before_title' 	=> '<div class="widget-title footer-widget-title"><h3>',
			'after_title' 	=> '</h3></div>',
		));
	}
}
add_action('widgets_init', 'vg_siva_widgets_init');


/******************************************************************************/
/****** Add Font Awesome to Redux *********************************************/
/******************************************************************************/
if(! function_exists('vg_siva_new_icon_font'))
{
	function vg_siva_new_icon_font()
	{
		wp_register_style(
			'redux-font-awesome',
			get_template_directory_uri() . '/assets/common/css/font-awesome.min.css',
			array(),
			time(),
			'all'
		);
		wp_enqueue_style('redux-font-awesome');
	}
}
add_action('redux/page/vg_siva_options/enqueue', 'vg_siva_new_icon_font');


/******************************************************************************/
/**************************** Enqueue styles **********************************/
/******************************************************************************/

// Fontend
if(! function_exists('vg_siva_styles'))
{
	function vg_siva_styles()
	{
		$vg_siva_options = get_option("vg_siva_options");

		// Load common css files
		wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/common/css/font-awesome.min.css', array(), '4.6.3', 'all');
		wp_enqueue_style('simple-line-icons', get_template_directory_uri() . '/assets/common/css/simple-line-icons.css', array(), '4.6.3', 'all');
		wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/common/css/bootstrap.min.css', array(), '3.3.7', 'all');
		wp_enqueue_style('bootstrap-theme', get_template_directory_uri() . '/assets/common/css/bootstrap-theme.min.css', array(), '3.3.7', 'all');

		wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/assets/common/css/owl.carousel.css', array(), '1.3.2', 'all');
		wp_enqueue_style('owl.theme', get_template_directory_uri() . '/assets/common/css/owl.theme.css', array(), '1.3.2', 'all');
		wp_enqueue_style('treeview', get_template_directory_uri() . '/assets/common/css/jquery.treeview.css', array(), '1.3.2', 'all');
		wp_enqueue_style('material', get_template_directory_uri() . '/assets/common/css/material-design-iconic-font.min.css', array(), '2.2.0', 'all');
		wp_enqueue_style('elegant', get_template_directory_uri() . '/assets/common/css/elegant-style.css', array(), '1.0.0', 'all');
		wp_enqueue_style('themify-icons', get_template_directory_uri() . '/assets/common/css/themify-icons.css', array(), '1.0.0', 'all');
		wp_enqueue_style('sumoselect', get_template_directory_uri() . '/assets/common/css/sumoselect.css', array(), '1.0.0', 'all');
		wp_enqueue_style('thumbnail-slider', get_template_directory_uri() . '/assets/common/css/thumbnail-slider.css', array(), '1.0.0', 'all');


		if(isset($vg_siva_options['font_source']) &&($vg_siva_options['font_source'] == "2")) {
			if((isset($vg_siva_options['font_google_code'])) &&($vg_siva_options['font_google_code'] != "")) {
				wp_enqueue_style('vg-siva-font_google_code', $vg_siva_options['font_google_code'], array(), '1.0', 'all');
			}
		}

		wp_enqueue_style('nanoscroller', get_template_directory_uri() . '/assets/common/css/nanoscroller.css', array(), '3.3.7', 'all');
		wp_enqueue_style('animate', get_template_directory_uri() . '/assets/css/animate.css', array(), '1.0', 'all');
		wp_enqueue_style('vg-siva-menus', get_template_directory_uri() . '/assets/css/menus.css', array(), '1.0', 'all');
		wp_enqueue_style('vg-siva-offcanvas', get_template_directory_uri() . '/assets/css/offcanvas.css', array(), '1.0', 'all');
		wp_enqueue_style('vg-siva-css', get_template_directory_uri() . '/assets/css/theme.css', array(), '1.0', 'all');
		wp_enqueue_style('vg-siva-style', get_stylesheet_uri(), array(), '1.0', 'all');
	}
}
add_action('wp_enqueue_scripts', 'vg_siva_styles', 99);

// Backend
if(! function_exists('vg_siva_admin_styles'))
{
	function vg_siva_admin_styles()
	{
		if(is_admin()) {
			wp_enqueue_style('vg-siva-admin', get_template_directory_uri() . '/assets/css/admin.css', array(), '1.0', 'all');
			wp_enqueue_style("wp-color-picker");
		}
	}
}
add_action('admin_enqueue_scripts', 'vg_siva_admin_styles');


/******************************************************************************/
/*************************** Enqueue scripts **********************************/
/******************************************************************************/

// frontend
if(! function_exists('vg_siva_scripts'))
{
	function vg_siva_scripts()
	{
		$vg_siva_options = get_option("vg_siva_options");

		/** In Header **/
		if(isset($vg_siva_options['font_source']) &&($vg_siva_options['font_source'] == "3")) {
			if((isset($vg_siva_options['font_typekit_kit_id'])) &&($vg_siva_options['font_typekit_kit_id'] != "")) {
				wp_enqueue_script('vg-siva-font_typekit', '//use.typekit.net/'.$vg_siva_options['font_typekit_kit_id'].'.js', array(), NULL, FALSE);
				wp_enqueue_script('vg-siva-font_typekit_exec', get_template_directory_uri() . '/assets/common/js/typekit.js', array(), NULL, FALSE);
			}
		}

		/** In Footer **/
		wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/common/js/bootstrap.min.js', array(), '3.3.7', FALSE);
		wp_enqueue_script('jquery-touchswipe', get_template_directory_uri() . '/assets/common/js/jquery.touchSwipe.min.js', array('jquery'), '1.6.5', TRUE);
		wp_enqueue_script('nanoscroller', get_template_directory_uri() . '/assets/common/js/jquery.nanoscroller.min.js', array(), '0.7.6', TRUE);
		wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/assets/common/js/owl.carousel.js', array(), '0.7.6', TRUE);
		wp_enqueue_script('treeview', get_template_directory_uri() . '/assets/common/js/jquery.treeview.js', array(), '0.7.6', TRUE);
		wp_enqueue_script('jquery-countdown', get_template_directory_uri() . '/assets/common/js/jquery.countdown.min.js', array(), '2.0.4', TRUE);
		wp_enqueue_script('vg-siva-plugins', get_template_directory_uri() . '/assets/common/js/plugins.js', array(), '0.3.6', TRUE);

		wp_enqueue_script('jquery-lazy', get_template_directory_uri() . '/assets/common/js/jquery.lazy.min.js', array(), '1.7.4', TRUE);
		wp_enqueue_script('jquery-lazy-plugins', get_template_directory_uri() . '/assets/common/js/jquery.lazy.plugins.min.js', array(), '1.7.4', TRUE);

		wp_enqueue_script('jquery-sumoselect', get_template_directory_uri() . '/assets/common/js/jquery.sumoselect.js', array(), '3.0.2', TRUE);
		wp_enqueue_script('thumbnail-slider', get_template_directory_uri() . '/assets/common/js/thumbnail-slider.js', array(), '3.0.2', TRUE);

		//Add Shuffle js
		wp_enqueue_script( 'modernizr-custom', get_template_directory_uri() . '/assets/common/js/modernizr.custom.min.js', array('jquery'), '2.6.2', true );
		wp_enqueue_script( 'jquery-shuffle', get_template_directory_uri() . '/assets/common/js/jquery.shuffle.min.js', array('jquery'), '3.0.0', true );


		wp_enqueue_script('vg-siva-js', get_template_directory_uri() . '/assets/js/theme.js', array('jquery'), '1.0', TRUE);

		if(is_singular() && comments_open() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}

		/** In Header **/
		wp_add_inline_script('vg-siva-js', 'var vg_siva_ajaxurl = "'. esc_url(admin_url('admin-ajax.php', 'relative')) .'";');
	}
}
add_action('wp_enqueue_scripts', 'vg_siva_scripts', 99);


/******************************************************************************/
/************************** Get Default Layout ********************************/
/******************************************************************************/

if(! function_exists('vg_siva_get_default_layout'))
{
	function vg_siva_get_default_layout()
	{
		$vg_siva_options = get_option("vg_siva_options");
		$default_layout	   = $vg_siva_options['default_layout'];

		// Check Demo Mode
		$demo_mode = $vg_siva_options['demo_mode'];

		if($demo_mode)
		{
			$demos = vg_siva_get_demo_settings();
			$demo  = reset($demos);

			if(count($demo)) {
				$default_layout = $demo['layout'];
			}

			// Get layout from URL
			$niche = get_query_var('demo', '');
			if(!empty($niche) && isset($demos[$niche]) && count($demos[$niche])) {
				$default_layout = $demos[$niche]['layout'];
			}
		}

		return $default_layout;
	}
}


/******************************************************************************/
/*************************** Get Demo Settings ********************************/
/******************************************************************************/

if(! function_exists('vg_siva_get_demo_settings'))
{
	function vg_siva_get_demo_settings()
	{
		$vg_siva_options = get_option("vg_siva_options");
		$demo_setting 	   = $vg_siva_options['demo_setting'];

		$demos = array();
		if(!empty($demo_setting) && 1 !== $demo_setting)
		{
			$demo_settings = explode("\n", $demo_setting);
			if(count($demo_settings))
			{
				for($i = 0; $i < count($demo_settings); $i++)
				{
					$niche  = preg_replace('/\s+/', '', $demo_settings[$i]);
					$niches = explode(":", $niche);
					$values = explode(",", $niches[1]);

					$demos[$niches[0]] = array(
						'layout' => $values[0],
						'preset' => $values[1],
						'width'  => $values[2]
					);
				}
			}
		}

		return $demos;
	}
}

/******************************************************************************/
/************************* Add Query Vars Filter ******************************/
/******************************************************************************/

if(! function_exists('vg_siva_add_query_vars_filter'))
{
	function vg_siva_add_query_vars_filter($vars)
	{
	  $vars[] = "demo";
	  return $vars;
	}
}
add_filter('query_vars', 'vg_siva_add_query_vars_filter');


/******************************************************************************/
/***************************** Set Body Class *********************************/
/******************************************************************************/

if(! function_exists('vg_siva_set_body_class'))
{
	function vg_siva_set_body_class($classes)
	{
		$vg_siva_options = get_option("vg_siva_options");
		$default_layout	  = isset($vg_siva_options['default_layout']) ? $vg_siva_options['default_layout'] : 'layout-1';
		$default_preset	  = isset($vg_siva_options['default_preset']) ? $vg_siva_options['default_preset'] : 'preset-1';
		$default_width	  = isset($vg_siva_options['website_width']) ? $vg_siva_options['website_width'] : 'full-width';

		// Check Demo Mode
		$demo_mode = $vg_siva_options['demo_mode'];
		if($demo_mode)
		{
			$demos = vg_siva_get_demo_settings();
			$demo  = reset($demos);

			if(count($demo)) {
				$default_layout = $demo['layout'];
				$default_preset = $demo['preset'];
				$default_width  = $demo['width'];
			}

			// Get layout from URL
			$niche = get_query_var('demo', '');
			if(!empty($niche) && isset($demos[$niche]) && count($demos[$niche])) {
				$default_layout = $demos[$niche]['layout'];
				$default_preset = $demos[$niche]['preset'];
				$default_width  = $demos[$niche]['width'];
			}
		}

		// Set Class for Body Tag
		$classes[] = $default_layout;
		$classes[] = $default_preset;
		$classes[] = $default_width;

		return $classes;
	}
}
add_filter('body_class', 'vg_siva_set_body_class');


/******************************************************************************/
/***************************** Display Top Logo *******************************/
/******************************************************************************/

if(! function_exists('vg_siva_display_top_logo'))
{
	function vg_siva_display_top_logo()
	{
		$vg_siva_options = get_option("vg_siva_options");
		$demo_mode         = $vg_siva_options['demo_mode'];

		$logo_html_code	= '<a href="'. esc_url(home_url('/')) .'" rel="home">';

		if($demo_mode)  // If `demo mode` is enabled.
		{
			$logo_html_code	.= '<span class="logo-background">'. get_bloginfo('name') .'</span>';
		}
		else // If `demo mode` is disabled.
		{
			if((isset($vg_siva_options['site_logo']['url'])) &&(trim($vg_siva_options['site_logo']['url']) != ""))
			{
				// Website Logo
				$site_logo 		 = $vg_siva_options['site_logo']['url'];
				$logo_html_code	.= '<img class="site-logo" src="'. esc_url($site_logo) .'" alt="'. esc_attr(get_bloginfo('name')) .'" />';

				// Mobile Logo
				if((isset($vg_siva_options['site_logo_mobile']['url'])) &&(trim($vg_siva_options['site_logo_mobile']['url']) != "")) {
					$sticky_logo 	 = $vg_siva_options['site_logo_mobile']['url'];
					$logo_html_code	.= '<img class="site-logo-mobile" src="'. esc_url($site_logo) .'" alt="'. esc_attr(get_bloginfo('name')) .'" />';
				}
			}
			else
			{
				$logo_html_code	.=  '<span class="logo-text">'. get_bloginfo('name') . '</span>';
			}
		}

		$logo_html_code .= '</a>';

		echo ($logo_html_code);
	}
}

/******************************************************************************/
/***************************** Display Bottom Logo *******************************/
/******************************************************************************/

if(! function_exists('vg_siva_display_bottom_logo'))
{
	function vg_siva_display_bottom_logo()
	{
		$vg_siva_options = get_option("vg_siva_options");
		$demo_mode         = $vg_siva_options['demo_mode'];

		$logo_html_code	= '<a href="'. esc_url(home_url('')) .'" rel="home">';

		if($demo_mode)  // If `demo mode` is enabled.
		{
			$logo_html_code	.= '<span class="logo-background">'. get_bloginfo('name') .'</span>';
		}
		else // If `demo mode` is disabled.
		{
			if((isset($vg_siva_options['site_logo_bottom']['url'])) && (trim($vg_siva_options['site_logo_bottom']['url']) != ""))
			{
				// Website Logo
				$site_logo 		 = $vg_siva_options['site_logo_bottom']['url'];
				$logo_html_code	.= '<img class="site-logo" src="'. esc_url($site_logo) .'" alt="'. esc_attr(get_bloginfo('name')) .'" />';
			}
			else
			{
				$logo_html_code	.=  '<span class="logo-text">'. get_bloginfo('name') . '</span>';
			}
		}

		$logo_html_code .= '</a>';

		echo ($logo_html_code);
	}
}

// get all files from a folder
if(! function_exists('vg_siva_get_all_files'))
{
	function vg_siva_get_all_files($dir)
	{
		global $wp_filesystem;

		if(empty($wp_filesystem)) {
			require_once (ABSPATH . '/wp-admin/includes/file.php');
			WP_Filesystem();
		}

		$result = array();
		$cdir  	= $wp_filesystem->dirlist($dir);

		foreach($cdir as $key => $value)
		{
			if(!in_array($key, array(".", "..")))
			{
				if(is_file($dir . DIRECTORY_SEPARATOR . $key))
				{
					$result[] = $key;
				}
			}
		}

		return $result;
	}
}


/******************************************************************************/
/************************ Return Global Variables *****************************/
/******************************************************************************/
if(! function_exists('vg_siva_global_variable'))
{
	function vg_siva_global_variable($variable) {
		global $$variable;
		return $$variable;
	}
}

/******************************************************************************/
/***************************** Include Library ********************************/
/******************************************************************************/

// Custom Tags
require get_template_directory() . '/includes/template-tags.php';

// Breadcrumb
require get_template_directory() . '/includes/breadcrumb.php';

// Custom Mega Menu
include_once(get_template_directory() . '/includes/custom-menu/custom-menu.php');

// Product Search Widget
include_once(get_template_directory() . '/includes/widgets/product-search.php');

if (class_exists('WooCommerce')){
	// Product Carousel Widget
	include_once(get_template_directory() . '/includes/widgets/product-carousel.php');
}

// Post Carousel Widget
include_once(get_template_directory() . '/includes/widgets/post-carousel.php');

// Brand Logos Carousel Widget
include_once(get_template_directory() . '/includes/widgets/brand-carousel.php');

// Social Media Widget
include_once(get_template_directory() . '/includes/widgets/social-media.php');

// Payment Methods Widget
include_once(get_template_directory() . '/includes/widgets/payment-methods.php');

// Category Treeview Widget
include_once(get_template_directory() . '/includes/widgets/category-treeview.php');

// Quick View
include_once(get_template_directory() . '/includes/woocommerce/quick-view.php');

// Woo Ajax
include_once(get_template_directory() . '/includes/woocommerce/wooajax.php');

// Testimonials
include_once(get_template_directory() . '/includes/testimonial/admin.php');
include_once(get_template_directory() . '/includes/testimonial/shortcode.php');

// Load Custom style from Theme Options
include_once(get_template_directory() . '/includes/custom-styles.php');

// Register Shortcode
if(function_exists('vg_helper_register_shortcode')) {
	vg_helper_register_shortcode();
}

/******************************************************************************/
/******************************* Woocommerce **********************************/
/******************************************************************************/
if (class_exists('WooCommerce')){
	// Product Carousel Widget
	include_once(get_template_directory() . '/woocommerce/woocommerce-registration-fields.php');
}

// Load Custom style from Theme Options
include_once(get_template_directory() . '/woocommerce/woocommerce-functions.php');


//Calculator Visual Composer
function vg_siva_cal_vc_width(){
	if(is_rtl() ||(isset($_GET['d']) && $_GET['d'] == 'rtl')){
		$inlineJS = "
			jQuery(function($) {
				var window_width = $(window).width();
				var container_width = $('.container').outerWidth();
				var right = (container_width - window_width)/2;
				$('.vc_row[data-vc-full-width=\"true\"]').css({'right' : right});
			});
		";
		wp_add_inline_script('vg-siva-js', $inlineJS);
	}
}
add_action('wp_head', 'vg_siva_cal_vc_width');

/******************************************************************************/
/******************************* Default Style **********************************/
/******************************************************************************/

function vg_siva_hover_effect_banner() {
	$vg_siva_options = get_option("vg_siva_options");

	$effectBanner = (isset($vg_siva_options['effect_banner']) && $vg_siva_options['effect_banner']!='') ? $vg_siva_options['effect_banner'] : '';

	$customJS = "
		jQuery(document).ready(function($){
			$('.banner-box').addClass('". esc_js($effectBanner) ."');
			$('.widget_sp_image').addClass('". esc_js($effectBanner) ."');
			$('.wpb_single_image').addClass('". esc_js($effectBanner) ."');
		});
	";
	wp_add_inline_script('vg-siva-js', $customJS);
}
add_action('wp_head', 'vg_siva_hover_effect_banner');


function vg_siva_default_woo_hover_effect(){

	$vg_siva_options = get_option("vg_siva_options");

	$wooHoverEffect = (isset($vg_siva_options['default_woo_hover_effect']) && $vg_siva_options['default_woo_hover_effect']!='') ? $vg_siva_options['default_woo_hover_effect'] : '';

	return $wooHoverEffect;
}

function vg_siva_default_post_style(){
	$vg_siva_options = get_option("vg_siva_options");

	$postStyle = (isset($vg_siva_options['default_post_style']) && $vg_siva_options['default_post_style']!='') ? $vg_siva_options['default_post_style'] : '';

	return $postStyle;
}

/* Sticky Menu */
function vg_siva_sticky_header () {
	$vg_siva_options = get_option("vg_siva_options");

	$stickyJS = (isset($vg_siva_options['sticky_menu']) && $vg_siva_options['sticky_menu']) ? "sticky_menu = true;" : "sticky_menu = false;";

	wp_add_inline_script('vg-siva-js', $stickyJS);
}
add_action('wp_head', 'vg_siva_sticky_header');

if(! function_exists('vg_siva_display_logo_sticky'))
{
	function vg_siva_display_logo_sticky()
	{
		$vg_siva_options = get_option("vg_siva_options");

		$logo_html_code	= '<div class="sticky_logo"><a href="'. esc_url(home_url('/')) .'" rel="home">';

		if((isset($vg_siva_options['sticky_header_logo']['url'])) &&(trim($vg_siva_options['sticky_header_logo']['url']) != ""))
		{
			// Website Logo
			$site_logo 		 = $vg_siva_options['sticky_header_logo']['url'];
			$logo_html_code	.= '<img class="site-logo" src="'. esc_url($site_logo) .'" alt="'. esc_attr(get_bloginfo('name')) .'" />';
		}
		else
		{
			$logo_html_code	.=  '<span class="logo-background">'. get_bloginfo('name') . '</span>';
		}

		$logo_html_code .= '</a></div>';

		echo ($logo_html_code);
	}
}

/* Remove Redux Demo Link */
function vg_siva_removeDemoModeLink()
{
    if(class_exists('ReduxFrameworkPlugin')) {
        remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2);
    }
    if(class_exists('ReduxFrameworkPlugin')) {
        remove_action('admin_notices', array(ReduxFrameworkPlugin::get_instance(), 'admin_notices'));
    }
}
add_action('init', 'vg_siva_removeDemoModeLink');
/******************************************************************************/
/******************************* Add Lazy Load ********************************/
/******************************************************************************/
if(!is_admin()){
	function vg_siva_alter_att_attributes($attr) {
		$attr['data-src'] = $attr['src'];
		$attr['class'] 	  = $attr['class'] . ' lazy';
		$attr['src'] 	  = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
		return $attr;
	}
	add_filter('wp_get_attachment_image_attributes', 'vg_siva_alter_att_attributes');
}

/******************************************************************************/
/******************************* Add Project ********************************/
/******************************************************************************/
remove_action( 'projects_before_single_project_summary', 'projects_template_single_title', 10 );
add_action( 'projects_single_project_summary', 'projects_template_single_title', 5 );

remove_action( 'projects_before_single_project_summary', 'projects_template_single_short_description', 20 );
add_action( 'projects_single_project_summary', 'projects_template_single_short_description', 25 );

remove_action( 'projects_before_single_project_summary', 'projects_template_single_feature', 30 );

remove_action( 'projects_single_project_summary', 'projects_template_single_description', 10 );
add_action( 'projects_single_project_summary_desc', 'projects_template_single_description', 30 );
//projects list
remove_action( 'projects_loop_item', 'projects_template_loop_project_title', 20 );

/* HJ20170525 Product Variables Options */

function vg_siva_variation_attribute_options($args = array())
{
	$args = wp_parse_args(apply_filters('woocommerce_dropdown_variation_attribute_options_args', $args), array(
		'options'          => false,
		'attribute'        => false,
		'product'          => false,
		'selected' 	       => false,
		'name'             => '',
		'id'               => '',
		'class'            => '',
		'show_option_none' => __('Choose an option', 'vg-siva'),
	));

	$options               = $args['options'];
	$product               = $args['product'];
	$attribute             = $args['attribute'];
	$name                  = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title($attribute);
	$id                    = $args['id'] ? $args['id'] : sanitize_title($attribute);
	$class                 = $args['class'];
	$show_option_none      = $args['show_option_none'] ? true : false;
	$show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __('Choose an option', 'vg-siva'); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

	if (empty($options) && ! empty($product) && ! empty($attribute)) {
		$attributes = $product->get_variation_attributes();
		$options    = $attributes[ $attribute ];
	}

	$html = '<select id="' . esc_attr($id) . '" class="' . esc_attr($class) . '" name="' . esc_attr($name) . '" data-attribute_name="attribute_' . esc_attr(sanitize_title($attribute)) . '" data-show_option_none="' . ($show_option_none ? 'yes' : 'no') . '">';
	$html .= '<option value="">' . esc_html($show_option_none_text) . '</option>';

	if (! empty($options)) {
		if ($product && taxonomy_exists($attribute)) {
			// Get terms if this is a taxonomy - ordered. We need the names too.
			$terms = wc_get_product_terms($product->get_id(), $attribute, array('fields' => 'all'));

			foreach ($terms as $term) {
				if (in_array($term->slug, $options)) {
					$html .= '<option value="' . esc_attr($term->slug) . '" ' . selected(sanitize_title($args['selected']), $term->slug, false) . '>' . esc_html(apply_filters('woocommerce_variation_option_name', $term->name)) . '</option>';
				}
			}
		} else {
			foreach ($options as $option) {
				// This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
				$selected = sanitize_title($args['selected']) === $args['selected'] ? selected($args['selected'], sanitize_title($option), false) : selected($args['selected'], $option, false);
				$html .= '<option value="' . esc_attr($option) . '" ' . $selected . '>' . esc_html(apply_filters('woocommerce_variation_option_name', $option)) . '</option>';
			}
		}
	}

	$html .= '</select>';

	return apply_filters('woocommerce_dropdown_variation_attribute_options_html', $html, $args);
}

function brand_shop_banner( $atts ) {
	$atts = shortcode_atts(
		array(
			'color_left' => '',
			'color_right' => '',
			'color_bottom' => '',
			'brand_uniq' => '',
		),
		$atts
	);

	$page_object = get_queried_object();
	$page_id     = get_queried_object_id();

	$logo_url = "";

	if (has_post_thumbnail($page_id)) {
	  $image = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full');
	  $logo_url = $image[0];
	}

	$banner_style = "";
	if ($atts['color_right'] != '' && $atts['color_left'] != '' && $atts['color_bottom'] != '') {
		$banner_style = "style='background-image: -webkit-linear-gradient(150deg, ". $atts['color_right'] ." 35%, ". $atts['color_left'] ." 35%); border-color: ".$atts['color_bottom']."'";
	}

	$banner = "<div class='brand_shop_banner container-fluid". $atts['brand_uniq'] ."' ". $banner_style ." >";
	$banner = $banner . "<div class='box-logo-brand'><img src='".$logo_url."' class='logo-brand' alt='". get_the_title() . "'></div></div>";

	return $banner;
}
add_shortcode( 'brand_shop_banner', 'brand_shop_banner' );
