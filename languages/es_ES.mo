��    G      T      �      �  
   �  /   �  /   �  +   �     $     4     A     O     T  
   b     m  	   z     �     �     �     �     �     �     �     �     �            B   /     r  
   �     �     �     �     �     �  ?   �       	        )     /     ?     P     V     b  	   k     u     |     �     �     �     �     �     �     �     �     �  
   �                          "     '     -     9  	   B     L  '   X     �      �  "   �  
   �     �     �  T  �     I  3   V  5   �  ;   �     �               ,     4  
   =     H  
   T     _     y     �     �     �     �     �     �     �     �       8   *     c     z     �     �     �     �     �  T   �     #  	   1     ;     J     Z     r     x  	   �  
   �     �     �     �     �     �     �     �  	   �     �       
        '  
   0  	   ;     E  	   Q  	   [     e     l          �     �  !   �     �     �  0   �     &     /  	   8    review(s) <strong>Error</strong>: First name is required! <strong>Error</strong>: Last name is required!. <strong>Error</strong>: Phone is required!. Add your review Apply Coupon Availability: Cart Cart Subtotal Categories Categories:  Category: Click here to login Client:  Continue Shopping Copyright &copy; 2016  Coupon code Create an account Date Date: Email address Estimate your shipping cost Featured Products Fill in blanks below to sign up. You can change the details later. Filter by Categories First name Forgot you password?  Have %s reviews Home Hot I'm searching for... If you signed up. Enter your Email Address & Password to login. In stock Last name Login Login to system Name of products Order Order Total Order by Order by: Order: Password Phone Price Proceed to checkout Product Product is added to cart Products Quantity Related Products Remember me Reset here Sale Select category Sign up Tag: Tags Title Update Cart Username View Cart View Detail You have no items in your shopping cart You may also like&hellip; You may be interested in&hellip; You must be logged in to checkout. Your order item items Project-Id-Version: vg-siva
Report-Msgid-Bugs-To: http://wordpress.org/tags/_s
POT-Creation-Date: 2017-11-03 09:57+0700
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-02-16 16:06+0000
Last-Translator: admin <alejandrocardenas957@gmail.com>
Language-Team: Español
X-Generator: Loco https://localise.biz/
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x;_ex;_n;esc_html_e;esc_html__
X-Poedit-Basepath: ..
X-Poedit-SearchPath-0: .
Language: es_ES
Plural-Forms: nplurals=2; plural=n != 1;
X-Loco-Version: 2.2.0; wp-4.9.9  opinion(es) <strong>Error</strong>: Los nombres son requeridos! <strong>Error</strong>: Los apellidos son requeridos! <strong>Error</strong>: El numero de telefono es requerido! Deja tu opinión  Aplicar Cupon Disponibilidad: Carrito Subtotal Categorias Categorias: Categoria: Click aqui para ingresar. Cliente: Continuar Comprando Derechos de autor &copy; 2016  Cupon de descuento Crear una cuenta Fecha Fecha: Correo electronico Valor estimado de tu compra Productos Destacados Llene todos los campos, estos los puedes editar despues. Filtrar por categorias Nombres Olvidaste la contraseña? Tiene %s opiniones Inicio Agotado BUSCAR PRODUCTO Si ya tienes una cuenta. Ingresa tu dirección de correo electrónico y contraseña. En existencia Apellidos Inicia sesión Iniciar sesión Nombre de los productos Orden Valor Total Orden por Orden por: Orden: Contraseña Telefono Precio Continuar compra Producto Ha sido añadido al carrito Productos Cantidad Productos Relacionados Recuerdame Recordar Promoción CATEGORIA Registrarme Etiqueta: Etiquetas Titulo Actualizar carrito Nombre de usuario Ver carrito Ver mas No tienes productos en el carrito Tambien te puede interesar Te puede interesar Debes ingresar con tu cuenta para poder comprar. Tu orden producto productos 