<?php
/**
 * The template for displaying 404 pages(not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package VG Siva
 */

get_header('404'); ?>
<div class="page-content">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/404/text_404-1.png" class="img-responsvie text"/>
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/404/text_404-2.png" class="img-responsvie text"/>
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/404/text_404-3.png" class="img-responsvie text text-3"/>
	<div class="action">
		<?php get_search_form(); ?>
		<p><a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/404/icon-home.png" class="img-responsvie"/></a></p>
	</div>
</div>
<?php get_footer('404'); ?>
