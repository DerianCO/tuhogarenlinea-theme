<?php
/*
 * This is driver page. DO NOT MODIFY!!!
 */

$layout = vg_siva_get_default_layout();
switch($layout)
{
	case "layout-5":
		get_template_part('template-parts/header/layout-5', 'header');
		break;
	case "layout-4":
		get_template_part('template-parts/header/layout-4', 'header');
		break;
	case "layout-3":
		get_template_part('template-parts/header/layout-3', 'header');
		break;
	case "layout-2":
		get_template_part('template-parts/header/layout-2', 'header');
		break;
	default:
		get_template_part('template-parts/header/layout-1', 'header');
		break;
}