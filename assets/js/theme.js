/**
 * The Javascript for our theme.
 *
 * @package VG Siva
 */
(function($) {
	"use strict"; 
	jQuery(document).ready(function($) {
		// Off Canvas Navigation ---------------------------------------
		var offcanvas_open = false;
		var offcanvas_from_left = false;
		var offcanvas_from_right = false;
		var window_width = $(window).innerWidth();
		
		// Submenu adjustments ---------------------------------------
		function submenu_adjustments() {
			$(".main-navigation > ul > .menu-item").mouseenter(function() {
				if($(this).children(".sub-menu").length > 0) {
					var submenu = $(this).children(".sub-menu");
					var window_width = parseInt($(window).outerWidth());
					var submenu_width = parseInt(submenu.outerWidth());
					var submenu_offset_left = parseInt(submenu.offset().left);
					var submenu_adjust = window_width - submenu_width - submenu_offset_left;
					var dir = $('html').attr("dir");
					
					if(dir == "rtl"){
						if(submenu_adjust < 0) {
							submenu.css("right", submenu_adjust-30 + "px");
							submenu.addClass("active");
						}
					}else{
						if(submenu_adjust < 0) {
							submenu.css("left", submenu_adjust-30 + "px");
						}
					}
				}
			});
		}
		
		submenu_adjustments();
		
		function offcanvas_left() {
			$(".vg-website-wrapper").removeClass("slide-from-right");
			$(".vg-website-wrapper").addClass("slide-from-left");
			$(".vg-website-wrapper").addClass("vg-menu-open");
			
			offcanvas_open = true;
			offcanvas_from_left = true;
			
			$(".vg-menu").addClass("open");
			$("body").addClass("offcanvas_open offcanvas_from_left");
			
			$(".nano").nanoScroller();
		}
		
		function offcanvas_right() {
			$(".vg-website-wrapper").removeClass("slide-from-left");
			$(".vg-website-wrapper").addClass("slide-from-right");
			$(".vg-website-wrapper").addClass("vg-menu-open");		
			
			offcanvas_open = true;
			offcanvas_from_right = true;
			
			$(".vg-menu").addClass("open");
			$("body").addClass("offcanvas_open offcanvas_from_right");

			$(".nano").nanoScroller();
		}
		
		function offcanvas_close() {
			if(offcanvas_open === true) {	
				$(".vg-website-wrapper").removeClass("slide-from-left");
				$(".vg-website-wrapper").removeClass("slide-from-right");
				$(".vg-website-wrapper").removeClass("vg-menu-open");
				
				offcanvas_open = false;
				offcanvas_from_left = false;
				offcanvas_from_right = false;
							
				$(".vg-menu").removeClass("open");
				$("body").removeClass("offcanvas_open offcanvas_from_left offcanvas_from_right");
			}
		}
		
		$(".offcanvas-menu-button, .open-offcanvas").click(function() {
			offcanvas_right();
		});
		
		$("#button_offcanvas_sidebar_left").click(function() {
			offcanvas_left();
		});
		
		$(".vg-website-wrapper").on("click", ".vg-pusher-after", function(e) {
			offcanvas_close();
		});
		
		$(".vg-pusher-after").swipe({
			swipeLeft:function(event, direction, distance, duration, fingerCount) {
				offcanvas_close();
			},
			swipeRight:function(event, direction, distance, duration, fingerCount) {
				offcanvas_close();
			},
			tap:function(event, direction, distance, duration, fingerCount) {
				offcanvas_close();
			},
			threshold:0
		});
		
		// Mobile menu ---------------------------------------
		$(".mobile-navigation .menu-item-has-children").append('<div class="more"><i class="fa fa-plus-circle"></i></div>');
		
		$(".mobile-navigation").on("click", ".more", function(e) {
			e.stopPropagation();
			
			$(this).parent().toggleClass("current")
							.children(".sub-menu").toggleClass("open");
							
			$(this).html($(this).html() == '<i class="fa fa-plus-circle"></i>' ? '<i class="fa fa-minus-circle"></i>' : '<i class="fa fa-plus-circle"></i>');
			$(".nano").nanoScroller();
		});
		
		$(".mobile-navigation").on("click", "a", function(e) {
			if(($(this).attr('href') === "#") ||($(this).attr('href') === "")) {
				$(this).parent().children(".more").trigger("click");
			} else {
				offcanvas_close();
			}
		});
		
		// Toogle Search Box ---------------------------------------
		var product_search  = $(".search-inside");
		
		$(".search-icon").on("click", function(e) {
			e.stopPropagation();
			
			if(product_search.hasClass("active"))
				product_search.removeClass("active");
			else
				product_search.addClass("active");
			
			e.preventDefault();
		});
		
		$(product_search).on("click", function(e){
			e.stopPropagation();
		});
		
		$(document).on("click", function(e) {
			e.stopPropagation();
			
			if(product_search.hasClass('active'))
				product_search.removeClass('active');
		});
		
		$(".widget_wysija_cont .wysija-submit").wrap('<span class="wysija-submit-wrap"></span>');
		
		
		// Quickview JS ---------------------------------------
		function product_quick_view_ajax(id) 
		{
			$.ajax({
				url: vg_siva_ajaxurl,
				data: {
					"action" 	 : "vg_siva_product_quick_view",
					"product_id" : id
				},
				success: function(results) {
					
					$("#placeholder_product_quick_view").html(results);
					$(".lazy").lazy();	
					var curent_dragging_item;
			
					$("#placeholder_product_quick_view .featured_img_temp").hide();
					
					$("#placeholder_product_quick_view .thumbnails").owlCarousel({
						singleItem : false,
						autoHeight : true,
						transitionStyle:"fade",
						lazyLoad : true,
						slideSpeed : 300,
						dragBeforeAnimFinish: false,
						navigation: true,
					});

					$("#quick_view_container").show();
					
					var form_variation = $("#placeholder_product_quick_view").find('.variations_form');
					var form_variation_select = $("#placeholder_product_quick_view").find('.variations_form .variations select');
					
					form_variation.wc_variation_form();
					form_variation_select.change();
					$(".lazy").lazy();
				},
				error: function(errorThrown) { console.log(errorThrown); }
			});
		}
		
		$(document).on('click', '.vg_siva_product_quick_view_button', function(e) {
			e.preventDefault();
			var product_id = $(this).data('product_id');
			product_quick_view_ajax(product_id);
		});
		
		$(window).mouseup(function(e) {	    
			var container = $("#placeholder_product_quick_view");
			if(! container.is(e.target) && container.has(e.target).length === 0) {	    
				$('#quick_view_container').hide();
			}
		});

		$(document).on("click", "#close_quickview", function(){
			$('#quick_view_container').hide();
		});

		$("#quick_view_container").on('click', '.zoom', function(e){
			e.preventDefault();
		});
		
		/* For add to card button */
		$('body').append('<div class="atc-notice-wrapper"><div class="atc-notice"></div><div class="close"><i class="fa fa-times-circle"></i></div></div>');
		
		$('.atc-notice-wrapper .close').on("click", function(){
			$('.atc-notice-wrapper').fadeOut();
			$('.atc-notice').html('');
		});
		
		$('body').on('adding_to_cart', function(event, button, data) {
			var ajaxPId = button.attr('data-product_id');
			//get product info by ajax
			$.post(
				vg_siva_ajaxurl, 
				{
					'action': 'vg_siva_get_productinfo',
					'product_id':  ajaxPId
				},
				function(response){
					$('.atc-notice').html(response);
					$(".lazy").lazy();
				}
			);
		});
		
		$('body').on('added_to_cart', function(event, fragments, cart_hash) {			
			$('.atc-notice-wrapper').fadeIn();
		});
		
		// Countdown
		$('.timer-grid').each(function(){
			var countTime = $(this).attr('data-time');
			$(this).countdown(countTime, function(event) {
				$(this).html(
					'<div class="day"><span class="number">'+event.strftime('%D')+' </span><i>days</i></div> <div class="hour"><span class="number">'+event.strftime('%H')+'</span><i>Hrs</i></div><div class="min"><span class="number">'+event.strftime('%M')+'</span> <i>Mins</i></div> <div class="sec"><span class="number">'+event.strftime('%S')+' </span><i>Secs</i></div>'
				);
			});
		});
		
		jQuery('.hotdeal').each(function(){
			var countTime = new Date();
			countTime = new Date(countTime.getFullYear() + 2, 1 - 1, 26);
			jQuery(this).countdown(countTime, function(event) {
				jQuery(this).html(
					'<div class="day"><span class="number">'+event.strftime('%D')+' </span><i>days</i></div> <div class="hour"><span class="number">'+event.strftime('%H')+'</span><i>hours</i></div><div class="min"><span class="number">'+event.strftime('%M')+'</span> <i>mins</i></div> <div class="sec"><span class="number">'+event.strftime('%S')+' </span><i>secs</i></div>'
				);
			});
		});
		
		jQuery('#coming-soon').each(function(){
			var countTime = '2018/08/20';
			jQuery(this).countdown(countTime, function(event) {
				jQuery(this).html(
					'<div class="day"><span class="number">'+event.strftime('%D')+' </span>days</div><i class="line">:</i><div class="hour"><span class="number">'+event.strftime('%H')+'</span>hours</div><i class="line">:</i><div class="min"><span class="number">'+event.strftime('%M')+'</span>mins</div> <i class="line">:</i><div class="sec"><span class="number">'+event.strftime('%S')+' </span>secs</div>'
				);
			});
		});
		
		//Woocommerce
		/* Category Product View Module */
		$('.view-mode').each(function(){
			/* Grid View */
			$(this).find('.grid').on("click", function(event){
				event.preventDefault();
				
				$('#content .view-mode').find('.grid').addClass('active');
				$('#content .view-mode').find('.list').removeClass('active');
				
				$('#content .shop-products').removeClass('list-view');
				$('#content .shop-products').addClass('grid-view');
				
				$('#content .list-col4').removeClass('col-xs-12 col-sm-6 col-lg-4');
				$('#content .list-col8').removeClass('col-xs-12 col-sm-6 col-lg-8');
			});
			
			/* List View */
			$(this).find('.list').on("click", function(event){
				event.preventDefault();
			
				$('#content .view-mode').find('.list').addClass('active');
				$('#content .view-mode').find('.grid').removeClass('active');
				
				$('#content .shop-products').addClass('list-view');
				$('#content .shop-products').removeClass('grid-view');
				
				$('#content .list-col4').addClass('col-xs-12 col-sm-6 col-lg-4');
				$('#content .list-col8').addClass('col-xs-12 col-sm-6 col-lg-8');
			});
		});
		
		// Gallery Posts
		$("#owl-slider-one-img").owlCarousel({
			navigation: false,
			slideSpeed: 300,
			pagination: false,
			singleItem: true,
			//navigationText: ["Prev", "Next"],
		});
		
		
		// Scroll Sticky Menu
		if(sticky_menu==true)
			stickymenu();
		
		// Add none loading when click to cart
		$('a.ajax_add_to_cart').addClass('no-preloader');
		$('a.compare').addClass('no-preloader');
		$('a.add_to_wishlist').addClass('no-preloader');
		$('.single-product-image .images a').addClass('no-preloader');
		
		// Add loading when click to any link
		$('body a').click(function() {
			var link   = $(this).attr('href');
			var loader = $(this).hasClass('no-preloader');
			if(!loader && typeof link !== "undefined" && link.toLowerCase().indexOf("/") >= 0) {
				jQuery('#pageloader').show();
			}
		});
		
		// Remove loading when click on loading 
		$('#pageloader').click(function() {
			$('#pageloader').fadeOut('slow');
		});
		
		//Add Class Front Page Body
		if($('.main-container').hasClass('front-page')) {
			$('body').addClass('vgw-front-page');
		}else{
			$('body').removeClass('vgw-front-page');
		}
		
		//Fix Mini Cart Safari
		$('.mini_cart_inner').on("hover", function(){
			$(".lazy").lazy();
			var minicart = $('.mcart-border');
			if(minicart.hasClass('active')){
				minicart.removeClass('active');
				minicart.css('visibility', 'hidden');
			}else{
				minicart.addClass('active');
				minicart.css('visibility', 'visible');
			}
		});
		
		//Menu Toogle
		$('.button_toggle').on("click", function(){
			var attr = $(this);
			attr.removeAttr('href');
			var menu = $('.toggle-box');
			if(attr.hasClass('active')){
				menu.removeClass('active');
				attr.removeClass('active');
				menu.css('visibility', 'hidden');
			}else{
				menu.addClass('active');
				attr.addClass('active');
				menu.css('visibility', 'visible');
			}
		});
		
		// Popup Search
		$( '.search-toggle' ).on( 'click.break', function( event ) {
			$( '.search-overlay' ).toggleClass( 'hidden' );
		});
		$( '.btn-close' ).on( 'click.break', function( event ) {
			$( '.menu-popup-fixed' ).addClass( 'hidden' );
		} );
		
		// Toogle Menu Box ---------------------------------------
		var toogle_menu  = jQuery(".toogle-inside");
		
		$(".toogle-icon").on("click", function(e) {
			e.stopPropagation();
			
			if(toogle_menu.hasClass("active"))
				toogle_menu.removeClass("active");
			else
				toogle_menu.addClass("active");
			
			e.preventDefault();
		});
		
		$(toogle_menu).on("click", function(e){
			e.stopPropagation();
		});
		
		$(document).on("click", function(e) {
			e.stopPropagation();
			
			if(toogle_menu.hasClass('active'))
				toogle_menu.removeClass('active');
		});
		
		// Top Settings Box ---------------------------------------
		var top_setting  = jQuery(".setting-inside");
		
		$(".setting-icon").on("click", function(e) {
			e.stopPropagation();
			
			if(top_setting.hasClass("active"))
				top_setting.removeClass("active");
			else
				top_setting.addClass("active");
			
			e.preventDefault();
		});
		
		$(top_setting).on("click", function(e){
			e.stopPropagation();
		});
		
		$(document).on("click", function(e) {
			e.stopPropagation();
			
			if(top_setting.hasClass('active'))
				top_setting.removeClass('active');
		});
		
	});

	//Counter About Us
	$('.statistic').appear(function() {
		$('.timer').countTo({
			speed: 4000,
			refreshInterval: 60,
			formatter: function(value, options) {
				return value.toFixed(options.decimals);
			}
		});
	});
	
	// Quantity buttons
	$('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').addClass('buttons_added');
	
	$(document).on('click', '.plus, .minus', function() {
		// Get values
		var $qty		= $(this).closest('.quantity').find('.qty'),
			currentVal	= parseFloat($qty.val()),
			max			= '',
			min			= 1,
			step		= 1;

		// Format values
		if(! currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
		if(max === '' || max === 'NaN') max = '';
		if(min === '' || min === 'NaN') min = 0;
		if(step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;

		// Change the value
		if($(this).is('.plus')) {
			if(max &&(max == currentVal || currentVal > max)) {
				$qty.val(max);
			} else {
				$qty.val(currentVal + parseFloat(step));
			}
		} else {
			if(min &&(min == currentVal || currentVal < min)) {
				$qty.val(min);
			} else if(currentVal > 0) {
				$qty.val(currentVal - parseFloat(step));
			}
		}

		// Trigger change event
		$qty.trigger('change');
	});
	
	/*To Top*/
	$(".to-top").hide();
	/* fade in #back-top */
	$(function() {
		$(window).scroll(function() {
			if($(this).scrollTop() > 100) {
				$('.to-top').fadeIn();
			} else {
				$('.to-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('.to-top').on("click", function() {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
	
	$('a.add_to_cart_button').on("click", function() {
		$(this).removeClass('loading');
	});
	
	// add review click
	$('.add-review').on('click',function(event){
		event.preventDefault();
		if($("#reviews").length > 0){
			$('a[href="#reviews"]').trigger('click');
			$('html, body').animate({scrollTop:$("#reviews").offset().top-50}, 'slow');
		}
	});
	
	$(".lazy").lazy();
	
	$('.SlectBox').SumoSelect({ csvDispCount: 3, captionFormatAllSelected: "Yeah, OK, so everything." });
	
	/* Projects Filter with shuffle.js */
	$('#projects_list').shuffle({ itemSelector: '.project' });
	
	$('.filter-options .btn').on('click', function() {			
		var filterBtn = $(this),
			isActive = filterBtn.hasClass('active'),
			group = isActive ? 'all' : filterBtn.data('group');

		// Hide current label, show current label in title
		if(!isActive) {
			$('.filter-options .active').removeClass('active');
		}

		filterBtn.toggleClass('active');

		// Filter elements
		$('#projects_list').shuffle('shuffle', group);
	});
	
	//Toogle Category
	$.fn.extend({  
         accordion: function() {       
            return this.each(function() {
            	
            	var $jqul = $(this);
            	
				if($jqul.data('accordiated'))
					return false;
													
				$.each($jqul.find('ul'), function(){
					$(this).data('accordiated', true);
					$(this).hide();
				});
				
				$.each($jqul.find('span.more'), function(){
					$(this).on("click", function(e){
						activate(this);
						return void(0);
					});
				});
				
				var active = (location.hash)?$(this).find('a[href=' + location.hash + ']')[0]:'';

				if(active){
					activate(active, 'toggle');
					$(active).parents().show();
				}
				
				function activate(el,effect){
					$(el).parent('li').toggleClass('active').siblings().removeClass('active').children('ul').slideUp('fast');
					$(el).siblings('ul')[(effect || 'slideToggle')]((!effect)?'fast':null);
				}
				
            });
        } 
    }); 
	
	$(".main-navigation.style-3 ul li.menu-item-has-children").each(function(){
        $(this).append('<span class="more"><a href="javascript:void(0)"></a></span>');
    });
	$('.main-navigation.style-3 ul').accordion();
	$(".main-navigation.style-3 ul li.active").each(function(){
		$(this).children().next("ul").css('display', 'block');
	});
	
	$(".nano").nanoScroller();
	
})(jQuery);

jQuery(window).load(function($) {
	jQuery(".lazy").lazy();
	jQuery('#pageloader').fadeOut('slow');
});

function stickymenu(){
	var vina_width 		= jQuery(window).width();
	var vina_top 		= jQuery('.site-navigation');
	var vina_header 	= jQuery('#vg-header-wrapper');
	var top_height		= vina_top.outerHeight();
	var header_height 	= vina_header.outerHeight();
	var height_scroll 	= 0;
	jQuery(window).scroll(function() {
		if(vina_header.hasClass('header-style-3')){
			height_scroll = 5;
		}else{
			height_scroll = header_height - top_height;
		}
		if(vina_width > 1024){
			var NextScroll = jQuery(this).scrollTop();
			
			if (NextScroll >= (height_scroll)) {
				vina_header.addClass('fixed');
				vina_header.parent().css('padding-top', header_height);
			}
			else {	
				vina_header.removeClass('fixed');
				vina_header.parent().css('padding-top', 0);
			}
		}
		else {
			vina_header.removeClass('fixed');
			vina_header.addClass('relative');
		}
	});
	jQuery(window).resize(function(event) {
		if(jQuery(window).width() < 1024){
			vina_header.removeClass('fixed');
			vina_header.addClass('relative');
		}
	});
}

/* Get Param value */
function vinageckogetParameterByName(name, string) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(string);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
