<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package VG Siva
 */
$vg_siva_options = get_option("vg_siva_options");
get_header(); ?>
<?php
$sidebar = 'none';
$blogClass = 'sidebar-none';
$blogColClass = 12;
$pullContent = 'pull-left';

if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$sidebar = $_GET['sidebar'];
	switch($sidebar) {
		case 'left':
			$blogClass = 'sidebar-left';
			$blogColClass = 9;
			$pullContent = 'pull-right';
			break;
		case 'right':
			$blogClass = 'sidebar-right';
			$blogColClass = 9;
			$pullContent = 'pull-left';
			break;
		default:
			$blogClass = 'sidebar-none';
			$blogColClass = 12;
			$pullContent = 'pull-left';
			break;
	}
}elseif(isset($vg_siva_options['default_blog_sidebar']) && $vg_siva_options['default_blog_sidebar']!=''){
	$sidebar = $vg_siva_options['default_blog_sidebar'];
	switch($sidebar) {
		case 'left':
			$blogClass = 'sidebar-left';
			$blogColClass = 9;
			$pullContent = 'pull-right';
			break;
		case 'right':
			$blogClass = 'sidebar-right';
			$blogColClass = 9;
			$pullContent = 'pull-left';
			break;
		default:
			$blogClass = 'sidebar-none';
			$blogColClass = 12;
			$pullContent = 'pull-left';
			break;
	}
}
$colContent = (is_active_sidebar('sidebar-1')) ? esc_attr($blogColClass) : 12;
?>
<div id="vg-main-content-wrapper" class="main-container blog-page <?php echo esc_attr($blogClass); ?>">
	<div class="site-breadcrumb <?php echo (is_active_sidebar('feature-post')) ? 'no-margin' : 'has-margin' ;?>">
		<div class="container">
			<?php vg_siva_breadcrumbs(); ?>
		</div>
	</div><!-- .site-breadcrumb -->

	<?php if(is_category()) :?>
		<?php if(is_active_sidebar('feature-post')) : ?>
		<div class="st-feature-post visible-buttons pg-absolute">
			<?php dynamic_sidebar('feature-post'); ?>
		</div><!-- End .st-feature -->
		<?php endif;?>
	<?php endif; ?>

	<div class="container">
		<div class="row">
			<div id="content" class="col-xs-12 col-md-<?php echo esc_attr($colContent); ?> site-content <?php echo esc_attr($pullContent); ?>">
				<main id="main" class="site-main" role="main">
					<?php if(is_category()) :?>
						<?php if(is_active_sidebar('sticky-post')) : ?>
						<div class="st-sticky-post hidden-controls">
							<h2 class="title"><?php echo esc_html__('post of the month', 'vg-siva'); ?></h2>
							<?php dynamic_sidebar('sticky-post'); ?>
						</div><!-- End .st-sticky -->
						<?php endif;?>
					<?php endif; ?>

					<div class="row">

						<?php if(is_active_sidebar('st-testimonial')) : ?>
						<div class="col-xs-12 col-md-5 st-testimonial">
							<div class="inside-section">
								<?php dynamic_sidebar('st-testimonial'); ?>
							</div>
						</div><!-- End .st-testimonial -->
						<?php endif;?>

						<div class="col-xs-12 col-md-<?php echo (is_active_sidebar('st-testimonial')) ? '7' : '12'; ?> all-posts">
							<header class="page-header">
								<?php
									the_archive_title('<h1 class="page-title">', '</h1>');
									the_archive_description('<div class="taxonomy-description">', '</div>');
								?>
							</header><!-- .page-header -->

							<?php if(have_posts()) : ?>
								<?php /* Start the Loop */ ?>
								<?php while(have_posts()) : the_post(); ?>

									<?php

										/*
										 * Include the Post-Format-specific template for the content.
										 * If you want to override this in a child theme, then include a file
										 * called content-___.php(where ___ is the Post Format name) and that will be used instead.
										 */
										get_template_part('template-parts/content', get_post_format());
									?>

								<?php endwhile; ?>

								<div class="clear clearfix"></div>
								<div class="pagination">
									<?php vg_siva_pagination(); ?>
								</div>

							<?php else : ?>

								<?php get_template_part('template-parts/content', 'none'); ?>

							<?php endif; ?>
						</div>
					</div>
				</main><!-- #main -->
			</div><!-- #content -->

			<?php if($sidebar == 'left' || $sidebar == 'right' ) : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
