<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package VG Siva
 */

get_header(); ?>
<div id="vg-main-content-wrapper" class="main-container search-page">
	<div class="site-breadcrumb">
		<div class="container">
			<?php vg_siva_breadcrumbs(); ?>
		</div>
	</div><!-- .site-breadcrumb -->
	<div class="container">
		<div class="row">
			<div id="content" class="col-xs-12 col-md-9 site-content">
				<main id="main" class="site-main" role="main">
				
				<header class="page-header">
					<h1 class="page-title"><?php printf(esc_html__('Search Results for: %s', 'vg-siva'), '<span>' . get_search_query() . '</span>'); ?></h1>
				</header><!-- .page-header -->
				
				<div class="all-posts">
					<?php if(have_posts()) : ?>

					<?php /* Start the Loop */ ?>
						<?php while(have_posts()) : the_post(); ?>

							<?php
							/**
							 * Run the loop for the search to output the results.
							 * If you want to overload this in a child theme then include a file
							 * called content-search.php and that will be used instead.
							 */
							get_template_part('template-parts/content', get_post_format());
							?>

						<?php endwhile; ?>
						<div class="clear clearfix"></div>
						<div class="pagination">
							<?php vg_siva_pagination(); ?>
						</div>
					<?php else : ?>
						<div class="entry-content">
							<p><?php echo esc_html__('Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'vg-siva'); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					<?php endif; ?>
				
				</div>
				</main><!-- #main -->
			</div><!-- #content -->
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
