<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package VG Siva
 */
$vg_siva_options = get_option("vg_siva_options");
?>
				<footer id="vg-footer-wrapper">
					<?php if(is_active_sidebar('ft-newsletters')) : ?>
					<div class="ft-newsletters">
						<div class="container">
							<?php dynamic_sidebar('ft-newsletters'); ?>
						</div>
					</div><!-- .ft-newsletters -->
					<?php endif;?>

					<div class="footer">
						<div class="container">
							<div class="row">
								<?php if(is_active_sidebar('footer-01')) : ?>
								<div class="col-xs-12 col-md-<?php echo (is_active_sidebar('footer-02')) ? 4 : 12; ?> col-footer">
									<div class="row">
										<?php dynamic_sidebar('footer-01'); ?>
									</div>
								</div>
								<?php endif; ?>

								<?php if(is_active_sidebar('footer-02')) : ?>
								<div class="col-xs-12 col-md-<?php echo (is_active_sidebar('footer-01')) ? 8 : 12; ?> col-footer">
									<div class="row">
										<?php dynamic_sidebar('footer-02'); ?>
									</div>
								</div>
								<?php endif; ?>
							</div>
							<?php if(is_active_sidebar('footer-payments')) : ?>
							<div class="ft-payment col-xs-12 col-md-<?php echo (is_active_sidebar('footer-payments')) ? 8 : 12; ?> col-footer">
								<div class="row">
									<?php dynamic_sidebar('footer-payments'); ?>
								</div>
							</div>
							<?php endif; ?>
						</div>
					</div><!-- .footer -->


					<div class="bottom-footer">
						<?php if(is_active_sidebar('footer-copyright')) : ?>
						<div class="colbottomft col-xs-12 col-lg-12 col-md-12">

							<?php
								dynamic_sidebar('footer-copyright');
							?>

						</div>
						<?php endif; ?>
					</div>

				</footer><!-- #vg-footer-wrapper -->

				<?php if(is_active_sidebar('social-icons')) : ?>
				<div class="vgw-socials">
					<?php dynamic_sidebar('social-icons'); ?>
				</div>
				<?php endif;?>
			</div><!-- .vg-pusher -->


			<!-- Off canvas from right -->
			<div class="vg-menu slide-from-right">
				<div class="nano">
					<div class="content">
						<div class="offcanvas_content_right">
							<div id="mobiles-menu-offcanvas">
								<nav class="mobile-navigation primary-navigation visible-xs visible-sm">
								<?php
									wp_nav_menu(array(
										'theme_location'  => 'primary',
										'fallback_cb'     => false,
										'container'       => false,
										'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
									));
								?>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Product Quick View -->
			<div id="quick_view_container">
				<div id="placeholder_product_quick_view" class="woocommerce">
					<div class="loaded"><?php echo esc_html_e('Loading...', 'vg-siva'); ?></div>
				</div>
			</div>

		</div><!-- .vg-website-wrapper -->
		<?php wp_footer(); ?>

	</body>
</html>
