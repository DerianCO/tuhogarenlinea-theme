<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package VG Siva
 */

$vg_siva_options = get_option("vg_siva_options");
get_header(); ?>
<?php
$sidebar = 'none';
$blogClass = 'sidebar-none';
$blogColClass = 12;
$pullContent = 'pull-left';
if(isset($vg_siva_options['default_blog_sidebar']) && $vg_siva_options['default_blog_sidebar']!=''){
	$sidebar = $vg_siva_options['default_blog_sidebar'];
	switch($sidebar) {
		case 'left':
			$blogClass = 'sidebar-left';
			$blogColClass = 9;
			$pullContent = 'pull-right';
			break;
		case 'right':
			$blogClass = 'sidebar-right';
			$blogColClass = 9;
			$pullContent = 'pull-left';
			break;
		default:
			$blogClass = 'sidebar-none';
			$blogColClass = 12;
			$pullContent = 'pull-left';
			break;
	}
}
$colContent = (is_active_sidebar('sidebar-1')) ? esc_attr($blogColClass) : 12;
?>
<div id="vg-main-content-wrapper" class="main-container page-site <?php echo esc_attr($blogClass); ?>">
	<div class="page-content">
		<div class="site-breadcrumb">
			<div class="container">
				<?php vg_siva_breadcrumbs(); ?>
			</div>
		</div><!-- .site-breadcrumb -->
		<div class="container">
			<header class="page-header">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</header>

			<div class="row">
				<div id="content" class="col-xs-12 col-md-<?php echo esc_attr($colContent); ?> site-content <?php echo esc_attr($pullContent); ?>">
					<main id="main" class="site-main" role="main">

						<?php while(have_posts()) : the_post(); ?>

							<?php get_template_part('template-parts/content', 'page'); ?>

							<?php
								// If comments are open or we have at least one comment, load up the comment template.
								if(comments_open() || get_comments_number()) :
									comments_template();
								endif;
							?>

						<?php endwhile; // End of the loop. ?>

					</main><!-- #main -->
				</div><!-- #primary -->

				<?php if($sidebar == 'left' || $sidebar == 'right' ) : ?>
					<?php get_sidebar(); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div><!-- #vg-main-content-wrapper -->
<?php get_footer(); ?>
