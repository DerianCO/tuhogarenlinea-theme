<?php
/**
 * The template part for displaying content
 *
 * @package VG Siva
 */
if(! defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
$vg_siva_options = get_option("vg_siva_options");
global $post;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if(!is_single()): ?>
		<?php $noFloat = ((has_post_thumbnail() && get_the_post_thumbnail() == NULL) || get_the_title() == '')? 'no-float' : ''; ?>
		<div class="post-wrapper <?php echo esc_attr($noFloat); ?>">
			<?php 
			if(is_home()){
			?>
				<?php vg_siva_post_thumbnail(); ?>
			<?php 
			}
			?>
			
			<?php the_title(sprintf('<h4 class="entry-title"><a href="%s" rel="bookmark">', get_permalink()), '</a></h4>'); ?> 
			
			<?php 
			if(is_home()){
				?>
				
				<?php $notitle = (get_the_title() == '')? 'no-title' : ''; ?>
				<div class="entry-meta meta-small <?php echo esc_attr($notitle); ?>">
					<?php if(function_exists('vg_siva_entry_meta_small')) : ?>
						<?php vg_siva_entry_meta_small(); ?>
					<?php endif; ?>
				</div><!-- .entry-meta -->
				
				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div><!-- .entry-summary -->
				<?php
			}
			?>
			
			<div class="post-date <?php echo esc_attr($noFloat); ?>">
				<?php if(function_exists('vg_siva_entry_date')) : ?>
					<?php vg_siva_entry_date(); ?>
				<?php endif; ?>
			</div>
		</div>
	<?php else : ?>
	<div class="post-wrapper">
		
		<?php vg_siva_post_thumbnail(); ?>

		<div class="post-content">
			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header><!-- .entry-header -->
			
			<div class="entry-meta">
				<?php if(function_exists('vg_siva_entry_meta')) : ?>
					<?php vg_siva_entry_meta(); ?>
				<?php endif; ?>
			</div><!-- .entry-meta -->
			
			<div class="entry-content">
				<?php the_content(); ?>
				<?php wp_link_pages(array('before' => '<div class="page-links">' . esc_html__('Pages:', 'vg-siva'), 'after' => '</div>', 'pagelink' => '<span>%</span>')); ?>
			</div><!-- .entry-content -->
			
			<div class="entry-footer">
				<div class="row">
					<div class="col-xs-12 col-md-6 meta_tags">
						<?php if(function_exists('vg_siva_entry_tags')) : ?>
							<?php vg_siva_entry_tags(); ?><!-- .entry-tags -->
						<?php endif; ?>
					</div>
					<div class="col-xs-12 col-md-6 sharing">
						<?php if(isset($vg_siva_options['sharing_options']) && $vg_siva_options['sharing_options']) : ?>
							<div class="addthis_native_toolbox"></div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div><!-- .post-content -->
	</div><!-- .post-wrapper -->
	<?php endif; ?>
</article><!-- #post-## -->
