<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package VG Siva
 */

// Get Theme Options Values
$vg_siva_options = get_option("vg_siva_options");
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Page Loader Block -->
<?php if (isset($vg_siva_options['theme_loading']) && $vg_siva_options['theme_loading']) : ?>
<div id="pageloader">
	<div id="loader"></div>
	<div class="loader-section left"></div>
	<div class="loader-section right"></div>
</div>
<?php endif; ?>

<div class="vg-website-wrapper">
	<div class="vg-pusher">
		<div class="vg-pusher-after"></div> <!-- Don't REMOVE this code -->
		
		<header id="vg-header-wrapper" class="header-style-3">
			<div class="site-header">
				<div class="container">
					<div class="row">	
						<div id="logo-wrapper" class="col-xs-12 col-lg-2">
							<div class="logo-inside">
								<?php vg_siva_display_top_logo(); // Call display top logo function; ?>
							</div>
						</div><!-- End site-logo -->
						
						<?php 
						$setPadding = (is_active_sidebar('top-cart')) ? 'set-padding' : ''; 
						
						$colMDMenu = (is_active_sidebar('top-menu') || is_active_sidebar('top-cart') || is_active_sidebar('top-settings') || is_active_sidebar('top-account')) ? 8 : 12;  
						$colLGMenu = (is_active_sidebar('top-menu') || is_active_sidebar('top-cart') || is_active_sidebar('top-settings') || is_active_sidebar('top-account')) ? 7 : 10;
						?>
						
						<div id="navigation" class="col-xs-12 col-md-<?php echo esc_attr($colMDMenu ); ?> col-lg-<?php echo esc_attr($colLGMenu ); ?>">
							<div class="menu-wrapper visible-lg <?php echo esc_attr($setPadding); ?>">
								<nav class="main-navigation default-navigation">
									<?php
										$walker = new rc_scm_walker;
										wp_nav_menu(array(
											'theme_location'  => 'primary',
											'fallback_cb'     => false,
											'container'       => false,
											'items_wrap'      => '<ul class="%1$s">%3$s</ul>',
											'walker' 		  => $walker
										));
									?>
								</nav><!-- .main-navigation -->
							</div><!-- End large-navigation -->
							<div class="responsive-navigation visible-xs">
								<ul>
									<li class="offcanvas-menu-button">
										<a class="tools_button">
											<span class="menu-button-text"><?php esc_html_e('Menu', 'vg-siva'); ?></span>
											<span class="tools_button_icon">
												<i class="fa fa-bars"></i>
											</span>
										</a>
									</li>
								</ul>
							</div><!-- End mobile-navigation -->
						</div><!-- End #navigation -->
						
						<?php if(is_active_sidebar('top-menu') || is_active_sidebar('top-cart') || is_active_sidebar('top-settings') || is_active_sidebar('top-account')) : ?>
						<div class="col-xs-12 col-md-4 col-lg-3 top-feature">
							<?php if(is_active_sidebar('top-menu')) : ?>	
							<div class="toogle-menu">
								<span class="toogle-icon"></span>
								<div class="toogle-inside">
									<?php dynamic_sidebar('top-menu'); ?>
								</div>
							</div><!-- End .toogle-menu -->
							<?php endif;?>	
							
							<?php if(is_active_sidebar('top-cart')) : ?>	
							<div class="top-cart style-2">
								<?php dynamic_sidebar('top-cart'); ?>
							</div><!-- End .top-cart -->
							<?php endif;?>	
							
							<?php if(is_active_sidebar('top-settings')) : ?>	
							<div class="top-settings">
								<span class="setting-icon"></span>
								<div class="setting-inside">
									<?php dynamic_sidebar('top-settings'); ?>
								</div>
							</div><!-- End .top-settings -->
							<?php endif;?>	
							
							<?php if(is_active_sidebar('top-account')) : ?>	
							<div class="top-account">
								<?php dynamic_sidebar('top-account'); ?>
							</div><!-- End .top-account -->
							<?php endif;?>	
						</div><!-- End .top-feature -->
						<?php endif;?>	
					</div>
				</div>
			</div>
		</header>