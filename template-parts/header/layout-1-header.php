<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package VG Siva
 */

// Get Theme Options Values
$vg_siva_options = get_option("vg_siva_options");
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Page Loader Block -->
<?php if (isset($vg_siva_options['theme_loading']) && $vg_siva_options['theme_loading']) : ?>
<div id="pageloader">
	<div id="loader"></div>
	<div class="loader-section left"></div>
	<div class="loader-section right"></div>
</div>
<?php endif; ?>

<div class="vg-website-wrapper">
	<div class="vg-pusher">
		<div class="vg-pusher-after"></div> <!-- Don't REMOVE this code -->
		
		<header id="vg-header-wrapper" class="header-style-1">
			<?php if(isset($vg_siva_options["top_bar_switch"]) && !empty($vg_siva_options["top_bar_switch"])): ?>
			<div class="top-bar">
				<div class="container">
					<div class="row">
						<?php if(is_active_sidebar('top-bar-01')) : ?>	
						<div class="col-xs-12 col-md-<?php echo (is_active_sidebar('top-bar-02')) ? '6' : '12' ?> col-topbar col-1">
							<div class="inside-column">
								<?php dynamic_sidebar('top-bar-01'); ?>
							</div>
						</div><!-- End .col-topbar.col-1 -->
						<?php endif;?>
						
						<?php if(is_active_sidebar('top-bar-02')) : ?>	
						<div class="col-xs-12 col-md-<?php echo (is_active_sidebar('top-bar-01')) ? '6' : '12' ?> col-topbar col-2">
							<div class="inside-column">
								<?php dynamic_sidebar('top-bar-02'); ?>
							</div>
						</div><!-- End .col-topbar.col-1 -->
						<?php endif;?>
					</div>
				</div>
			</div><!-- End .top-bar -->
			<?php endif;?>		
			
			<div class="site-header">
				<div class="container">
					<div class="row">	
						<?php
						
							$colLogo = '12';
							if(is_active_sidebar('top-search') && is_active_sidebar('top-cart')) {
								$colLogo = '3';
							}elseif(is_active_sidebar('top-search') && !is_active_sidebar('top-cart')){
								$colLogo = '6';
							}elseif(!is_active_sidebar('top-search') && is_active_sidebar('top-cart')){
								$colLogo = '9';
							}else{
								$colLogo = '12 logo-center';
							}
							
						?>
						<div id="logo-wrapper" class="col-xs-12 col-md-<?php echo esc_attr($colLogo); ?>">
							<div class="logo-inside">
								<?php vg_siva_display_top_logo(); // Call display top logo function; ?>
							</div>
						</div><!-- End site-logo -->
						
						<?php if(is_active_sidebar('top-search')) : ?>	
						<div class="col-xs-12 col-md-6 top-search">
							<span class="search-icon"></span>
							<div class="search-inside">
								<?php dynamic_sidebar('top-search'); ?>
							</div>
						</div><!-- End .top-search -->
						<?php endif;?>		

						<?php if(is_active_sidebar('top-cart')) : ?>	
						<div class="col-xs-12 col-md-3 top-cart style-1">
							<div class="cart-inside">
								<?php dynamic_sidebar('top-cart'); ?>
							</div>
						</div><!-- End .top-cart -->
						<?php endif;?>	
						
					</div>
				</div>
			</div>
			<?php $setPadding = (is_active_sidebar('top-search') || is_active_sidebar('top-cart')) ? 'set-padding' : ''; ?>
			<div class="site-navigation">
				<div class="container">
					<div class="row">
						<div id="navigation" class="col-xs-12 col-lg-<?php echo is_active_sidebar('top-static') ? 8 : 12; ?>">
							<div class="menu-wrapper visible-lg <?php echo esc_attr($setPadding); ?>">
								<nav class="main-navigation default-navigation">
									<?php
										$walker = new rc_scm_walker;
										wp_nav_menu(array(
											'theme_location'  => 'primary',
											'fallback_cb'     => false,
											'container'       => false,
											'items_wrap'      => '<ul class="%1$s">%3$s</ul>',
											'walker' 		  => $walker
										));
									?>
								</nav><!-- .main-navigation -->
							</div><!-- End large-navigation -->
							<div class="responsive-navigation visible-xs">
								<ul>
									<li class="offcanvas-menu-button">
										<a class="tools_button">
											<span class="menu-button-text"><?php esc_html_e('Menu', 'vg-siva'); ?></span>
											<span class="tools_button_icon">
												<i class="fa fa-bars"></i>
											</span>
										</a>
									</li>
								</ul>
							</div><!-- End mobile-navigation -->
						</div><!-- End #navigation -->
						
						<?php if(is_active_sidebar('top-static')) : ?>
						<div class="col-xs-12 col-lg-4 top-static">
							<div class="static-inside">
								<?php dynamic_sidebar('top-static'); ?>
							</div>
						</div>
						<?php endif;?>
					</div>
				</div>
			</div>
		</header>