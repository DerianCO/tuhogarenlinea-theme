<?php
/**
 * The template for displaying project content in the single-project.php template
 *
 * Override this template by copying it to yourtheme/projects/content-single-project.php
 *
 * @author 		WooThemes
 * @package 	Projects/Templates
 * @version     1.0.0
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

global $wpdb, $post;
$vg_siva_options = get_option("vg_siva_options");

$prcates = get_the_terms($post->ID, 'project-category');
$datagroup = array();
foreach ($prcates as $category) {
	$datagroup[] = '"'.$category->slug.'"';
}
$datagroup = implode(", ", $datagroup);

?>
<div class="main-container main-site-page project-page">
	<div class="site-breadcrumb">
		<div class="container">
			<?php vg_siva_breadcrumbs(); ?>
		</div>
	</div>
	<div class="site-content">
		<div class="container">
			<?php
				/**
				 * projects_before_single_project hook
				 *
				 */
				 do_action('projects_before_single_project');
			?>

			<div id="project-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<div class="row">
					<?php $attachment_ids = projects_get_gallery_attachment_ids(); ?>
					<?php if ($attachment_ids) { ?>
					<div class="col-xs-12 col-sm-12 col-md-5">
						<?php
							/**
							 * projects_before_single_project_summary hook
							 * @hooked projects_template_single_title - 10
							 * @hooked projects_template_single_short_description - 20
							 * @hooked projects_template_single_feature - 30
							 * @hooked projects_template_single_gallery - 40
							 */
							do_action( 'projects_before_single_project_summary' );
						?>
					</div>
					<?php } ?>
					
					
					<div class="col-xs-12 col-sm-12 col-md-<?php if ($attachment_ids) { echo '7';} else { echo '12';} ?>">
						<div class="summary entry-summary">
							<?php
								/**
								 * projects_single_project_summary hook
								 *
								 * @hooked projects_template_single_description - 10
								 * @hooked projects_template_single_meta - 20
								 */
								do_action( 'projects_single_project_summary' );
							?>
						</div><!-- .summary -->
					</div>
				</div>
				<div class="project-content">
					<?php do_action('projects_single_project_summary_desc'); ?>
				</div>
				<?php
					/**
					 * projects_after_single_project_summary hook
					 *
					 */
					do_action('projects_after_single_project_summary');
				?>
			</div><!-- #project-<?php the_ID(); ?> -->

			<?php
				/**
				 * projects_after_single_project hook
				 *
				 * @hooked projects_single_pagination - 10
				 */
				//do_action('projects_after_single_project');
			?>
			
		</div>
	</div>
</div>