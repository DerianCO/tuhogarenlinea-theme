<?php
/**
 * The Template for displaying project archives, including the main showcase page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/projects/archive-project.php
 *
 * @author 		WooThemes
 * @package 	Projects/Templates
 * @version     1.0.0
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

global $projects_loop, $post, $vg_siva_projectrows;
$vg_siva_options = get_option("vg_siva_options");

// Store loop count we're currently on
if (empty($projects_loop['loop']))
	$projects_loop['loop'] = 0;
// Store column count for displaying the grid
if (empty($projects_loop['columns'])) {
	$projects_loop['columns'] = apply_filters('projects_loop_columns', 4);
}

$projects_loop['columns'] = isset($vg_siva_options['portfolio_columns']) ? $vg_siva_options['portfolio_columns'] : "4";

if (isset($_GET['columns'])) {
	$projects_loop['columns'] = (int)$_GET['columns'];
}

get_header(); ?>
<div class="main-container main-site-page project-page">
	<div class="site-breadcrumb">
		<div class="container">
			<?php vg_siva_breadcrumbs(); ?>
		</div>
	</div>
	<div class="site-content">
		<div class="container">
			<div class="logo-page">
				<img src="<?php echo esc_url( get_template_directory_uri()); ?>/assets/images/logo/dark/logo.png" class="img-responsvie"/>
			</div>
			
			<header class="page-header style-2 text-center">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
				?>		
				<div class="description"><?php echo esc_html__('Sed ut perspiciatis unde omnis iste natus error sit volup tatem accusantium doloremque laud.','vg-siva'); ?></div>
			</header>
		
			<div class="project-archive">
				<?php
					/**
					 * projects_before_main_content hook
					 *
					 * @hooked projects_output_content_wrapper - 10 (outputs opening divs for the content)
					 */
					do_action('projects_before_main_content');
				?>

				<?php do_action('projects_archive_description'); ?>

				<?php if (have_posts()) : ?>

					<?php
						/**
						 * projects_before_loop hook
						 *
						 */
						do_action('projects_before_loop');
					?>
					<div class="filter-options btn-group">
						<button data-group="all" class="btn active btn--warning"><span><?php esc_html_e('All', 'vg-siva');?></span></button>
						<?php 
						$datagroups = array();
						if(isset($vg_siva_options['portfolio_per_page'])) {
							query_posts('posts_per_page='.esc_attr($vg_siva_options['portfolio_per_page']).'&post_type=project');
						}
						while (have_posts()) : the_post();
						
							$prcates = get_the_terms($post->ID, 'project-category');
							
							foreach ($prcates as $category) {
								$datagroups[$category->slug] = $category->name;
							}
							?>
						<?php endwhile; // end of the loop. ?>
						<?php
						foreach($datagroups as $key=>$value) { ?>
							<button data-group="<?php echo esc_attr($key);?>" class="btn btn--warning"><span><?php echo esc_html($value);?></span></button>
						<?php }
						?>
					</div>
					<div class="all-projects">
						<div class="row">
						<?php projects_project_loop_start(); ?>
							<?php $vg_siva_projectrows = 1; ?>
							<?php while (have_posts()) : the_post(); ?>

								<?php projects_get_template_part('content', 'project'); ?>

							<?php endwhile; // end of the loop. ?>

						<?php projects_project_loop_end(); ?>
						</div>
					</div><!-- .projects -->

					<?php
						/**
						 * projects_after_loop hook
						 *
						 * @hooked projects_pagination - 10
						 */
						do_action('projects_after_loop');
					?>

				<?php else : ?>

					<?php projects_get_template('loop/no-projects-found.php'); ?>

				<?php endif; ?>

				<?php
					/**
					 * projects_after_main_content hook
					 *
					 * @hooked projects_output_content_wrapper_end - 10 (outputs closing divs for the content)
					 */
					do_action('projects_after_main_content');
				?>

				<?php
					/**
					 * projects_sidebar hook
					 *
					 * @hooked projects_get_sidebar - 10
					 */
					//do_action('projects_sidebar');
				?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>