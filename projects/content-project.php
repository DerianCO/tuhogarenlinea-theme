<?php
/**
 * The template for displaying project content within loops.
 *
 * Override this template by copying it to yourtheme/projects/content-project.php
 *
 * @author 		WooThemes
 * @package 	Projects/Templates
 * @version     1.0.0
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

global $projects_loop, $post, $vg_siva_projectrows , $vg_siva_projectsfound;
$vg_siva_options = get_option("vg_siva_options");


// Store loop count we're currently on
if (empty($projects_loop['loop']))
	$projects_loop['loop'] = 0;
// Store column count for displaying the grid
if (empty($projects_loop['columns'])) {
	$projects_loop['columns'] = apply_filters('projects_loop_columns', 4);
}

$projects_loop['columns'] = isset($vg_siva_options['portfolio_columns']) ? $vg_siva_options['portfolio_columns'] : "4";

if (isset($_GET['columns'])) {
	$projects_loop['columns'] = (int)$_GET['columns'];
}

// Increase loop count
$projects_loop['loop']++;

// Extra post classes
$classes = array();
if (0 == ($projects_loop['loop'] - 1) % $projects_loop['columns'] && $projects_loop['loop'] > 1)
	$classes[] = 'first';
if (0 == $projects_loop['loop'] % $projects_loop['columns'])
	$classes[] = 'last';

$colwidth = 12/$projects_loop['columns'];
$classes[] = 'item-col col-xs-12 col-sm-'.esc_attr($colwidth);

$prcates = get_the_terms($post->ID, 'project-category');
$datagroup = array();
foreach ($prcates as $category) {
	$datagroup[] = '"'.$category->slug.'"';
}
$datagroup = implode(", ", $datagroup);

$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
?>
<?php
if ((0 == ($projects_loop['loop'] - 1) % 2) && ($projects_loop['columns'] == 2)) {
	if($vg_siva_projectrows!=1) {
		echo '<div class="group">';
	}
}
?>
<div <?php post_class($classes); ?> data-groups='[<?php echo esc_attr($datagroup); ?>]'>
	<div class="inner-project">

		<?php do_action('projects_before_loop_item'); ?>
		<?php
			/**
			 * projects_loop_item hook
			 *
			 * @hooked projects_template_loop_project_thumbnail - 10
			 * @hooked projects_template_loop_project_title - 20
			 */
			do_action('projects_loop_item');
		?>
		<a href="<?php the_permalink(); ?>" class="readmore"><i class="zmdi zmdi-zoom-in"></i></a>
	</div>
</div>
<?php if ((0 == $projects_loop['loop'] % 2 || $vg_siva_projectsfound == $projects_loop['loop']) && ($projects_loop['columns'] == 2)) {
	if($vg_siva_projectrows!=1) {
		echo '</div>';
	}
} ?>