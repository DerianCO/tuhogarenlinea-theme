<?php
/**
 * Single Project title
 *
 * @author 		WooThemes
 * @package 	Projects/Templates
 * @version     1.0.0
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

?>
<div class="page-header">
	<h1 itemprop="name" class="page-title style-2"><?php the_title(); ?></h1>
</div>