<?php
/**
 * Single Project Meta
 *
 * @author 		WooThemes
 * @package 	Projects/Templates
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post;
?>
<div class="project-meta">
	<div class="project_date">
		<span class="day"><?php esc_html_e('Date:', 'vg-siva');?> <b><?php echo get_the_date('d'); ?> <?php echo get_the_date('F'); ?>, <?php echo get_the_date('Y'); ?></b></span>
	</div>
	<?php
		// Categories
		$terms_as_text 	= get_the_term_list( $post->ID, 'project-category', '<li>', '</li> / <li>', '</li>' );

		// Meta
		$client 		= esc_attr( get_post_meta( $post->ID, '_client', true ) );
		$url 			= esc_url( get_post_meta( $post->ID, '_url', true ) );

		do_action( 'projects_before_meta' );

		/**
		 * Display categories if they're set
		 */
		if ( $terms_as_text ) {
			echo '<div class="categories">';
			echo '<span class="title">' . esc_html__( 'Categories: ', 'vg-siva' ) . '</span>';
			echo '<ul class="single-project-categories">';
			echo ($terms_as_text);
			echo '</ul>';
			echo '</div>';
		}

		/**
		 * Display client if set
		 */
		if ( $client ) {
			echo '<div class="client">';
			echo '<span class="title">' . esc_html__( 'Client: ', 'vg-siva' ) . '</span>';
			echo '<span class="client-name">' . $client . '</span>';
			echo '</div>';
		}

		do_action( 'projects_after_meta' );
	?>
</div>