<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package VG Siva
 */
if(isset($vg_siva_options['sharing_options']) && $vg_siva_options['sharing_options']){
	function vg_siva_addthis_js(){
		wp_enqueue_script('addthis', 'http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-553dd7dd1ff880d4', array(), '1.0.0', 'all');
	}
	add_action('wp_head', 'vg_siva_addthis_js', 99);
}

get_header(); 
?>
<?php
$sidebar = 'right';
$blogClass = 'sidebar-right';
$blogColClass = 9;
$pullContent = 'pull-left';

if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$sidebar = $_GET['sidebar'];
	switch($sidebar) {
		case 'left':
			$blogClass = 'sidebar-left';
			$blogColClass = 9;
			$pullContent = 'pull-right';
			break;
		case 'none':
			$blogClass = 'sidebar-none';
			$blogColClass = 12;
			break;
		default:
			$blogClass = 'sidebar-right';
			$blogColClass = 9;
			$pullContent = 'pull-left';
			break;
	}
}elseif(isset($vg_siva_options['default_blog_sidebar']) && $vg_siva_options['default_blog_sidebar']!=''){
	$sidebar = $vg_siva_options['default_blog_sidebar'];
	switch($sidebar) {
		case 'left':
			$blogClass = 'sidebar-left';
			$blogColClass = 9;
			$pullContent = 'pull-right';
			break;
		case 'none':
			$blogClass = 'sidebar-none';
			$blogColClass = 12;
			break;
		default:
			$blogClass = 'sidebar-right';
			$blogColClass = 9;
			$pullContent = 'pull-left';
			break;
	}
}
$colContent = (is_active_sidebar('sidebar-1')) ? esc_attr($blogColClass) : 12;
?>
<div id="vg-main-content-wrapper" class="main-container single-post <?php echo esc_attr($blogClass); ?>">
	<div class="site-breadcrumb">
		<div class="container">
			<?php vg_siva_breadcrumbs(); ?>
		</div>
	</div><!-- .site-breadcrumb -->
	<div class="container">
		<div class="row">
			<div id="content" class="col-xs-12 col-md-<?php echo esc_attr($colContent); ?> site-content <?php echo esc_attr($pullContent); ?>">
				<main id="main" class="site-main" role="main">

				<?php while(have_posts()) : the_post(); ?>

					<?php get_template_part('template-parts/content', 'single'); ?>

					<?php the_post_navigation(); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if(comments_open() || get_comments_number()) :
							comments_template();
						endif;
					?>

				<?php endwhile; // End of the loop. ?>

				</main><!-- #main -->
			</div><!-- #content -->

			<?php if($sidebar == 'left' || $sidebar == 'right' ) : ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
