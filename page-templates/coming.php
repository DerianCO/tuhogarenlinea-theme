<?php
/**
 * Template Name: Coming Soon
 *
 * Description: Coming Soon
 *
 * @package    VG Siva
 * @author     VinaGecko <support@vinagecko.com>
 * @copyright  Copyright(C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */

get_header('coming');
?>
<div id="vg-main-content-wrapper" class="coming-soon">
	<div id="logo-wrapper">
		<div class="logo-inside">
			<?php vg_siva_display_top_logo(); // Call display top logo function; ?>
		</div>
	</div><!-- End site-logo -->
	
	<?php if(have_posts()) : ?>
		<?php while(have_posts()) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php the_content(); ?>
			</article>
		<?php endwhile; ?>
	<?php endif; ?>
</div><!-- .vg-main-content-wrapper -->

<?php get_footer('coming'); ?>